/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <QApplication>
#include <QObject>
#include <QtQuick/QQuickView>
#include <QScriptEngine>
#include <QQmlEngine>
#include <QQmlContext>
#include <iostream>
#include <relaybus/Relay.hpp>
#include <extension_system/ExtensionSystem.hpp>
#include <relaybus/qt/Relaybus.h>
#include <QFile>
#include <QTextStream>

#include <QQmlDebuggingEnabler>

class Viewer : public QObject {
	Q_OBJECT
public:
	Viewer(QQuickView &view) : _view(view) {}

	Q_INVOKABLE void showFullScreen() {
		_view.showFullScreen();
	}

	Q_INVOKABLE void showNormal() {
		_view.showNormal();
	}

	Q_INVOKABLE bool isFullScreen() {
		return _view.windowState() & Qt::WindowFullScreen;
	}

	Q_INVOKABLE void setTitle(const QString &title) {
		_view.setTitle(title);
	}

private:
	QQuickView &_view;
};

int main(int argc, char **argv) {
	QApplication app(argc, argv);

	int execReturn = 0;

	if(argc < 2) {
		std::cerr<<"relaybus loader\n"
			<<"  Copyright (C) 2012-2014 Bernd Amend <berndamend+relaybus@googlemail.com>\n"
			<<"  Copyright (C) 2012-2014 Michael Adam <relaybus+Michael.Adam@gmail.com>\n"
			<<"  This program is free software: you can redistribute it and/or modify\n"
			<<"  it under the terms of the GNU Lesser General Public version 2.1 or later as published by\n"
			<<"  the Free Software Foundation. This program comes with ABSOLUTELY NO WARRANTY\n"
			<<"usage "<<argv[0]<<" <js-filename|qml-filename|--list-relays>\n";
		return 1;
	}

	const std::string filename = argv[1];

	if(filename == "--list-relays") {
		extension_system::ExtensionSystem extension_system;

		extension_system.searchDirectory("./");
		extension_system.searchDirectory("../lib");

		std::cout << "\nAvailable relays:\n";
		auto extensions = extension_system.extensions<relaybus::Relay>();
		for(const auto &iter : extensions) {
			std::cout << "  " << iter.name() << " Version: " << iter.version() << "\n";
		}
		std::cout<<"\n";
		return 0;
	}

	const std::string extension = filename.substr(filename.length()-3);

	try {
		if(extension == ".js") {
			QFile scriptFile(QString::fromStdString(filename));
			if (!scriptFile.open(QIODevice::ReadOnly)) {
				// handle error
				std::cerr<< ":(" << "\n";
			}
			QTextStream stream(&scriptFile);
			QString contents = stream.readAll();
			scriptFile.close();

			QScriptEngine engine;
			QObject *rb = new relaybus::Relaybus();
			QScriptValue objectValue = engine.newQObject(rb);
			engine.globalObject().setProperty("relaybus", objectValue);

			engine.evaluate(contents, QString::fromStdString(filename));

			execReturn = app.exec();

			return execReturn;
		} else if(extension == "qml") {
			QQuickView view;
			view.setTitle("Viewer");

			auto viewer = new Viewer(view);

			view.engine()->addImportPath(".");
			view.engine()->addImportPath("../lib");

			QObject::connect(view.engine(), &QQmlEngine::quit, &QApplication::quit);

			view.setResizeMode(QQuickView::SizeRootObjectToView);
			view.setSource(QUrl(argv[1]));

			view.rootContext()->setContextProperty("viewer", viewer);

			view.show();

			execReturn = app.exec();

			return execReturn;
		} else {
			std::cerr<<"Unknown file extension. Bye\n";
		}

	} catch(std::exception &e) {
		std::cerr<<"exception "<<e.what()<<"\n";
		return -1;
	} catch(...) {
		std::cerr<<"unknown exception\n";
		return -2;
	}
}

#include "main.moc"
