/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include "Relaybus.h"

#include <relaybus/Bus.hpp>
#include <relaybus/Host.hpp>
#include <extension_system/ExtensionSystem.hpp>

#include <iostream>

using namespace relaybus;

Relaybus::Relaybus()
{
	_bus = Bus::getSingleton();
	_host = std::make_shared<Host>(std::make_shared<extension_system::ExtensionSystem>(), _bus);
}

void Relaybus::addSearchDirectory(const QString &path) {
	_host->getExtensionSystem()->searchDirectory(path.toStdString());
}

void Relaybus::createRelay(const QString &path, const QString &relay_type)
{
	_host->create(path.toStdString(), relay_type.toStdString());
}

void Relaybus::destroyRelay(const QString &path)
{
	_host->destroy(path.toStdString());
}

void Relaybus::connectPins(const QString &destination, const QString &source)
{
	_bus->connect(Wire(destination.toStdString(), source.toStdString(), WireProfile(WireType::Auto, 0)));
}

void Relaybus::setPin(const QString &path, const QString &value)
{
	_bus->set(path.toStdString(), value.toStdString());
}

