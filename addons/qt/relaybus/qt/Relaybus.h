/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <QObject>
#include <QString>
#include <memory>

#include "qt_global.hpp"

namespace relaybus {
	class Bus;
	class Host;

class RELAYBUS_QT_API Relaybus : public QObject
{
	Q_OBJECT

public:
	Relaybus();
public slots:
	Q_INVOKABLE void addSearchDirectory(const QString &path);
	Q_INVOKABLE void createRelay(const QString &path, const QString &relay_type);
	Q_INVOKABLE void destroyRelay(const QString &path);
	Q_INVOKABLE void connectPins(const QString &destination, const QString &source);
	Q_INVOKABLE void setPin(const QString &path, const QString &value);

private:
	std::shared_ptr<relaybus::Bus> _bus;
	std::shared_ptr<relaybus::Host> _host;
};

}
