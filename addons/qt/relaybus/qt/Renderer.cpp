/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include "Renderer.h"

#include <QOpenGLFunctions>

#include <QtCore/QMutex>
#include <QtCore/QThread>

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFramebufferObject>
#include <QtGui/QGuiApplication>

#include <QtQuick/QQuickWindow>
#include <qsgsimpletexturenode.h>

#include <QElapsedTimer>
#include <QDebug>

#include <relaybus/chrono.hpp>

#include <relaybus/Bus.hpp>

#include <relaybus/qt/gl/ImageRenderer.hpp>
#include <relaybus/qt/gl/SmoothRenderingHelper.hpp>

class RendererTextureNode : public QObject, public QSGSimpleTextureNode, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	RendererTextureNode(Renderer &renderer, QQuickWindow *window)
		: _initialized(false)
		, m_fbo(0)
		, m_texture(0)
		, m_window(window)
		, m_renderer(renderer)
		, _image_changed(false)
		, _image_output(nullptr)
		, _framenum(0)
		, _force_framerate(-1)
		, _video_state(Renderer::VideoState_InvalidImageOutput)
	{
		_bus = relaybus::Bus::getSingleton();
		connect(m_window, SIGNAL(beforeRendering()), this, SLOT(renderFBO()));
	}

	~RendererTextureNode()
	{
		delete m_texture;
		delete m_fbo;
	}

	public slots:
		void setPath(const QString &path) {
			_path = path;
			_input.reset();
			_image_output = nullptr;
		}

		void setForceFramerate(int fps) {
			_force_framerate = fps;
		}

		void renderFBO()
		{
			if(!_initialized) {
				_initialized = true;
				initializeOpenGLFunctions();
			}
			if(_image_output == nullptr) {
				_input = _bus->getRelay(_path.toStdString());
				if(_input == nullptr)
					_image_output = nullptr;
				else
					_image_output = dynamic_cast<relaybus::ImageOutput*>(_input.get());
			}

			if(_image_output) {
				if(!_image_changed && _image_output->hasNewImage()) {
					_image = _image_output->getNewImage();
					_last_image_update = relaybus::clock().now();
					if(_image.isValid()) {
						setVideoState(Renderer::VideoState_Ok);
						// We assume that we won't display images that are larger than 2^31 Pixels.
						if( m_size.width() !=  static_cast<int>(_image.r().width()) ||  m_size.height() !=  static_cast<int>(_image.r().height())) {
							m_size = QSize(_image.r().width(),  _image.r().height());
							delete m_fbo; m_fbo = nullptr;

							if(m_size.width() > 0 && m_size.height() > 0)
								emit imageSizeChanged(m_size);
						}
						_smooth_rendering_helper.updateNewImageTime();
					} else {
						setVideoState(Renderer::VideoState_InvalidImage);
					}
					_image_changed = true;
				} else {
					auto diff = relaybus::clock().now() - _last_image_update;
					if(_video_state != Renderer::VideoState_Timeout && diff > std::chrono::milliseconds(500)) {
						setVideoState(Renderer::VideoState_Timeout);
					}
				}
			} else {
				auto diff = relaybus::clock().now() - _last_image_update;
				if(_video_state != Renderer::VideoState_InvalidImageOutput && diff > std::chrono::milliseconds(500)) {
					setVideoState(Renderer::VideoState_InvalidImageOutput);
				}
			}

			if (!m_fbo) {
				QOpenGLFramebufferObjectFormat format;
				format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
				if(m_size.width() <= 0 || m_size.height() <= 0) {
					m_size = QSize(1920, 1080);
					emit imageSizeChanged(m_size);
				}
				m_fbo = new QOpenGLFramebufferObject(m_size, format);
				m_texture = m_window->createTextureFromId(m_fbo->texture(), m_size);
				setTexture(m_texture);
				setFiltering(QSGTexture::Linear);
			}

			m_fbo->bind();
			glViewport(0, 0, m_size.width(), m_size.height()); // size

			glDisable(GL_BLEND);
			glDisable(GL_DEPTH_TEST);

			glClearColor(0, 0, 0, 1.0);
			glClear(GL_COLOR_BUFFER_BIT);

			if(_image_output) {
				if(_image.isValid() && _image.r().format() != relaybus::image::RPixelformat::RPixelformat_NONE) {
					if(_force_framerate < 0) {
						if(_image_changed && _smooth_rendering_helper.renderNewImage(_image_output->pendingUpdateCount())) {
							_renderer.draw(_image.r());

							auto timeSinceLastUpdate = _smooth_rendering_helper.updateUpdateTime();
							// TODO: we should remove the following line later, to avoid performance penalties
							emit timeBetweenImageUpdates(QVariant::fromValue( std::chrono::duration_cast<std::chrono::microseconds>(timeSinceLastUpdate).count()));
							_image_changed = false;
						} else {
							// render texture again
							_renderer.draw();
						}
					} else {
						if(_image_changed) {
							if(_framenum % 2 == 0) {
								_renderer.draw(_image.r());

								auto timeSinceLastUpdate = _smooth_rendering_helper.updateUpdateTime();
								// TODO: we should remove the following line later, to avoid performance penalties
								emit timeBetweenImageUpdates(QVariant::fromValue( std::chrono::duration_cast<std::chrono::microseconds>(timeSinceLastUpdate).count()));
								_image_changed = false;
							} else {
								// render texture again
								_renderer.draw();
							}
						} else {
							// render texture again
							_renderer.draw();
						}
					}
				} else {
					_image_changed = false;
				}
			}

			_framenum++;
			_smooth_rendering_helper.updateRenderTime();

			//if(_framenum % 100 == 0)
			//	std::cout<<( 1000000.0 / relaybus::chrono::duration_cast<relaybus::chrono::microseconds>(_smooth_rendering_helper.getRenderTimeMeasurement().timeBetweenUpdates()).count())<<" fps\n";

			m_fbo->bindDefault();

			glEnable(GL_BLEND);
			glEnable(GL_DEPTH_TEST);

			m_window->update();
		}

signals:
	void imageSizeChanged(QSize);
	void videoStateChanged(int);
	void timeBetweenImageUpdates(QVariant inMicroseconds);

private:

	void setVideoState(int state)
	{
		const bool changed = (_video_state != state);
		_video_state = state;
		if(changed)
			emit videoStateChanged(state);
	}

	bool _initialized;
	QOpenGLFramebufferObject *m_fbo;
	QSGTexture *m_texture;
	QQuickWindow *m_window;

	Renderer &m_renderer;

	QSize m_size;

	relaybus::gl::ImageRenderer	_renderer;

	relaybus::ZeroCopy<relaybus::image::BaseImageView>		_image;

	bool									_image_changed;

	relaybus::gl::SmoothRenderingHelper _smooth_rendering_helper;

	relaybus::time_point _last_image_update;

	std::shared_ptr<relaybus::Bus> _bus;

	QString _path;
	std::shared_ptr<relaybus::Relay>	 _input;
	relaybus::ImageOutput				*_image_output;

	// HACK: add support to handle the forced framerate properly
	uint64_t						_framenum;

	int _force_framerate;

	int _video_state;
};

Renderer::Renderer()
	: _video_state(Renderer::VideoState_InvalidImageOutput)
	, _force_framerate(-1)
{
	setFlag(ItemHasContents, true);
}

QSize Renderer::imageSize() const {
	return _image_size;
}

void Renderer::setImageSize(const QSize &size)
{
	_image_size = size;
	emit imageSizeChanged(size);
}

int Renderer::videoState() const {
	return _video_state;
}

QString Renderer::path() const
{
	return _path;
}

int Renderer::forceFramerate() const {
	return _force_framerate;
}

void Renderer::setForceFramerate(int fps) {
	_force_framerate = fps;
	emit forceFramerateChanged(fps);
}

void Renderer::setPath(const QString &path)
{
	_path = path;
	emit pathChanged(_path);
}

void Renderer::setVideoState(int state)
{
	_video_state = state;
	emit videoStateChanged(state);
}

QSGNode *Renderer::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *)
{
	// Don't bother with resize and such, just recreate the node from scratch
	// when geometry changes.
	if (oldNode)
		delete oldNode;

	RendererTextureNode *node = new RendererTextureNode(*this, window());

	node->setRect(boundingRect());

	connect(node, &RendererTextureNode::imageSizeChanged, this,  &Renderer::setImageSize, Qt::DirectConnection);
	connect(node, &RendererTextureNode::videoStateChanged, this,  &Renderer::setVideoState, Qt::DirectConnection);
	connect(node, &RendererTextureNode::timeBetweenImageUpdates, this,  &Renderer::timeBetweenImageUpdates);
	connect(this, &Renderer::pathChanged, node,  &RendererTextureNode::setPath);
	connect(this, &Renderer::forceFramerateChanged, node,  &RendererTextureNode::setForceFramerate);

	node->setPath(_path);
	node->setForceFramerate(_force_framerate);

	return node;
}

#include "Renderer.moc"
