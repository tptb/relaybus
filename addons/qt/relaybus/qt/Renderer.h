/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <QQuickItem>
#include <memory>

#include <mutex>
#include <relaybus/Relay.hpp>
#include <relaybus/ImageOutput.hpp>

#include "qt_global.hpp"

class RendererTextureNode;

class RELAYBUS_QT_API Renderer : public QQuickItem
{
Q_OBJECT
	Q_PROPERTY(QSize imageSize READ imageSize NOTIFY imageSizeChanged)
	Q_PROPERTY(int videoState READ videoState NOTIFY videoStateChanged)
	Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
	Q_PROPERTY(int forceFramerate READ forceFramerate WRITE setForceFramerate NOTIFY forceFramerateChanged)

public:

	enum VideoState {
		VideoState_Ok,
		VideoState_InvalidImage,
		VideoState_Timeout,
		VideoState_InvalidImageOutput
	};

	Renderer();

	QSize imageSize() const;
	int videoState() const;
	QString path() const;
	int forceFramerate() const;

public slots:
	void setPath(const QString &path);
	void setForceFramerate(int fps);

signals:
	void imageSizeChanged(QSize);
	void videoStateChanged(int);
	void timeBetweenImageUpdates(QVariant inMicroseconds);
	void pathChanged(const QString &path);
	void forceFramerateChanged(int fps);

protected:
	QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *);

private slots:
	void setImageSize(const QSize &size);
	void setVideoState(int state);

private:
	QSize _image_size;
	int _video_state;
	QString _path;
	int _force_framerate;

	friend class RendererTextureNode;
};
