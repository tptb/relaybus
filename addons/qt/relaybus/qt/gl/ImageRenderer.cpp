 /**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include "ImageRenderer.hpp"

#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <iostream>

#include <relaybus/MakeString.hpp>

#include "predefined_shader.hpp"
#include "Texture.hpp"

#define checkForGLErrors() \
	{ \
		auto errorcode = glGetError(); \
		if(errorcode != GL_NO_ERROR) { \
			std::cerr	<<"ImageRenderer: " \
						<<EXTENSION_SYSTEM_FUNCTION_NAME<<":"<<__LINE__ \
						<<std::hex<<errorcode<<"\n"; \
		} \
	}

using namespace relaybus;
using namespace relaybus::image;
using namespace relaybus::gl;

namespace relaybus { namespace gl {

class InternalImageRenderer : protected QOpenGLFunctions {
public:
	InternalImageRenderer(const std::string &fragment_shader)
		: _textures(image_dimensions)
	#ifdef GL_UNPACK_ROW_LENGTH
		, _gl_unpack_row_length_available(true)
	#endif
	{
		initializeOpenGLFunctions();

		checkForGLErrors();

		checkForGLErrors();

		_program.addShaderFromSourceCode(QOpenGLShader::Vertex, vertex_shader.c_str());
		_program.addShaderFromSourceCode(QOpenGLShader::Fragment, fragment_shader.c_str());
		_program.link();

		checkForGLErrors();

		for(int i=0;i<image_dimensions;++i) {
			const std::string str = MakeString() << "tex" << i;
			_texture_locations[i] = _program.uniformLocation(str.c_str());

			texcoords[i] << QVector2D(0.f, 0.f)
				<< QVector2D(1.f, 0.f)
				<< QVector2D(0.f, 1.f)
				<< QVector2D(1.f, 1.f);

			const std::string str2 = MakeString() << "texcoord" << i;
			_coords_location[i] = _program.attributeLocation(str2.c_str());

			_texture_coordinates[i] = 1.0f;
		}

		checkForGLErrors();

		vertices << QVector2D(-1.f,-1.f);
		vertices << QVector2D( 1.f,-1.f);
		vertices << QVector2D(-1.f, 1.f);
		vertices << QVector2D( 1.f, 1.f);

		_tris_location = _program.attributeLocation("vert");

		checkForGLErrors();
	}

	void uploadImage(const BaseImageView &image) {
		checkForGLErrors();
		auto &desc = RPixelformat::PixelFormatDescriptionBase::get(image.format());

		GLint org_unpack_alignment;
		glGetIntegerv(GL_UNPACK_ALIGNMENT, &org_unpack_alignment);

		checkForGLErrors();

#ifdef GL_UNPACK_ROW_LENGTH
		GLint org_unpack_row_length;
		if(_gl_unpack_row_length_available) {
			glGetIntegerv(GL_UNPACK_ROW_LENGTH, &org_unpack_row_length);
			checkForGLErrors();
		}
#endif

		// upload textures
		for(int i=0;i<image_dimensions;++i) {
			if(image.data(i) == nullptr || desc.elementsPerPixel(i) == 0)
				break; // we assume that there are no empty planes

			glActiveTexture(GL_TEXTURE0 + i);

			checkForGLErrors();

			_textures.bind(i);

			checkForGLErrors();

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			auto elements_per_pixel = desc.elementsPerPixel(i);
			auto elements_per_stride = image.stride(i) / (desc.sizeOfThePixelType() * elements_per_pixel);

#ifdef GL_UNPACK_ROW_LENGTH
			if(_gl_unpack_row_length_available) {
				checkForGLErrors();

				glPixelStorei(GL_UNPACK_ROW_LENGTH, elements_per_stride);

				if(glGetError() != GL_NO_ERROR) {
					_gl_unpack_row_length_available = false;
				}
			}
#endif

			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

			GLenum type = 0;
			switch(desc.pixelTypeID()) {
				case TypeSystem<uint8_t>::id: type = GL_UNSIGNED_BYTE; break;
				case TypeSystem<uint16_t>::id: type = GL_UNSIGNED_SHORT; break;
				case TypeSystem< int8_t>::id: type = GL_BYTE; break;
				case TypeSystem< int16_t>::id: type = GL_SHORT; break;
				case TypeSystem< int32_t>::id: type = GL_INT; break;
				case TypeSystem<uint32_t>::id: type = GL_UNSIGNED_INT; break;
				case TypeSystem<float>::id: type = GL_FLOAT; break;
				case TypeSystem<double>::id: type = GL_DOUBLE; break;
			}

			GLenum format = 0;
			switch(elements_per_pixel) {
			case 1:
				format = GL_LUMINANCE;
				break;
			case 2:
#ifdef GL_RG
				format = GL_RG;
#else
				throw std::runtime_error("Can not render 2 channel images on this platform");
#endif
				break;
			case 3:
				format = GL_RGB;
				break;
			case 4:
				format = GL_RGBA;
				break;
			}

#ifdef GL_TEXTURE_BASE_LEVEL
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
#endif
#ifdef GL_TEXTURE_MAX_LEVEL
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
#endif

			auto img_height = image.height(i);
			auto img_width = image.width(i);

#ifdef GL_UNPACK_ROW_LENGTH
			if(_gl_unpack_row_length_available) {
				glTexImage2D(GL_TEXTURE_2D, 0, format, img_width, img_height, 0, format, type, image.data(i));
			} else {
#endif

#if 1
				glTexImage2D(GL_TEXTURE_2D, 0, format, elements_per_stride, img_height, 0, format, type, image.data(i));
				// Use the texture coordinates to map away not required image content
				float tx = static_cast<float>(img_width) / static_cast<float>(elements_per_stride);
				if(tx !=_texture_coordinates[i]) {
					texcoords[i].clear();
					texcoords[i] << QVector2D(0.f, 0.f)
								<< QVector2D(tx, 0.f)
								<< QVector2D(0.f, 1.0f)
								<< QVector2D(tx, 1.f);
					_texture_coordinates[i] = tx;
				}
#else
				// The performance of glTexSubImage2D is very bad in comparsion to the
				// implementation that uses the shader
				glTexImage2D( GL_TEXTURE_2D, 0, format, img_width, img_height, 0, format, type, NULL );

				for( int y = 0; y < img_height; ++y )
					glTexSubImage2D( GL_TEXTURE_2D, 0,
									0, y,
									img_width, 1,
									format, type,
									image.data(i) + y * image.stride(i));
#endif
			}

			checkForGLErrors();

#ifdef GL_UNPACK_ROW_LENGTH
		}

		if(_gl_unpack_row_length_available)
			glPixelStorei(GL_UNPACK_ROW_LENGTH, org_unpack_row_length);
#endif

		glPixelStorei(GL_UNPACK_ALIGNMENT, org_unpack_alignment);

		glActiveTexture(GL_TEXTURE0);
	}

	void draw() {
		_program.bind();

		for(int i=0;i<image_dimensions;++i) {
			_program.setUniformValue(_texture_locations[i], i);
			glActiveTexture(GL_TEXTURE0 + i);
			_textures.bind(i);

			_program.setAttributeArray(_coords_location[i], texcoords[i].constData());
			_program.enableAttributeArray(_coords_location[i]);
		}

		glActiveTexture(GL_TEXTURE0);

		_program.setAttributeArray(_tris_location, vertices.constData());

		_program.enableAttributeArray(_tris_location);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		_program.disableAttributeArray(_tris_location);

		_program.release();

		for(int i=0;i<image_dimensions;++i) {
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, 0);
			_program.disableAttributeArray(_coords_location[i]);
		}

		glActiveTexture(GL_TEXTURE0);
	}

	void draw(const BaseImageView &image) {
		uploadImage(image);
		draw();
	}

private:

	GLuint _tris_location;
	GLuint _coords_location[image_dimensions];
	float _texture_coordinates[image_dimensions];

	QOpenGLShaderProgram _program;

	Texture	_textures;
	std::array<GLint,image_dimensions> _texture_locations;

	Texture _output_texture;

	QVector<QVector2D>	vertices;
	QVector<QVector2D>	texcoords[image_dimensions];

	bool _gl_unpack_row_length_available;
};

}}

ImageRenderer::ImageRenderer()
	: _format(RPixelformat::RPixelformat_NONE) {

	// add standard shader
	_default_shader[RPixelformat::RPixelformat_RGB24] = fragment_shader_rgb24;
	_default_shader[RPixelformat::RPixelformat_GRAY8] = fragment_shader_luminance;
	_default_shader[RPixelformat::RPixelformat_DEPTH_16ULE] = fragment_shader_depth;
	_default_shader[RPixelformat::RPixelformat_YUV420P] = fragment_shader_yuv420p;
	_default_shader[RPixelformat::RPixelformat_YUVJ420P] = fragment_shader_yuv420p;

	resetDefault();
}

ImageRenderer::~ImageRenderer() {
}

void ImageRenderer::addShader(RPixelformat::Format format, const std::string &source ) {
	if(format == _format) {
		_format = RPixelformat::RPixelformat_NONE;
		_renderer.reset();
	}

	_shader[format] = source;
}

void ImageRenderer::resetDefault() {
	_format = RPixelformat::RPixelformat_NONE;
	_renderer.reset();

	_shader.clear();
	_shader = _default_shader;
}

void ImageRenderer::resetDefault(RPixelformat::Format format) {
	if(format == _format) {
		_format = RPixelformat::RPixelformat_NONE;
		_renderer.reset();
	}

	auto iter = _default_shader.find(format);

	if(iter != _default_shader.end())
		_shader[format] = iter->second;
	else
		_shader.erase(format);
}

bool ImageRenderer::draw() {
	if(_renderer == nullptr)
		return false;

	_renderer->draw();

	return true;
}

void ImageRenderer::draw(const BaseImageView &image) {
	draw(image, image.format());
}

void ImageRenderer::draw(const BaseImageView &image, RPixelformat::Format format) {
	if(image == nullptr || format == RPixelformat::RPixelformat_NONE)
		return;

	if(format != _format || _renderer == nullptr) {
		_format = format;
		_renderer.reset();

		// check if we can render it
		auto iter = _shader.find(format);

		if(iter == _shader.end())
			throw std::runtime_error("no shader available to render this image format");

		_renderer.reset(new InternalImageRenderer(iter->second));
	}

	if(_renderer == nullptr)
		throw std::runtime_error("_renderer was a nullptr");

	_renderer->draw(image);
}

void ImageRenderer::destroy() {
	_renderer.reset();
}
