/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include "../qt_global.hpp"
#include <relaybus/image/BaseImage.hpp>

#include <unordered_map>

namespace relaybus { namespace gl {

class InternalImageRenderer;

/**
 * @brief The ImageRenderer class
 * @remarks The class doesn't cache previously used shader,
 *			don't use one instance with different image formats.
 *			Not thread-safe
 */
class RELAYBUS_QT_API ImageRenderer {
public:
	ImageRenderer();
	ImageRenderer(const ImageRenderer&) =delete;
	ImageRenderer& operator=(const ImageRenderer&) =delete;
	~ImageRenderer();

	void addShader(image::RPixelformat::Format format, const std::string &source );

	void resetDefault();
	void resetDefault(image::RPixelformat::Format format);

	// true: rendering was successful, false: e.g. if no image was shown bevore
	bool draw();
	void draw(const image::BaseImageView &image);
	void draw(const image::BaseImageView &image, image::RPixelformat::Format format);

	// TODO: rename function
	void destroy();

private:
	image::RPixelformat::Format									_format;
	std::unique_ptr<InternalImageRenderer>					_renderer;

	std::unordered_map<image::RPixelformat::Format, std::string, std::hash<int>>	_shader;
	std::unordered_map<image::RPixelformat::Format, std::string, std::hash<int>>	_default_shader;
};

}}
