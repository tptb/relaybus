/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <relaybus/TimeMeasurement.hpp>

namespace relaybus { namespace gl {

class SmoothRenderingHelper {
public:

	void clear() {
		_render_tm.clear();
		_update_tm.clear();
		_new_tm.clear();
	}

	bool renderNewImage(std::size_t pending_update_count) {

		bool time_measurement_is_unstable = (!_render_tm.isStable() || !_update_tm.isStable());
		auto now = clock::now();
		auto next_render_time = now + (_render_tm.timeBetweenUpdates());
		auto next_update_time = _update_tm.nextUpdate();

		auto count_larger_then_threshold = pending_update_count > 3;

		auto its_render_time =	((next_render_time-std::chrono::milliseconds(1)) > next_update_time && pending_update_count > 1) ||
								((now+std::chrono::milliseconds(4)) > next_update_time);

		return (time_measurement_is_unstable || its_render_time || count_larger_then_threshold);
	}

	duration updateRenderTime() {
		return _render_tm.update();
	}
	duration updateUpdateTime() {
		return _update_tm.update();
	}
	duration updateNewImageTime() {
		return _new_tm.update();
	}

	const TimeMeasurement &getRenderTimeMeasurement() const {
		return _render_tm;
	}
	const TimeMeasurement &getUpdateTimeMeasurement() const {
		return _update_tm;
	}
	const TimeMeasurement &getNewImageTimeMeasurement() const {
		return _new_tm;
	}

private:
	TimeMeasurement			_render_tm;
	TimeMeasurement			_update_tm;
	TimeMeasurement			_new_tm;
};

}}
