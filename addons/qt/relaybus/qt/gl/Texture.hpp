/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include "../qt_global.hpp"
#include <QOpenGLFunctions>
#include <memory>
#include <string>
#include <vector>

namespace relaybus { namespace gl {

class RELAYBUS_QT_API Texture : QOpenGLFunctions {
public:
	Texture(std::size_t size=1) {
		initializeOpenGLFunctions();
		_textures.resize(size);
		glGenTextures(_textures.size(), _textures.data());
	}
	Texture(const Texture&) =delete;
	Texture& operator=(const Texture&) =delete;

	virtual ~Texture() {
		glDeleteTextures(_textures.size(), _textures.data());
	}

	GLuint id(std::size_t id=0) {
		return _textures[id];
	}

	void bind(std::size_t id=0) {
		glBindTexture(GL_TEXTURE_2D, _textures[id]);
	}

private:
	std::vector<GLuint> _textures;
};

}}
