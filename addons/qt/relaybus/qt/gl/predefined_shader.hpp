 /**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <string>

static const std::string vertex_shader =
		"attribute mediump vec2 vert;\n"
		"attribute mediump vec2 texcoord0;\n"
		"attribute mediump vec2 texcoord1;\n"
		"attribute mediump vec2 texcoord2;\n"
		"attribute mediump vec2 texcoord3;\n"
		"varying mediump vec2 coord0;\n"
		"varying mediump vec2 coord1;\n"
		"varying mediump vec2 coord2;\n"
		"varying mediump vec2 coord3;\n"
		"void main(void) {\n"
		"	coord0=texcoord0;\n"
		"	coord1=texcoord1;\n"
		"	coord2=texcoord2;\n"
		"	coord3=texcoord3;\n"
		"	gl_Position = vec4(vert, 0., 1.0);\n"
		"}\n";

#define RELAYBUS_DEFINE_SHADER(name, source) \
	static const std::string fragment_shader_ ## name = \
		"uniform sampler2D tex0;\n" \
		"uniform sampler2D tex1;\n" \
		"uniform sampler2D tex2;\n" \
		"uniform sampler2D tex3;\n" \
		"varying mediump vec2 coord0;\n" \
		"varying mediump vec2 coord1;\n" \
		"varying mediump vec2 coord2;\n" \
		"varying mediump vec2 coord3;\n" \
		"void main(void) {\n" \
		source \
		"}\n";

RELAYBUS_DEFINE_SHADER(rgb24,
	"gl_FragColor = texture2D(tex0, coord0);\n"
)

RELAYBUS_DEFINE_SHADER(luminance,
	"gl_FragColor = vec4(texture2D(tex0, coord0).rrr, 1.);\n"
)

// scale the depth to the sensor range
/*"   float f = (int)(depth/(1./6.));"
"   float f = (int)(depth/(1./6.));"
"   if       (depth < 1./6.) {\n"
"   } else if(depth < 2./6.) {\n"
"   } else if(depth < 3./6.) {\n"
"   } else if(depth < 4./6.) {\n"
"   } else if(depth < 5./6.) {\n"
"   } else {\n"
"   }\n"*/
RELAYBUS_DEFINE_SHADER(depth,
	"mediump float depth = texture2D(tex0, coord0).r;\n"
	"depth *= 65536.0/10000;\n"
	"gl_FragColor = vec4(depth, depth, depth, 1.);\n"
)

RELAYBUS_DEFINE_SHADER(yuv420p,
	"mediump float y = texture2D(tex0, coord0).r;\n"
	"mediump float u = texture2D(tex1, coord1).r;\n"
	"mediump float v = texture2D(tex2, coord2).r;\n"

	"y=1.1643*(y-0.0625);\n"
	"u=u-0.5;\n"
	"v=v-0.5;\n"

	"mediump float r=y+1.5958*v;\n"
	"mediump float g=y-0.39173*u-0.81290*v;\n"
	"mediump float b=y+2.017*u;\n"

	"gl_FragColor = vec4(r,g,b,1.0);\n"
)
