/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include "Renderer.h"
#include "Relaybus.h"

#include <qimage.h>
#include <qqml.h>
#include <qqmlengine.h>
#include <qqmlextensionplugin.h>

QT_BEGIN_NAMESPACE

class QtQuickRelaybusPlugin : public QQmlExtensionPlugin
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
	void registerTypes(const char *uri);
	void initializeEngine(QQmlEngine *engine, const char *uri);
};

void QtQuickRelaybusPlugin::registerTypes(const char *uri)
{
	qmlRegisterType<relaybus::Relaybus>(uri, 1, 0, "Relaybus");
	qmlRegisterType<Renderer>(uri, 1, 0, "Renderer");
}

void QtQuickRelaybusPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
	EXTENSION_SYSTEM_UNUSED(engine); EXTENSION_SYSTEM_UNUSED(uri);
}

QT_END_NAMESPACE

#include "plugin.moc"
