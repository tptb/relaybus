/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @date      2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <extension_system/macros.hpp>

#ifdef RELAYBUS_QT_EXPORTS
#	define RELAYBUS_QT_API EXTENSION_SYSTEM_EXPORT
#else
#	define RELAYBUS_QT_API EXTENSION_SYSTEM_IMPORT
#endif
