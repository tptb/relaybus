relaybus.addSearchDirectory(".");
relaybus.addSearchDirectory("../lib");

relaybus.createRelay("/test/source", "relaybus::Source2");
relaybus.createRelay("/test/sink", "relaybus::Sink2");
relaybus.connectPins("/test/sink/int32", "/test/source/int32");
