relaybus.addSearchDirectory(".");
relaybus.addSearchDirectory("../lib");

relaybus.createRelay("/test/source", "relaybus::Source");
relaybus.createRelay("/test/sink", "relaybus::Sink");

relaybus.setPin("/test/source/debug", "true");
relaybus.setPin("/test/sink/debug", "true");

relaybus.setPin("/test/sink/bool", "true");
relaybus.setPin("/test/sink/int32", "42");
relaybus.setPin("/test/sink/double", "42.42");
relaybus.setPin("/test/sink/string", "Teststring");
relaybus.setPin("/test/sink/string_2", "Component Test String");

relaybus.connectPins("/test/sink/bool", "/test/source/bool");
relaybus.connectPins("/test/sink/int32", "/test/source/int32");
relaybus.connectPins("/test/sink/double", "/test/source/double");
relaybus.connectPins("/test/sink/string", "/test/source/string");
relaybus.connectPins("/test/sink/image", "/test/source/image");
