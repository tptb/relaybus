/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
import QtQuick 2.1

import Relaybus 1.0

Item {
	id: imageItem

	// possible values are 0=Native, 1=ScaledPreserveAspectFit
	property int fillMode: 1

	property alias path: renderer.path
	property alias forceFramerate: renderer.forceFramerate

	function scaleImage() {
		if(fillMode==0) {
			renderer.scale = 1;
		} else if(fillMode==1){
			renderer.scale = Math.min(width / renderer.imageSize.width, height / renderer.imageSize.height);
		} else {
			console.log("unknown fillMode value " + fillMode);
			renderer.scale = 1;
		}
	}

	clip: true // required to show the image without scaling

	onFillModeChanged: scaleImage()

	onWidthChanged: scaleImage();
	onHeightChanged: scaleImage();

	Renderer {
		objectName: "Video"
		id: renderer
		anchors.centerIn: parent
		clip: true

		onImageSizeChanged: {
			height = imageSize.height;
			width = imageSize.width;
			parent.scaleImage();
		}

	}
}
