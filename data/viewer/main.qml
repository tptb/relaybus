/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
import QtQuick 2.1
import QtQuick.Dialogs 1.0

import Relaybus 1.0

Rectangle {
	id: window
	color: "black"
	width: 800
	height: 600

	property bool imagestab: false

	FileDialog {
		id: fileDialog
		title: "Please choose a video file"
		onAccepted: {
			var filename = fileDialog.fileUrl.toString().substr(7);
			relaybus.setPin("/streamreader/uri", filename);
		}
	}

	Relaybus {
		id: relaybus
		Component.onCompleted: {
			addSearchDirectory(".");
			addSearchDirectory("../lib");


			switch(1) {
			case 0:
				createRelay("/streamreader", "relaybus::ffmpeg::StreamReader");
				break;
			case 1:
				createRelay("/streamreader", "relaybus::image::V4L2");
				setPin("/streamreader/uri", "/dev/video0");
				break;
			case 2:
				createRelay("/streamreceiver", "relaybus::UdpMulticastH264Receiver");
				relaybus.setPin("/streamreceiver/multicast_address", "233.1.15.175");
				relaybus.setPin("/streamreceiver/multicast_port", "12345");

				createRelay("/streamreader", "relaybus::ffmpeg::StreamReader");
				relaybus.setPin("/streamreader/uri", "relaybus_use_input_packet");
				connectPins("/streamreader/input_packet", "/streamreceiver/stream/packet");
				break;
			}
			createRelay("/decoder", "relaybus::ffmpeg::Decoder");
			createRelay("/roi", "relaybus::image::Roi");
			createRelay("/image_stabilization", "relaybus::opencv::ImageStabilization");
			createRelay("/output", "relaybus::ImageOutput");

			connectPins("/decoder/stream/information", "/streamreader/stream/information/0");
			connectPins("/decoder/stream/packet", "/streamreader/stream/packet");
			connectPins("/roi/input", "/decoder/image");
			connectPins("/image_stabilization/input", "/roi/output");
			connectPins("/output/image", "/image_stabilization/output");

		}
	}

	RelaybusImageItem {
		width: window.width
		height: window.height
		path: "/output"

		//forceFramerate: 30

		focus: true
		Keys.onPressed: {
			if(event.key == Qt.Key_O) {
				fileDialog.open();
			} else if(event.key == Qt.Key_Escape) {
				Qt.quit();
			} else if(event.key == Qt.Key_S) {
				imagestab = !imagestab;
				relaybus.setPin("/image_stabilization/enable", imagestab?"true":"false");
			} else if(event.key == Qt.Key_F) {
				if(viewer.isFullScreen())
					viewer.showNormal();
				else
					viewer.showFullScreen();
			}
		}
	}

	Rectangle {
		visible: imagestab
		color: "red"
		width: 5
		height: width
		radius: width*0.5
	}
}
