/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include "ZeroCopy.hpp"
#include <mutex>
#include <condition_variable>
#include <functional>
#include "Optional.hpp"
#include <queue>

namespace relaybus {

	template<typename T>
	class Buffer {
	public:
		Buffer() : _max_size(1), _enable_drop(true), _exit(false) {}
		Buffer(const Buffer&) =delete;
		Buffer& operator=(const Buffer&) =delete;

		~Buffer() {
			exit();
		}

		void reset() {
			// TODO: We should handle this in a better way.
			_exit = false;
		}

		void exit() {
			_exit = true;

			_cond_removed.notify_all();
			_cond_new.notify_all();

			clear();
		}

		void clear() {
			std::lock_guard<std::mutex> lock(_mutex);
			_data = std::queue<ZeroCopy<T> >();
		}

		void setMaxSize(std::size_t size) {
			if(size < 1)
				throw std::runtime_error("size has to be larger than 0");
			std::lock_guard<std::mutex> lock(_mutex);
			_max_size = size;
		}

		std::size_t getMaxSize() const {
			std::lock_guard<std::mutex> lock(_mutex);
			return _max_size;
		}

		void setEnableDrop(bool enable_drop) {
			std::lock_guard<std::mutex> lock(_mutex);
			_enable_drop = enable_drop;
		}

		bool getEnableDrop() const {
			std::lock_guard<std::mutex> lock(_mutex);
			return _enable_drop;
		}

		void push(const ZeroCopy<T> &n) {
			if(_exit)
				return;
			{
				std::unique_lock<std::mutex> lock(_mutex);
				if(!isNotFull()) {
					if(_enable_drop) {
						_data.pop();
					} else {
						_cond_removed.wait(lock, std::bind(&Buffer::isNotFull, this));
					}
				}

				_data.push(n);
			}
			_cond_new.notify_one();
			if(_push_callback)
				_push_callback();
		}

		template<typename duration_type>
		bool push(const ZeroCopy<T> &n, duration_type const& wait_duration) {
			if(_exit)
				return false;
			{
				std::unique_lock<std::mutex> lock(_mutex);
				if(!isNotFull()) {
					if(_enable_drop) {
						_data.pop();
					} else {
						if(!_cond_removed.wait_for(lock, wait_duration, std::bind(&Buffer::isNotFull, this)) || _exit)
							return false;
					}
				}

				if(_exit)
					return false;

				_data.push(n);
			}
			_cond_new.notify_one();
			if(_push_callback)
				_push_callback();
			return true;
		}

		/**
		 * @brief getNew
		 *		you have to free the returned object using delete
		 *		may return a nullptr if the buffer is destroyed
		 *
		 */
		Optional<ZeroCopy<T>> pop() {
			Optional<ZeroCopy<T>> current;
			if(_exit)
				return current;
			{
				std::unique_lock<std::mutex> lock(_mutex);
				_cond_new.wait(lock, std::bind(&Buffer::isNotEmpty, this));

				if(_exit)
					return current;

				 current = Optional<ZeroCopy<T>>(_data.front());
				_data.pop();
			}
			_cond_removed.notify_one();
			if(_pop_callback)
				_pop_callback();
			return current;
		}

		/**
		 * pop with timeout
		 */
		template<typename duration_type>
		Optional<ZeroCopy<T>> pop(duration_type const& wait_duration) {
			Optional<ZeroCopy<T>> current;
			if(_exit)
				return current;
			{
				std::unique_lock<std::mutex> lock(_mutex);
				if(!_cond_new.wait_for(lock, wait_duration, std::bind(&Buffer::isNotEmpty, this)) || _exit)
					return current;

				 current = Optional<ZeroCopy<T>>(_data.front());
				_data.pop();
			}
			_cond_removed.notify_one();
			if(_pop_callback)
				_pop_callback();
			return current;
		}

		bool hasBufferedElements() const {
			std::lock_guard<std::mutex> lock(_mutex);
			return isNotEmpty();
		}

		std::size_t bufferedElementCount() const {
			std::lock_guard<std::mutex> lock(_mutex);
			return _data.size();
		}

		// don't call this function while the buffer is in use
		void setPushCallback(const std::function<void()> &callback) {
			_push_callback = callback;
		}

		// don't call this function while the buffer is in use
		void setPopCallback(const std::function<void()> &callback) {
			_pop_callback = callback;
		}

	private:

		bool isNotEmpty() const { return !_data.empty() || _exit; }
		bool isNotFull() const { return _data.size() < _max_size || _exit; }

		mutable std::mutex _mutex;
		std::condition_variable _cond_new;
		std::condition_variable _cond_removed;

		std::size_t _max_size;
		bool _enable_drop;
		bool _exit;

		std::queue<ZeroCopy<T> > _data;

		// called if an element was pushed into the Buffer
		// This callback should not block and make any assumptions in which
		// thread the function will be called
		std::function<void()> _push_callback;

		std::function<void()> _pop_callback;
	};

}
