/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include "Bus.hpp"
#include "Relay.hpp"

#include <relaybus/Vector.hpp>

#include <relaybus/MakeString.hpp>
#include <cassert>


#include <extension_system/string.hpp>

// TODO: remove the following line
#include <relaybus/image/PixelFormat.hpp>

using namespace relaybus;

Bus::Bus() {
}

Bus::~Bus() {
	unpublishAllRelays();
}

// TODO: check if a pin is not a real substring of another pin
//std::vector<std::string> strs;
//boost::split(strs, "string to split", boost::is_any_of("\t "));

void Bus::publish(const std::string &path, const std::shared_ptr<Relay> &component) {
	std::lock_guard<std::mutex> lock(_components_mutex);
	if(_components.find(path) != _components.end())
		throw std::runtime_error("path is already in use");
	component->publishComponent(this, path);
	_components[path] = component;
}

void Bus::unpublish(const std::shared_ptr<Relay> &component) {
	std::lock_guard<std::mutex> lock(_components_mutex);
	auto iter = _components.find(component->getPath());
	if(iter == _components.end())
		throw std::runtime_error("relay is not published");
	component->unpublishComponent();
	_components.erase(iter);
}

void Bus::unpublishAllRelays() {
	std::lock_guard<std::mutex> lock(_components_mutex);

	// unpublish all relays
	for(auto &iter :_components) {
		iter.second->unpublishComponent();
	}

	// if there are no further references to them, destroy them
	for(auto iter = std::begin(_components); iter != std::end(_components);) {
		iter = _components.erase(iter);
	}
}

void Bus::publish(Relay *component, Pin *pin) {
	if(!pin->isPinValid())
		throw std::runtime_error("can not publish invalid property");
	if(pin->isPublished())
		throw std::runtime_error("can not publish already published property");

	const std::string path = component->getPath() + "/" + pin->getName();

	{
		std::lock_guard<std::mutex> lock(_pins_mutex);
		if(_pins.find(path) != _pins.end())
			throw std::runtime_error("path is already in use");
		_pins[path] = pin;
	}

	pin->published(path);

	// apply pending set operations
	{
		std::lock_guard<std::mutex> lock(_pending_set_operations_mutex);
		auto iter = _pending_set_operations.find(path);
		if(iter != _pending_set_operations.end()) {
			for(auto i = std::begin(iter->second); i != std::end(iter->second); ) {
				set(pin, *i);
				i = iter->second.erase(i);
			}
		}
	}

	// check if there are open wires
	{
		std::lock_guard<std::mutex> lock(_wires_mutex);

		auto in_iter = _wires_in.find(path);
		if(in_iter != _wires_in.end()) {
			connectPins(in_iter->second);
		}

		auto out_iter = _wires_out.equal_range(path);
		for (auto it = out_iter.first; it != out_iter.second; ++it) {
			connectPins(it->second);
		}
	}
}

void Bus::unpublish(Pin *pin) {
	const std::string path = pin->getPath();

	{
		std::lock_guard<std::mutex> lock(_pins_mutex);
		_pins.erase(path);
	}

	// check if there are wires that have to be removed
	{
		std::lock_guard<std::mutex> lock(_wires_mutex);

		auto in_iter = _wires_in.find(path);
		if(in_iter != _wires_in.end()) {
			disconnectPins(in_iter->second);
		}

		auto out_iter = _wires_out.equal_range(path);
		for (auto it = out_iter.first; it != out_iter.second; ++it) {
			disconnectPins(it->second);
		}
	}

	pin->unpublished();
}

bool Bus::connectPins(const Wire &wire) {
	std::lock_guard<std::mutex> lock(_pins_mutex);
	auto source = _pins.find(wire.source());
	auto destination = _pins.find(wire.destination());

	// check if _source and _destination are valid pins
	if(source != _pins.end() && destination != _pins.end()) {
		IInput *dest = dynamic_cast<IInput*>(destination->second);
		if(dest) {

			// check if the pins are already connected
			if(source->second->hasOutgoingWire(dest)) {
				reportWarning(nullptr, "connectPins: pins are already wired");
				return true;
			}

			dest->setProfile(wire.profile());

			// set the current_value
			// TODO: check if this is really correct
			dest->addInitalValueToInputBuffer(source->second->getCurrentBaseOutput());

			source->second->addOutgoingWire(dest);
		} else
			reportError(nullptr, "the destination is no input pin");
	}

	return true;
}

bool Bus::connect(const Wire &wire) {
	{
		std::lock_guard<std::mutex> lock(_wires_mutex);

		if(wire.source() == wire.destination())
			throw std::runtime_error("can not connect a pin to himself");

		auto iter = _wires_in.find(wire.destination());
		if(iter != _wires_in.end()) {
			if(iter->second.source() == wire.source())
				throw std::runtime_error("the connection already exists");
			else
				throw std::runtime_error(MakeString() << "the destination is already connected with " << wire.source());
		}

		_wires_in[wire.destination()] = wire;

		 _wires_out.emplace(wire.source(), wire);
	}

	return connectPins(wire);
}

bool Bus::disconnectPins(const Wire &wire) {
	std::lock_guard<std::mutex> pin_lock(_pins_mutex);
	auto source = _pins.find(wire.source());
	auto destination = _pins.find(wire.destination());

	// check if _source and _destination are valid pins
	if(source != _pins.end() && destination != _pins.end()) {
		IInput *dest = dynamic_cast<IInput*>(destination->second);
		if(dest)
			source->second->removeOutgoingWire(dest);
		else
			reportError(nullptr, "the destination is no input pin");
	}

	return true;
}

bool Bus::disconnect(const Wire &wire) {
	std::lock_guard<std::mutex> wire_lock(_wires_mutex);

	if(_wires_in.find(wire.destination()) == _wires_in.end())
		throw std::runtime_error("wire doesn't exist");

	_wires_in.erase(wire.destination());

	auto iter = _wires_out.equal_range(wire.source());
	for (auto it = iter.first; it != iter.second; ++it) {
		if(it->second.destination() == wire.destination())
			_wires_out.erase(it);
	}

	return disconnectPins(wire);
}

bool Bus::set(const std::string &destination, const std::string &value, bool setIfPossible) {
	std::lock_guard<std::mutex> lock(_pins_mutex);

	auto iter = _pins.find(destination);
	if(iter == _pins.end()) {
		if(setIfPossible) {
			std::lock_guard<std::mutex> pending_lock(_pending_set_operations_mutex);
			_pending_set_operations[destination].push_back(value);
		}
		return false;
	} else {
		set(iter->second, value);
		return true;
	}
}

template<typename T>
inline static bool setInputValue(Pin *pin, IInput *input, const std::string &value) {
	if(pin->getType() == TypeSystem<T>::get()) {
		T data;
		std::stringstream sstr;
		sstr << value;
		sstr >> data;

		if(sstr.fail()) // TODO: handle this
			return true; // TODO: why true?

		// TODO: we should ensure that this doesn't block forever
		input->addValueToInputBuffer(ZeroCopy<T>(data));

		return true;
	}
	return false;
}

static std::vector<std::string> split(const std::string &str, const std::string &delimiter) {
	std::vector<std::string> splitted;
	extension_system::split(str, delimiter, [&](const std::string &s) {
		splitted.push_back(s);
		return true;
	});
	return splitted;
}

void Bus::set(Pin *pin, const std::string &value) {
	IInput *input = dynamic_cast<IInput*>(pin);
	if(input == nullptr) {
		reportError(nullptr, "pin is no input pin");
		return;
	}

	if(pin->getType() == TypeSystem<std::string>::get()) {
		input->addValueToInputBuffer(ZeroCopy<std::string>(value));
	} else if(pin->getType() == TypeSystem<bool>::get()) {
		if(value == "true")
			input->addValueToInputBuffer(ZeroCopy<bool>(true));
		else if(value == "false")
			input->addValueToInputBuffer(ZeroCopy<bool>(false));
		else
			reportError(nullptr, MakeString() << "set doesn't know how to set a bool to " << value);
	} else if(setInputValue<int8_t>(pin, input, value)) {
	} else if(setInputValue<uint8_t>(pin, input, value)) {
	} else if(setInputValue<int16_t>(pin, input, value)) {
	} else if(setInputValue<uint16_t>(pin, input, value)) {
	} else if(setInputValue<int32_t>(pin, input, value)) {
	} else if(setInputValue<uint32_t>(pin, input, value)) {
	} else if(setInputValue<int64_t>(pin, input, value)) {
	} else if(setInputValue<uint64_t>(pin, input, value)) {
	} else if(setInputValue<float>(pin, input, value)) {
	} else if(setInputValue<double>(pin, input, value)) {
	} else if(pin->getType() == TypeSystem<relaybus::Vector_2d>::get()) {
		auto splitted = split(value, " ");
		if(splitted.size() == 2) {
			double x = 0, y = 0;
			try {
				x = std::stod( splitted[0] );
			} catch( std::exception & ) {
				reportError(nullptr, "couldn't parse x argument for Vector_2d");
			}
			try {
				y = std::stod( splitted[1] );
			} catch( std::exception & ) {
				reportError(nullptr, "couldn't parse y argument for Vector_2d");
			}
			input->addValueToInputBuffer(ZeroCopy<Vector_2d>(Vector_2d(x, y)));
		} else {
			reportError(nullptr, "invalid value for Vector_2d, expected \"num1 num2\" e.g. 1.1 2.1");
		}
	} else if(pin->getType() == TypeSystem<relaybus::Rectangle_d>::get()) {
		auto splitted = split(value, " ");
		if(splitted.size() == 4) {
			double x = 0, y = 0, width = 0, height = 0;
			try {
				x = std::stod( splitted[0] );
			} catch( std::exception & ) {
				reportError(nullptr, "couldn't parse x argument for Rectangle_d");
			}
			try {
				y = std::stod( splitted[1] );
			} catch( std::exception& ) {
				reportError(nullptr, "couldn't parse y argument for Rectangle_d");
			}
			try {
				width = std::stod( splitted[2] );
			} catch( std::exception& ) {
				reportError(nullptr, "couldn't parse width argument for Rectangle_d");
			}
			try {
				height = std::stod( splitted[3] );
			} catch( std::exception& ) {
				reportError(nullptr, "couldn't parse height argument for Rectangle_d");
			}
			input->addValueToInputBuffer(ZeroCopy<Rectangle_d>(Rectangle_d(Vector_2d(x, y), Vector_2d(width, height))));
		} else {
			reportError(nullptr, "invalid value for Rectangle_d");
		}
	} else if(pin->getType() == TypeSystem<relaybus::Rectangle_size_t>::get()) {
		auto splitted = split(value, " ");
		if(splitted.size() == 4) {
			std::size_t x = 0, y = 0, width = 0, height = 0;
			try {
				x = std::stod( splitted[0] );
			} catch( std::exception& ) {
				reportError(nullptr, "couldn't parse x argument for Rectangle_d");
			}
			try {
				y = std::stod( splitted[1] );
			} catch( std::exception& ) {
				reportError(nullptr, "couldn't parse y argument for Rectangle_d");
			}
			try {
				width = std::stod( splitted[2] );
			} catch( std::exception& ) {
				reportError(nullptr, "couldn't parse width argument for Rectangle_d");
			}
			try {
				height = std::stod( splitted[3] );
			} catch( std::exception& ) {
				reportError(nullptr, "couldn't parse height argument for Rectangle_d");
			}
			input->addValueToInputBuffer(ZeroCopy<Rectangle_size_t>(Rectangle_size_t(Vector_2_size_t(x, y), Vector_2_size_t(width, height))));
		} else {
			reportError(nullptr, "invalid value for Rectangle_d");
		}
	} else if(pin->getTypeString() == "relaybus::RPixelformat::Format") {
		// TODO: handle this in a more generic way
		if(value == "RPixelformat_RGB24") {
			input->addValueToInputBuffer(ZeroCopy<image::RPixelformat::Format>(image::RPixelformat::RPixelformat_RGB24));
		} else if(value == "RPixelformat_NONE"){
			input->addValueToInputBuffer(ZeroCopy<image::RPixelformat::Format>(image::RPixelformat::RPixelformat_NONE));
		} else if(value == "RPixelformat_RGBA"){
			input->addValueToInputBuffer(ZeroCopy<image::RPixelformat::Format>(image::RPixelformat::RPixelformat_RGBA));
		} else {
			reportError(nullptr, "set can not handle this pixel format");
		}
	} else {
		reportError(nullptr, "set can not handle this type");
	}
}

std::shared_ptr<Relay> Bus::getRelay(const std::string &name) {
	std::lock_guard<std::mutex> lock(_components_mutex);
	auto iter = _components.find(name);
	if(iter == _components.end())
		return std::shared_ptr<Relay>();
	else
		return iter->second;
}


void Bus::reportError(Relay *component, const std::string &msg) {
	EXTENSION_SYSTEM_UNUSED(component);
	std::cerr<<"Error: "<<msg<<"\n";
}

void Bus::reportWarning(Relay *component, const std::string &msg) {
	EXTENSION_SYSTEM_UNUSED(component);
	std::cerr<<"Warning: "<<msg<<"\n";
}

void Bus::reportInformation(Relay *component, const std::string &msg) {
	EXTENSION_SYSTEM_UNUSED(component);
	std::cout<<"Info: "<<msg<<"\n";
}

static std::mutex _singleton_mutex;
static std::shared_ptr<Bus> _singleton_instance;

std::shared_ptr<Bus> Bus::getSingleton() {
	std::unique_lock<std::mutex> lock(_singleton_mutex);
	if(_singleton_instance.get() == nullptr)
		_singleton_instance = std::make_shared<Bus>();
	return _singleton_instance;
}
