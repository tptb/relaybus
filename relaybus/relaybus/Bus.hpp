/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>

#include <string>
#include <vector>
#include <stdexcept>
#include <mutex>
#include <memory>

#include <iostream>
#include <relaybus/thread.hpp>
#include <functional>

#include <unordered_map>

#include "Wire.hpp"

namespace relaybus {

class Relay;
class Pin;
struct BusObserver;

/**
 * @note threadsafe
 */
class RELAYBUS_API Bus {
public:
	Bus();
	Bus(const Bus&) =delete;
	Bus& operator=(const Bus&) =delete;
	~Bus();

	// returns a shared_ptr to a singleton instance of the bus
	static std::shared_ptr<Bus> getSingleton();

	/**
	 * @brief publish
	 *	there are two different types of publications:
	 *	/...  -> publish a property, only possible if a component was previously published
	 *	/.../ -> publish a component
	 * @param path
	 * @param component
	 */
	void publish(const std::string &path, const std::shared_ptr<Relay> &component);
	void unpublish(const std::shared_ptr<Relay> &component);
	void unpublishAllRelays();

	/**
	 * @brief publish
	 * @param component
	 * @param property
	 */
	void publish(Relay *component, Pin *property);
	void unpublish(Pin *property);

	bool connect(const Wire &wire);
	bool disconnect(const Wire &wire);

	/**
	 * set a relaybus path
	 * The value is converted into the type of the destination currently supported types are
	 * std::string, int8_t, uint8_t, int16_t, uint16_t, int32_t, uint32_t, int64_t, uint64_t, float, double
	 * @param setIfPossible set the parameter when the destination path appears
	 */
	bool set(const std::string &destination, const std::string &value, bool setIfPossible=true);

	void set(Pin *pin, const std::string &value);

	std::shared_ptr<Relay> getRelay(const std::string &name);

private:
	std::string									_bus_name;

	std::mutex									_wires_mutex;
	std::unordered_map<std::string, Wire>		_wires_in;
	std::unordered_multimap<std::string, Wire>	_wires_out;

	std::mutex													_components_mutex;
	std::unordered_map<std::string, std::shared_ptr<Relay> >	_components;

	std::mutex								_pins_mutex;
	std::unordered_map<std::string, Pin*>	_pins;

	std::mutex													_pending_set_operations_mutex;
	std::unordered_map<std::string, std::vector<std::string> >	_pending_set_operations;

	friend class Relay;

	/**
	 * @brief reportError
	 * @param component can be a nullptr
	 * @param msg
	 */
	void reportError(Relay *component, const std::string &msg);

	/**
	 * @brief reportWarning
	 * @param component can be a nullptr
	 * @param msg
	 */
	void reportWarning(Relay *component, const std::string &msg);

	/**
	 * @brief reportInformation
	 * @param component can be a nullptr
	 * @param msg
	 */
	void reportInformation(Relay *component, const std::string &msg);

	bool connectPins(const Wire &wire);
	bool disconnectPins(const Wire &wire);
};

}
