/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <memory>

namespace relaybus {

template<typename T>
class Clone {
public:
	typedef T InputType;
	typedef T ResultType;
	typedef std::shared_ptr<ResultType> ResultTypePtr;

	/**
	  * Helper function to clone a class instance
	  * Required to clone objects that doesn't have a cctor or
	  * have to be cloned differently. Like the BaseImageView where
	  * a cloned instance should be a BaseImage.
	  * This function is called in ZeroCopy::rw()
	  */
	static ResultTypePtr clone(const InputType &org) {
		return std::make_shared<ResultType>(org);
	}

	/**
	 * The main difference between clone and fast_clone is
	 * that fast clone should not copy all data e.g.
	 * for cloning an image the image data itself should not be copied into the new instance
	 * This function is called in ZeroCopy::w()
	 */
	static ResultTypePtr fast_clone(const InputType &org) {
		return std::make_shared<ResultType>(org);
	}
};
}
