/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include "Host.hpp"
#include "Bus.hpp"
#include "Relay.hpp"
#include <extension_system/ExtensionSystem.hpp>

#include <stdexcept>
#include <string>
#include <relaybus/MakeString.hpp>

using namespace relaybus;
using namespace extension_system;

Host::Host(const std::shared_ptr<ExtensionSystem> &extension_system, const std::shared_ptr<Bus> &bus) : _extension_system(extension_system), _bus(bus) {}

Host::~Host() {
	destroyAll();
}

void Host::create(const std::string &path, const std::string &type) {
	if(_components.find(path) != _components.end())
		throw std::runtime_error("path is already in use by this Host");

	std::shared_ptr<Relay> relay = _extension_system->createExtension<Relay>(type);

	if(relay == nullptr)
		throw std::runtime_error(MakeString() << "couldn't create relay " << type << " (path=" << path << ")");

	// check if the relaybus version matches
	ExtensionDescription desc = _extension_system->findDescription(relay);

	if(!desc.isValid())
		throw std::runtime_error(MakeString() << "couldn't get description from relay path=" << path << " type="<< type);

	if(desc["relaybus_version"] != EXTENSION_SYSTEM_STR(RELAYBUS_VERSION))
		throw std::runtime_error("relaybus version doesn't match");

	if(relay == nullptr)
		throw std::runtime_error("couldn't load relay");

	_bus->publish(path, relay);

	// if everything was successful (no exceptions has been thrown), insert relay into known components
	_components.insert(std::make_pair(path, relay));

}

void Host::destroy(const std::string &path) {
	auto iter = _components.find(path);
	if(iter == _components.end())
		throw std::runtime_error("path is not managed by this instance or doesn't exist");

	_bus->unpublish(iter->second);
	_components.erase(iter);
}

void Host::destroyAll() {
	for(auto iter : _components) {
		_bus->unpublish(iter.second);
	}

	for(auto iter = std::begin(_components); iter != std::end(_components);) {
		iter = _components.erase(iter);
	}
}

const std::shared_ptr<Bus> &Host::getBus() const
{
	return _bus;
}

const std::shared_ptr<ExtensionSystem> &Host::getExtensionSystem() const
{
	return _extension_system;
}
