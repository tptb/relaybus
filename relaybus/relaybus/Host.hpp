/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>

#include <string>
#include <unordered_map>
#include <relaybus/thread.hpp>
#include <mutex>
#include <memory>

namespace extension_system {
	class ExtensionSystem;
}

namespace relaybus {

class Bus;
class Relay;

/**
 * @brief The Host class
 * @remarks Class is not thread-safe (at least yet)
 */
class RELAYBUS_API Host final {
public:

	Host(const std::shared_ptr<extension_system::ExtensionSystem> &extension_system, const std::shared_ptr<Bus> &bus);
	~Host();

	void create(const std::string &path, const std::string &type);
	void destroy(const std::string &path);

	void destroyAll();

	const std::shared_ptr<Bus> &getBus() const;

	const std::shared_ptr<extension_system::ExtensionSystem> &getExtensionSystem() const;

private:
	std::shared_ptr<extension_system::ExtensionSystem>	_extension_system;
	std::shared_ptr<Bus>				_bus;

	std::mutex					_components_mutex;
	std::unordered_map<std::string, std::shared_ptr<Relay> >	_components;

	Host( const Host& );
	const Host& operator=( const Host& );
};

}
