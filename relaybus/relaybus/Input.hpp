/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include "Output.hpp"

namespace relaybus {

template<typename T, typename TProfile>
class Input : public Output<T>, public IInput {
public:
	Input() : IInput(TProfile::value()) {}

	void published(const std::string &path) override {
		_input_buffer.reset();
		Output<T>::published(path);
	}
	void unpublished() override {
		_input_buffer.exit();
		Output<T>::unpublished();
	}
	void exit() override {
		_input_buffer.exit();
		Pin::exit();
	}

	bool addValueToInputBuffer(const BaseZeroCopy &other) override {
		const ZeroCopy<T> *o = dynamic_cast<const ZeroCopy<T>*>(&other);
		if(o == nullptr)
			return false;
		_input_buffer.push(*o);
		return true;
	}

	// TODO: find a better way
	bool addInitalValueToInputBuffer(const BaseZeroCopy &other) override {
		const ZeroCopy<T> *o = dynamic_cast<const ZeroCopy<T>*>(&other);
		if(o == nullptr)
			return false;
		return _input_buffer.push(*o, std::chrono::milliseconds(0));
	}

	/**
	 * @brief has_changed
	 * @return true if the property was changed remotely
	 */
	bool hasPendingUpdates() const override {
		return _input_buffer.hasBufferedElements();
	}

	std::size_t pendingUpdateCount() const override {
		return _input_buffer.bufferedElementCount();
	}

	bool update() override {
		auto r = _input_buffer.pop();
		if(r.isValid()) {
			Output<T>::_current = r.data();
			return true;
		} else {
			return false;
		}
	}

	bool updateAndSubmit() override {
		if(update()) {
			Output<T>::submit();
			return true;
		} else {
			return false;
		}
	}

	bool update(const duration &timeout) override {
		auto d = _input_buffer.pop(timeout);
		if(d.isValid()) {
			Output<T>::_current = d.data();
			return true;
		} else {
			return false;
		}
	}

	bool updateAndSubmit(const duration &timeout) {
		bool r = update(timeout);
		if(r)
			Output<T>::submit();
		return r;
	}

	bool tryUpdate() override {
		return update(std::chrono::microseconds::zero());
	}

	bool tryUpdateAndSubmit() override {
		if(tryUpdate()) {
			Output<T>::submit();
			return true;
		}
		return false;
	}

	void setProfile(const WireProfile &p) override {
		WireProfile profile = ((p.type() == WireType::Auto) ? getDefaultProfile() : p);
		switch(profile.type()) {
			case WireType::Blocking:
				_input_buffer.setEnableDrop(false);
			break;
			case WireType::Drop:
				_input_buffer.setEnableDrop(true);
			break;
			case WireType::Auto:
				// TODO: report error
			break;
		}
		_input_buffer.setMaxSize(profile.bufferSize());
	}
	WireProfile getProfile() const override {
		return WireProfile(_input_buffer.getEnableDrop() ? WireType::Drop : WireType::Blocking,
						   _input_buffer.getMaxSize());
	}

	Input<T, TProfile> &operator=(std::nullptr_t) {
		Output<T>::_current = nullptr;
		return *this;
	}

	bool operator==(std::nullptr_t) const {
		return !Output<T>::isValid();
	}

	bool operator!=(std::nullptr_t) const {
		return Output<T>::isValid();
	}

	const std::string &getPath() const override {
		return Output<T>::getPath();
	}

	void setInputBufferPendingUpdateCallback(const std::function<void()> &callback) override {
		_input_buffer.setPushCallback(callback);
	}

protected:
	Buffer<T>		_input_buffer;
};

}
