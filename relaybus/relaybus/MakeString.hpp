/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <sstream>

namespace relaybus {

	class MakeString {
	public:
		std::stringstream stream;
		operator std::string() const { return stream.str(); }

		template<class T>
		MakeString& operator<<(T const& var) { stream << var; return *this; }
	};

}
