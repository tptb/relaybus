/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>
#include <cstddef> // std::nullptr_t

namespace relaybus {

	// TODO: check boost::optional
	// TODO: check if the compiler supports experimemta/optional and use it if possible. drop this code
	template<typename T>
	class Optional {
	public:

		Optional() EXTENSION_SYSTEM_NOEXCEPT : _is_valid(false) {}

		Optional(const Optional<T> &other) EXTENSION_SYSTEM_NOEXCEPT : _data(other._data), _is_valid(other._is_valid) {}
		Optional(Optional<T> &&) = default;

		Optional(const T &data) EXTENSION_SYSTEM_NOEXCEPT : _data(data), _is_valid(true) {}
		Optional(T &&data) EXTENSION_SYSTEM_NOEXCEPT : _data(std::move(data)), _is_valid(true) {
			data._is_valid = false;
		}

		Optional(std::nullptr_t) EXTENSION_SYSTEM_NOEXCEPT : _is_valid(false) {}

		Optional<T>& operator=(Optional<T>&&) = default;
		Optional<T>& operator=(T &&other) {
			_data = std::move(other);
			_is_valid = true;
		}

		Optional<T>& operator=(const Optional<T> &other) {
			_data = other._data;
			_is_valid = other._is_valid;
		}

		Optional<T>& operator=(const T &other) {
			_data = other;
			_is_valid = true;
		}

		Optional<T>& operator=(std::nullptr_t) EXTENSION_SYSTEM_NOEXCEPT {
			_is_valid = false;
		}


		T &data() {
			if(!_is_valid)
				throw std::runtime_error("Optional is invalid");
			return _data;
		}

		const T &data() const {
			if(!_is_valid)
				throw std::runtime_error("Optional is invalid");
			return _data;
		}

		bool isValid() const EXTENSION_SYSTEM_NOEXCEPT {
			return _is_valid;
		}

	private:
		T	_data;
		bool _is_valid;
	};
}
