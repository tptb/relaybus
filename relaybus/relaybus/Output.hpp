/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include "Pin.hpp"
#include <relaybus/Buffer.hpp>

namespace relaybus {

template<typename T>
class Output : public Pin {
public:

	Output() : Pin(TypeSystem<T>::get(), extension_system::InterfaceName<T>::getString()) {
	}


	const T &r() const {
		return _current.r();
	}

	T &c() {
		return _current.c();
	}

	T &f() {
		return _current.f();
	}

	T &w() {
		return _current.w();
	}

	T &rw() {
		return _current.rw();
	}

	ZeroCopy<T> current() {
		return _current;
	}

	Output<T> &operator=(std::nullptr_t) {
		_current = nullptr;
		return *this;
	}

	bool isValid() const override {
		return _current.isValid();
	}

	bool operator==(std::nullptr_t) const {
		return !isValid();
	}

	bool operator!=(std::nullptr_t) const {
		return isValid();
	}

	void submit() override {
		submit(nullptr);
	}

	bool submit(const Optional<std::chrono::milliseconds> &timeout) override {
		// TODO: do we need to handle _published
		// TODO: what should we do if the element is invalid
		// TODO: handle timeout
		// TODO: what should we do if we reach the timeout while we update the receiver pins?
		EXTENSION_SYSTEM_UNUSED(timeout);

		std::lock_guard<std::mutex> lock(_mutex);
		_current_output = _current;

		// TODO: The following can block the whole application if removeOutgoingWire is called
		// link REMOVE-WIRE-DEAD-LOCK
		for(auto &iter : _receiver) {
			if(!iter->addValueToInputBuffer(_current_output)) {
				//std::cerr<<"incompatible Pin type"<<"\n";
			}
		}

		return true; // TODO: should be false if the timeout is reached
	}

	const BaseZeroCopy &getCurrentBaseOutput() const override {
		std::lock_guard<std::mutex> lock(_mutex);
		return _current_output;
	}

	ZeroCopy<T> getCurrentOutput() const {
		std::lock_guard<std::mutex> lock(_mutex);
		return _current_output;
	}

	bool hasPendingUpdates() const override {
		return false;
	}

protected:
	ZeroCopy<T>	_current;
	ZeroCopy<T>	_current_output;
};

}
