/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include "Pin.hpp"
#include <algorithm>

using namespace relaybus;

IInput::~IInput() {}

Pin::Pin(const std::type_info &type_info, const std::string &type_string)
	: _drop_if_not_connected(true)
	, _published(false)
	, _type_info(type_info)
	, _type_string(type_string) {
}

Pin::~Pin() {}


const std::string &Pin::getName() const {
	std::lock_guard<std::mutex> lock(_mutex);
	return _name;
}

void Pin::setName(const std::string &name) {
	std::lock_guard<std::mutex> lock(_mutex);
	if(_published)
		throw std::runtime_error("Pin is currently published, changing the name is not allowed");
	_name = name;
}


const std::string &Pin::getDescription() const {
	std::lock_guard<std::mutex> lock(_mutex);
	return _description;
}
void Pin::setDescription(const std::string &description) {
	std::lock_guard<std::mutex> lock(_mutex);
	_description = description;
}


void Pin::published(const std::string &path) {
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if(_published)
			throw std::runtime_error("Pin is already published, you have to unpublish it first");
		_path = path;
		_published = true;
	}
}
void Pin::unpublished() {
	{
		std::lock_guard<std::mutex> lock(_mutex);
		_path = "";
		_published = false;
	}
	// TODO: stop all blocking operations
}


bool Pin::isPinValid() const {
	std::lock_guard<std::mutex> lock(_mutex);
	return !_name.empty();
}

bool Pin::isPublished() const {
	std::lock_guard<std::mutex> lock(_mutex);
	return _published;
}


const std::type_info &Pin::getType() const {
	return _type_info;
}
const std::string &Pin::getTypeString() const {
	return _type_string;
}
const std::string &Pin::getPath() const {
	return _path;
}


void Pin::addOutgoingWire(IInput *input) {
	std::lock_guard<std::mutex> lock(_mutex);
	auto iter = std::find(std::begin(_receiver), std::end(_receiver), input);
	if(iter != std::end(_receiver))
		throw std::runtime_error("wire does already exist");
	_receiver.push_back(input);

	//setEnableOutputBufferDrop(false);
}

bool Pin::hasOutgoingWire(IInput *input) const {
	std::lock_guard<std::mutex> lock(_mutex);
	auto iter = std::find(std::begin(_receiver), std::end(_receiver), input);
	return iter != _receiver.end();
}

void Pin::removeOutgoingWire(IInput *input) {
	// TODO: The following blocks the whole application
	// link REMOVE-WIRE-DEAD-LOCK
	std::lock_guard<std::mutex> lock(_mutex);
	auto iter = std::find(std::begin(_receiver), std::end(_receiver), input);
	if(iter != std::end(_receiver))
		_receiver.erase(iter);

	//if(_receiver.empty())
	//	setEnableOutputBufferDrop(true);
}

bool Pin::hasOutgoingWires() const {
	std::lock_guard<std::mutex> lock(_mutex);
	return !_receiver.empty();
}
