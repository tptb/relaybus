/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once
#include <relaybus/macros.hpp>

#include <stdexcept>
#include <vector>

#include <relaybus/chrono.hpp>

#include <relaybus/Optional.hpp>
#include <relaybus/TypeSystem.hpp>
#include <relaybus/ZeroCopy.hpp>
#include <mutex>
#include <relaybus/thread.hpp>
#include <functional>

#include "Wire.hpp"

namespace relaybus {

class RELAYBUS_API IInput {
public:
	IInput(const WireProfile &default_profile)
		: _default_profile(default_profile) {
		verifyDefaultProfile(default_profile);
	}
	virtual ~IInput();

	/**
	 * should we move this function in to a minimal input interface
	 */
	virtual bool addValueToInputBuffer(const BaseZeroCopy &other) = 0;
	virtual bool addInitalValueToInputBuffer(const BaseZeroCopy &other) = 0;

	/**
	 * @brief update
	 *	Blocks until the Pin was updated,
	 *	this may block until the Input is connected to a Output
	 */
	virtual bool update() = 0;
	virtual bool updateAndSubmit() = 0;

	virtual bool update(const duration &timeout) = 0;
	virtual bool updateAndSubmit(const duration &timeout) = 0;

	virtual bool tryUpdate() = 0;
	virtual bool tryUpdateAndSubmit() = 0;

	virtual std::size_t pendingUpdateCount() const = 0;

	virtual void setProfile(const WireProfile &profile) = 0;
	virtual WireProfile getProfile() const = 0;

	virtual const std::string &getPath() const = 0;

	virtual void setInputBufferPendingUpdateCallback(const std::function<void()> &callback) = 0;

	void setDefaultProfile(const WireProfile &profile) {
		verifyDefaultProfile(profile);
		_default_profile = profile;
	}

	WireProfile getDefaultProfile() const {
		return _default_profile;
	}

	void resetToDefaultProfile() {
		setProfile(getDefaultProfile());
	}

private:
	inline static void verifyDefaultProfile(const WireProfile &profile) {
		if(profile.type() == WireType::Auto)
			throw std::runtime_error("invalid default profile with type WireType::Auto");
	}

	WireProfile		_default_profile;
};

/**
	@brief The Pin class
	The Pin should be extended to provide at least the following information:
	- added Updates
	- dropped Updates
	- updates fetched with update
	- time between updates
	- buffer size
	- Profile
	- type
	- description
*/
class RELAYBUS_API Pin {
public:

	Pin(const std::type_info &type_info, const std::string &type_string);
	Pin(const Pin&) =delete;
	Pin& operator=(const Pin&) =delete;

	virtual ~Pin();

	const std::string &getName() const;
	void setName(const std::string &name);

	const std::string &getDescription() const;
	void setDescription(const std::string &description);

	virtual void published(const std::string &path);
	virtual void unpublished();

	virtual bool isValid() const = 0;

	bool operator==(std::nullptr_t) const {
		return !isValid();
	}

	bool operator!=(std::nullptr_t) const {
		return isValid();
	}

	bool isPinValid() const;
	bool isPublished() const;

	const std::type_info &getType() const;
	const std::string &getTypeString() const;
	const std::string &getPath() const;

	// Output
	virtual void submit() = 0;
	virtual bool submit(const Optional<std::chrono::milliseconds> &timeout) = 0;

	void addOutgoingWire(IInput *input);
	void removeOutgoingWire(IInput *input);
	bool hasOutgoingWire(IInput *input) const;
	bool hasOutgoingWires() const;

	// Always returns false for Output Pins
	virtual bool hasPendingUpdates() const = 0;

	virtual const BaseZeroCopy &getCurrentBaseOutput() const = 0;

	void setDropIfNotConnected(bool enable) {
		_drop_if_not_connected = enable;
	}

	/**
	 * @brief exit
	 *	invalidates the Pin
	 */
	virtual void exit() {
		std::lock_guard<std::mutex> lock(_mutex);
		_receiver.clear();
	}

protected:

	bool _drop_if_not_connected;

	mutable std::mutex	_mutex;

	bool	_published;
	const std::type_info &_type_info;
	const std::string	_type_string;

	std::string	_name;
	std::string	_description;
	std::string	_path;

	std::vector<IInput*>	_receiver;
};

}
