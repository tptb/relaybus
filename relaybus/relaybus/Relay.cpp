/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include "Relay.hpp"
#include "Bus.hpp"

#include <algorithm>
#include <functional>

using namespace relaybus;

Relay::Relay() : _exit(false), _bus(nullptr), _initialized(false), _pending_updates_flag(false) {}

Relay::~Relay() {
}

void Relay::init() {}

void Relay::deinit() {}

void Relay::main() {
	try {
		init();
	} catch(std::exception &e) {
		std::cerr<<"init exception "<<e.what()<<"\n";
		return;
	} catch(...) {
		std::cerr<<"unknown init exception\n";
	}
	_initialized = true;

	try {
		while(!_exit) {
			process();
		}
	} catch(Component_Exit_Exception &) {
		// todo
	} catch(std::exception &e) {
		std::cerr<<"process exception "<<e.what()<<"\n";
	} catch(...) {
		std::cerr<<"unknown process exception\n";
	}

	try {
		deinit();
	} catch(std::exception &e) {
		std::cerr<<"deinit exception "<<e.what()<<"\n";
	} catch(...) {
		std::cerr<<"unknown deinit exception\n";
	}

	try {
		unpublishAll();
	} catch(std::exception &e) {
		std::cerr<<"unpublishAll exception "<<e.what()<<"\n";
	} catch(...) {
		std::cerr<<"unknown unpublishAll exception\n";
	}

	_initialized=false;
}

void Relay::publishComponent(Bus *bus, const std::string &path) {
	_exit = false;
	_pending_updates_flag = false;
	_bus = bus;
	_path = path;

	_thread = std::thread(&Relay::main, this);
	setThreadName(_thread, "r" + path);
}

void Relay::unpublishComponent() {
	_exit = true;
	_pending_updates_flag = true;
	_sleep_condition_variable.notify_all();
	_pending_updates_cond.notify_one();

	// invalidate all _pins
	{
		std::unique_lock<std::mutex> lock(_pins_mutex);
		for(auto &iter : _pins) {
			iter->exit();
		}
	}

	_thread.join();

	_bus = nullptr;
	_path = "";
}

const std::string &Relay::getPath() const {
	return _path;
}


bool Relay::waitForWires() const {
	// TODO
	return true;
}

bool Relay::havePinPendingUpdates() const {
	std::unique_lock<std::mutex> lock(_pins_mutex);
	if(_pending_updates_flag || _exit)
		return true;
	for(auto &iter : _pins) {
		if(iter->hasPendingUpdates())
			return true;
	}
	return false;
}

void Relay::pendingUpdateCallback() {
	_pending_updates_flag = true;
	_pending_updates_cond.notify_one();
}

bool Relay::waitForPendingUpdates() {
	std::unique_lock<std::mutex> lock(_pending_updates_mutex);

	if(_exit)
		return false;

	if(_pending_updates_flag) {
		_pending_updates_flag = false;
		return true;
	}

	_pending_updates_cond.wait(lock, std::bind(&Relay::havePinPendingUpdates, this));
	_pending_updates_flag = false;

	return true;
}

void Relay::publish(const std::string &name, Pin &pin, const std::string &description) {
	verifyThatBusIsValid();
	pin.setName(name);
	pin.setDescription(description);
	publish(pin);
}

void Relay::publish(Pin &pin) {
	std::unique_lock<std::mutex> lock(_pins_mutex);
	verifyThatBusIsValid();

	auto iter = std::find(_pins.begin(), _pins.end(), &pin);
	if(iter != _pins.end())
		reportError("Pin is already published");

	_bus->publish(this, &pin);
	auto inputPin = dynamic_cast<IInput*>(&pin);
	if(inputPin)
		inputPin->setInputBufferPendingUpdateCallback(std::bind(&Relay::pendingUpdateCallback, this));
	_pins.push_back(&pin);
}

void Relay::unpublish(Pin &pin) {
	std::unique_lock<std::mutex> lock(_pins_mutex);
	verifyThatBusIsValid();

	auto iter = std::find(_pins.begin(), _pins.end(), &pin);
	if(iter == _pins.end())
		reportError("Can not unpublish unpublished pin");

	_bus->unpublish(&pin);
	_pins.erase(iter);
}

void Relay::unpublishAll() {
	std::unique_lock<std::mutex> lock(_pins_mutex);
	for(auto iter = _pins.begin(); iter != _pins.end();) {
		_bus->unpublish(*iter);
		iter = _pins.erase(iter);
	}
}

void Relay::exit() {
	_exit = true;
	throw Component_Exit_Exception();
}

void Relay::reportError(const std::string &msg) {
	if(_bus)
		_bus->reportError(this, msg);
	else
		std::cerr<<"Error: "<<msg<<"\n";
	throw std::runtime_error(msg);
}

void Relay::reportWarning(const std::string &msg) {
	if(_bus)
		_bus->reportWarning(this, msg);
	else
		std::cerr<<"Warning: "<<msg<<"\n";
}

void Relay::reportInformation(const std::string &msg) {
	if(_bus)
		_bus->reportInformation(this, msg);
	else
		std::cout<<"Info: "<<msg<<"\n";
}


bool Relay::isInitialized() const {
	return _initialized;
}

void Relay::verifyThatBusIsValid() {
	if(_bus==nullptr)
		reportError("Bus is not set (_bus==nullptr)");
}
