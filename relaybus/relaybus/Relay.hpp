/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>
#include "Input.hpp"
#include <extension_system/Extension.hpp>

#include <stdexcept>

#include <relaybus/thread.hpp>
#include <mutex>
#include <condition_variable>

#define RELAYBUS_VERSION 1

#define RELAYBUS_RELAY(_name, _version, _description) \
	EXTENSION_SYSTEM_EXTENSION(relaybus::Relay, _name, EXTENSION_SYSTEM_STR(_name), _version, _description, \
		EXTENSION_SYSTEM_DESCRIPTION_ENTRY("relaybus_version", EXTENSION_SYSTEM_STR(RELAYBUS_VERSION)) \
	)

namespace relaybus {

class Bus;

struct Component_Exit_Exception {};

/**
 * @brief The Relay class
 *	The ctor and dtor of a derived class should do as less as possible.
 *	The initialization of the class should be done in the function Relay::init()
 *	The deinitialization should be done in the function Relay::deinit()
 *	The main code of the relay should be performed in the Relay::process() function.
 *
 *	Don't try to directly use Relay instances.
 *
 *	This class is not thread safe, since all public function should only be called from a Bus or Host there is no need
 *	to make them thread safe.
 *
 *Unter jedem Relay sollten folgende Informationen bereitstehen
	- calculation time
	- sleep time
	- runs per second
 *
 */
class RELAYBUS_API Relay {
public:
	virtual ~Relay();

	/**
	 * @brief init
	 *	The ctor of a derived class should do as less as possible.
	 *	Init code should be done in the init function.
	 *	Called within the final thread context.
	 */
	virtual void init();

	/**
	 * @brief deinit
	 *	Called within the final thread context.
	 */
	virtual void deinit();

	/**
	 * Should contain the code that should be executed periodically
	 * if the function doesn't return periodically you HAVE TO honor the exit requests!
	 * Don't access pin from different threads!
	 * All waits on properties are aborted automatically with an exception if an exit is requested
	 */
	virtual void process() = 0;

	/**
	 * @brief setBus
	 *	This function is only intended to be used by a Bus instance
	 *	The function should only be called before init is called.
	 * @param bus
	 */
	void publishComponent(Bus *bus, const std::string &path);

	void unpublishComponent();

	const std::string &getPath() const;

protected:

	Relay();

	/**
	 * @brief main
	 *	Don't try to override this function. You should override process()
	 */
	void main();

	bool waitForWires() const;

	bool waitForPendingUpdates();

	/**
	 * @brief publish
	 *	Property has to be valid until it is unpublished
	 *	Don't call this function in the ctor or dtor.
	 * @param property
	 * @param buffer_size
	 */
	void publish(const std::string &name, Pin &pin, const std::string &description="");

	void publish(Pin &pin);

	/**
	 * @brief unpublish
	 *	It is not required to unpublish all properties in deinit.
	 *	Don't call this function in the ctor or dtor.
	 * @param property
	 */
	void unpublish(Pin &pin);

	void unpublishAll();

	/**
	 * @brief sleep
	 *	A requestExit safe sleep.
	 * @return true=sleep finished normally, false=sleep was interrupted by a exit request
	 * @todo not verified yet
	 */
	template <class Rep, class Period>
	bool sleep( const std::chrono::duration<Rep, Period>& duration) {
		std::lock_guard<std::mutex> lock(_sleep_condition_variable_mutex);
		return _sleep_condition_variable.wait_for(_sleep_condition_variable_mutex, duration, [&]{return !_exit;});
	}

	/**
	 * @brief sleep
	 *	A requestExit safe sleep
	 *	Sleep will abort with a Component_Exit_Exception if an exit is requested.
	 *	@see sleep
	 */
	template <class Rep, class Period>
	void sleepWithExit( const std::chrono::duration<Rep, Period>& duration) {
		if(!sleep(duration))
			throw Component_Exit_Exception();
	}

	EXTENSION_SYSTEM_NORETURN void exit();

	/**
	 * reports a fatal error, this will stop the component
	 * @brief reportError
	 * @param msg
	 */
	EXTENSION_SYSTEM_NORETURN void reportError(const std::string &msg);

	void reportWarning(const std::string &msg);

	void reportInformation(const std::string &msg);

	bool isInitialized() const;

	bool isExitRequested() const { return _exit; }

private:

	/**
	 * @brief _exit
	 *	true if the component should exit
	 */
	bool _exit;
	Bus *_bus;

	std::string _path;

	bool _initialized;

	/**
	 * @brief verifyThatBusIsValid
	 *	calls reportError if _bus==nullptr
	 */
	void verifyThatBusIsValid();

	std::mutex				_sleep_condition_variable_mutex;
	std::condition_variable	_sleep_condition_variable;

	// required for implementing waitForPendingUpdates
	void pendingUpdateCallback();
	bool havePinPendingUpdates() const;
	mutable bool _pending_updates_flag; // set if a update is pending, this avoids expensive checks if any pin was updated
	mutable std::mutex _pending_updates_mutex;
	std::condition_variable _pending_updates_cond;

	mutable std::mutex _pins_mutex;
	std::vector<Pin*>	_pins;

	std::thread	_thread;
};

}

RELAYBUS_DECLARE_TYPE(relaybus::Relay, 'r','e','l','a')
