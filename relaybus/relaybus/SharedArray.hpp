/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>
#include <memory>
#include <stdexcept>

namespace relaybus {

template <typename T>
class SharedArray {
public:
	SharedArray() : _size(0), _data_pointer(nullptr), _data_size(0) {}

	SharedArray(T* data, std::size_t size)
		: _pointer(data, arrayDelete)
		, _size(size)
		, _data_pointer(data)
		, _data_size(size)
	{}

	template <class D>
	SharedArray(T* data, std::size_t size, D d)
		: _pointer(data, d)
		, _size(size)
		, _data_pointer(data)
		, _data_size(_size)
	{}

	SharedArray(std::size_t size)
		: _pointer(new T[size], arrayDelete), _size(size),
		  _data_pointer(nullptr), _data_size(size)
	{
		_data_pointer = _pointer.get();
	}

	inline T& operator [] (std::size_t i) {
		return _data_pointer[i];
	}

	inline const T& operator [] (std::size_t i) const {
		return _data_pointer[i];
	}

	inline std::shared_ptr<T> baseData() EXTENSION_SYSTEM_NOEXCEPT {
		return _pointer;
	}

	inline const std::shared_ptr<T> baseData() const EXTENSION_SYSTEM_NOEXCEPT {
		return _pointer;
	}

	inline std::size_t baseSize() const EXTENSION_SYSTEM_NOEXCEPT {
		return _size;
	}


	inline T* data() EXTENSION_SYSTEM_NOEXCEPT {
		return _data_pointer;
	}

	inline const T * data() const EXTENSION_SYSTEM_NOEXCEPT {
		return _data_pointer;
	}


	inline std::size_t size() const EXTENSION_SYSTEM_NOEXCEPT {
		return _data_size;
	}

	// todo change function to allow creating a SharedArray for parts of a larger array
	inline void setDataRegion(std::size_t start, std::size_t size) {
		if(start+size > _size)
			throw std::invalid_argument("start+size > baseSize");
		_data_pointer = &(_pointer.get()[start]);
		_data_size = size;
	}

	static void arrayDelete( T* data ) {
		delete [] data;
	}

private:
	std::shared_ptr<T> _pointer;
	std::size_t _size;

	T *_data_pointer;
	std::size_t _data_size;
};

}
