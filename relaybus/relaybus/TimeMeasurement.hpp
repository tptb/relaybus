 /**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#pragma once

#include "macros.hpp"
#include "chrono.hpp"
#include <queue>

namespace relaybus {

class TimeMeasurement {
public:
	TimeMeasurement(std::size_t sample_count=100) EXTENSION_SYSTEM_NOEXCEPT
		: _sample_count(sample_count) {}

	time_point nextUpdate() const EXTENSION_SYSTEM_NOEXCEPT {
		if(_time_points.empty())
			return clock::now();
		else
			return _time_points.back() + timeBetweenUpdates();
	}

	duration timeBetweenUpdates() const EXTENSION_SYSTEM_NOEXCEPT {
		if(_time_points.size() < 2)
			return duration::min();
		 auto dur = _time_points.back() - _time_points.front();
		 return dur / (_time_points.size()-1);
	}

	duration update() EXTENSION_SYSTEM_NOEXCEPT {
		if(_time_points.size() >= _sample_count)
			_time_points.pop();
		_time_points.push(clock::now());
		return timeBetweenUpdates();
	}

	void clear() EXTENSION_SYSTEM_NOEXCEPT {
		_time_points = std::queue<time_point>();
	}

	bool isStable() const EXTENSION_SYSTEM_NOEXCEPT {
		return _time_points.size() == _sample_count; // TODO: we should calculate how stable the calculation is
	}

	time_point lastUpdated() const EXTENSION_SYSTEM_NOEXCEPT {
		if(_time_points.empty())
			return clock::now();
		else
			return _time_points.back();
	}

private:
	const std::size_t		_sample_count;
	std::queue<time_point>	_time_points;
};

}
