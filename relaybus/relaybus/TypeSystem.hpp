/**
	@file
	@copyright
		Copyright Bernd Amend and Michael Adam 2014
		Distributed under the Boost Software License, Version 1.0.
		(See accompanying file LICENSE_1_0.txt or copy at
		http://www.boost.org/LICENSE_1_0.txt)
*/
#pragma once

#include <relaybus/macros.hpp>
#include <extension_system/Extension.hpp>
#include <string>
#include <cstdint>
#include <type_traits>

#include <typeinfo>

namespace relaybus {
	typedef uint32_t type_id;

	template <type_id a, type_id b, type_id c, type_id d>
	struct EXTENSION_SYSTEM_TYPE_ID {
		static const type_id value = (((((d << 24) | c) << 16) | b) << 8) | a;
	};
}

/**
 * You have to call this macro with the fully qualified typename
 * Using the getID function is unsafe and may result in wrong results.
 */
#define RELAYBUS_DECLARE_TYPE(T, a, b, c, d) \
	EXTENSION_SYSTEM_INTERFACE(T) \
	namespace relaybus { \
		template<> struct TypeSystem<T> { \
			EXTENSION_SYSTEM_CONSTEXPR inline static const std::type_info &get() { return typeid(T); } \
			static const type_id id = EXTENSION_SYSTEM_TYPE_ID<a, b, c, d>::value; \
		}; \
	}

namespace relaybus {
	template<typename T>
	struct TypeSystem {
		// you have to specify RELAYBUS_DECLARE_TYPE for your type
	};
}

// definitions for all base types
RELAYBUS_DECLARE_TYPE(bool, 'b','o','o','l')

RELAYBUS_DECLARE_TYPE(int8_t, ' ','s','i','8')
RELAYBUS_DECLARE_TYPE(uint8_t, ' ','u','i','8')

RELAYBUS_DECLARE_TYPE(int16_t, 's','i','1','6')
RELAYBUS_DECLARE_TYPE(uint16_t, 'u','i','1','6')

RELAYBUS_DECLARE_TYPE(int32_t, 's','i','3','2')
RELAYBUS_DECLARE_TYPE(uint32_t, 'u','i','3','2')

RELAYBUS_DECLARE_TYPE(int64_t, 's','i','6','4')
RELAYBUS_DECLARE_TYPE(uint64_t, 'u','i','6','4')

RELAYBUS_DECLARE_TYPE(float, 'f','l','o','a')
RELAYBUS_DECLARE_TYPE(double, 'd','o','u','b')

RELAYBUS_DECLARE_TYPE(std::string, ' ','s','t','r')
RELAYBUS_DECLARE_TYPE(std::wstring, 'w','s','t','r')
