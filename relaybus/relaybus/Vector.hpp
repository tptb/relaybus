/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#pragma once

#include <array>
#include "TypeSystem.hpp"
#include "template.hpp"

namespace relaybus {

	template<typename T, int D>
	class Vector {
		static_assert(D > 0, "invalid dimension");
	public:
		EXTENSION_SYSTEM_CONSTEXPR Vector() EXTENSION_SYSTEM_NOEXCEPT
		{
		}

		Vector(const T &x, const T &y) EXTENSION_SYSTEM_NOEXCEPT {
			_data[0] = x;
			_data[1] = y;
		}

		inline T &x() EXTENSION_SYSTEM_NOEXCEPT {
			return _data[0];
		}

		EXTENSION_SYSTEM_CONSTEXPR inline const T &x() const EXTENSION_SYSTEM_NOEXCEPT {
			return _data[0];
		}

		inline T &y() EXTENSION_SYSTEM_NOEXCEPT {
			return _data[1];
		}

		EXTENSION_SYSTEM_CONSTEXPR inline const T &y() const EXTENSION_SYSTEM_NOEXCEPT {
			return _data[1];
		}

		EXTENSION_SYSTEM_CONSTEXPR inline Vector<T,D> operator+(const Vector<T,D> &other) const EXTENSION_SYSTEM_NOEXCEPT {
			return Vector<T,D>(x()+other.x(), y()+other.y());
		}

		EXTENSION_SYSTEM_CONSTEXPR inline Vector<T,D> operator-(const Vector<T,D> &other) const EXTENSION_SYSTEM_NOEXCEPT {
			return Vector<T,D>(x()-other.x(), y()-other.y());
		}

		EXTENSION_SYSTEM_CONSTEXPR inline Vector<T,D> operator/(double val) const EXTENSION_SYSTEM_NOEXCEPT {
			return Vector<T,D>(x()/val, y()/val);
		}

		EXTENSION_SYSTEM_CONSTEXPR inline Vector<T,D> operator*(double val) const EXTENSION_SYSTEM_NOEXCEPT {
			return Vector<T,D>(x()*val, y()*val);
		}

		inline Vector<T,D> &operator=(double val) EXTENSION_SYSTEM_NOEXCEPT {
			x() = y() = val;
			return *this;
		}

		inline Vector<T,D> &operator+=(const Vector<T,D> &other) EXTENSION_SYSTEM_NOEXCEPT {
			x() += other.x();
			y() += other.y();
			return *this;
		}

		inline Vector<T,D> &operator-=(const Vector<T,D> &other) EXTENSION_SYSTEM_NOEXCEPT {
			x() -= other.x();
			y() -= other.y();
			return *this;
		}

		inline Vector<T,D> &operator/=(double val) EXTENSION_SYSTEM_NOEXCEPT {
			x() /= val;
			y() /= val;
			return *this;
		}

		inline Vector<T,D> &operator*=(double val) EXTENSION_SYSTEM_NOEXCEPT {
			x() *= val;
			y() *= val;
			return *this;
		}

	private:
		std::array<T, D> _data;
	};

	typedef Vector<double, 2> Vector_2d;
	typedef Vector<Vector_2d, 2> Rectangle_d;
	typedef Vector<std::size_t, 2> Vector_2_size_t;
	typedef Vector<Vector_2_size_t, 2> Rectangle_size_t;
}

RELAYBUS_SIMPLE_TYPE(relaybus::Vector_2d)
RELAYBUS_DECLARE_TYPE(relaybus::Vector_2d, 'r','v','2','d')

RELAYBUS_SIMPLE_TYPE(relaybus::Rectangle_d)
RELAYBUS_DECLARE_TYPE(relaybus::Rectangle_d, ' ','r','r','d')

RELAYBUS_SIMPLE_TYPE(relaybus::Rectangle_size_t)
RELAYBUS_DECLARE_TYPE(relaybus::Rectangle_size_t, ' ','r','s','d')
