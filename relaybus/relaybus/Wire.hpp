/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <string>
#include <relaybus/macros.hpp>

namespace relaybus {

enum class WireType {
	Blocking,
	Drop,
	Auto
};

struct WireProfile {
public:
	EXTENSION_SYSTEM_CONSTEXPR WireProfile()
		: _type(WireType::Blocking), _buffer_size(1) {}

	EXTENSION_SYSTEM_CONSTEXPR WireProfile(WireType type, std::size_t buffer_size)
		: _type(type), _buffer_size(buffer_size) {}

	EXTENSION_SYSTEM_CONSTEXPR inline WireType type() const {
		return _type;
	}

	EXTENSION_SYSTEM_CONSTEXPR inline std::size_t bufferSize() const {
		return _buffer_size;
	}

private:
	WireType	_type;
	std::size_t	_buffer_size;
};

struct DataProfile {
	EXTENSION_SYSTEM_CONSTEXPR inline static WireProfile value() {
		return WireProfile(WireType::Blocking, 2);
	}
};

struct ParameterProfile {
	EXTENSION_SYSTEM_CONSTEXPR inline static WireProfile value() {
		return WireProfile(WireType::Drop, 1);
	}
};

/**
	Wire types

	Output -> Input (Blocking)

	Output -> Input (Drop)

	Output -> Input (Blocking)
		 \ -> Input (Blocking)

	Output -> Input (Blocking)
		 \ -> Input (Drop)

	Output -> Input (Drop)
		 \ -> Input (Drop)
 */
struct Wire {

	Wire() {}

	Wire(const std::string &destination, const std::string &source, const WireProfile &profile)
		: _destination(destination), _source(source), _profile(profile) {}

	const std::string &destination() const {
		return _destination;
	}

	const std::string &source() const {
		return _source;
	}

	const WireProfile &profile() const {
		return _profile;
	}

	bool operator==(const Wire &rhs) const {
		return (_destination + _source) == (rhs._destination + rhs._source);
	}

	bool operator!=(const Wire &rhs) const {
		return !operator==(rhs);
	}

	bool operator<(const Wire &rhs) const {
		return (_destination + _source) < (rhs._destination + rhs._source);
	}

private:
	std::string	_destination;
	std::string	_source;
	WireProfile	_profile;
};
}

/**
 * Stream operator to allow printing a Message in a human readable form.
 */
template <typename T, typename traits>
std::basic_ostream<T,traits> & operator << (std::basic_ostream<T,traits> &out, const relaybus::Wire &obj) {
	switch(obj.profile().type()) {
	case relaybus::WireType::Blocking:
		out << "_";
		break;
	case relaybus::WireType::Drop:
		out << "&";
		break;
	case relaybus::WireType::Auto:
		out << ""; //TODO: What should we do?
		break;
	}

	out << obj.profile().bufferSize() << "|" << obj.destination() << "=" << obj.source();
	return out;
}
