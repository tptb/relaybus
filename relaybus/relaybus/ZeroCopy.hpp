/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>
#include <memory>
#include "Clone.hpp"
#include "template.hpp"
#include <cstddef>
#include <stdexcept>

namespace relaybus {

	class BaseZeroCopy {
	public:
		virtual ~BaseZeroCopy() {}
		virtual bool isValid() const = 0;
	};

	template<typename T, typename Enable = void>
	class ZeroCopy : public BaseZeroCopy {
	public:

		ZeroCopy() EXTENSION_SYSTEM_NOEXCEPT
			{}

		/**
		 * @brief Construct a ZeroCopy container and pass ownership of object to ZeroCopy container.
		 *		ZeroCopy container will count references to the object and will call objects deleter.
		 *		when no references exists anymore.
		 *		You should not keep a shared_ptr instance to the object!!!!
		 * @param obj Object to be managed by container.
		 */
		ZeroCopy( const std::shared_ptr<T> &obj ) EXTENSION_SYSTEM_NOEXCEPT
			: _data(obj) {}

		/**
		 * @brief Construct a ZeroCopy container and pass ownership of object to ZeroCopy container.
		 *		ZeroCopy container will count references to the object and will call "delete object"
		 *		when no references exists anymore.
		 * @param obj Object to be managed by container.
		 */
		ZeroCopy( T *obj ) EXTENSION_SYSTEM_NOEXCEPT {
			_data.reset(obj);
		}

		/**
		 * @brief Copy constructor for ZeroCopy container.
		 *			No contained data will be copied.
		 */
		ZeroCopy( const ZeroCopy<T>& other ) EXTENSION_SYSTEM_NOEXCEPT
			: _data(other._data) {}

		/**
		 * @brief Move-constructor for ZeroCopy container
		 */
		ZeroCopy( ZeroCopy<T>&& other ) EXTENSION_SYSTEM_NOEXCEPT
			: _data(std::move(other._data)) {
		}

		/**
		 * @brief Move-constructor for ZeroCopy container
		 */
		ZeroCopy( std::shared_ptr<T>&& other ) EXTENSION_SYSTEM_NOEXCEPT
			: _data(std::move(other)) {
		}

		/**
		 * @brief Destructor for ZeroCopy container.
		 */
		~ZeroCopy() EXTENSION_SYSTEM_NOEXCEPT {}

		ZeroCopy<T> &operator=(const ZeroCopy<T> &other) EXTENSION_SYSTEM_NOEXCEPT {
			_data = other._data;
			return *this;
		}

		ZeroCopy<T> &operator=(std::nullptr_t) EXTENSION_SYSTEM_NOEXCEPT {
			_data.reset();
			return *this;
		}

		bool isValid() const EXTENSION_SYSTEM_NOEXCEPT override {
			return _data != nullptr;
		}

		bool operator==(std::nullptr_t) const EXTENSION_SYSTEM_NOEXCEPT {
			return !isValid();
		}

		bool operator!=(std::nullptr_t) const EXTENSION_SYSTEM_NOEXCEPT {
			return isValid();
		}

		/**
		 * @brief Get a read reference to contained data.
		 *			The returned value is valid as long as the ZeroCopy instance exists.
		 * @return Read-only reference to data
		 */
		inline const T &r() const {
			if(!isValid())
				throw std::runtime_error("container is invalid");
			return *_data.get();
		}

		/**
		 * @brief Get a write reference to contained data. This will cause that new data are allocated using new
		 *			the old data are freed if the ZeroCopy instance was unique.
		 *			In contrast to write, nothing is copied.
		 *			Reading from the returned value may result in undefined data.
		 *			The returned value is valid as long as the ZeroCopy instance exists.
		 * @return Write-only reference to data.
		 */
		inline T &c() EXTENSION_SYSTEM_NOEXCEPT {
			_data = std::make_shared<T>();
			return *_data.get();
		}

		/**
		 * @brief Get a write reference to contained data. This will cause that new data are allocated using new
		 *			if the ZeroCopy is not unique.
		 *			In contrast to write, nothing is copied.
		 *			Reading from the returned value may result in undefined data.
		 *			The returned value is valid as long as the ZeroCopy instance exists.
		 * @return Write-only reference to data.
		 */
		inline T &f() EXTENSION_SYSTEM_NOEXCEPT {
			if(isValid()) {
				if(!_data.unique())
					_data = std::make_shared<T>();
			} else {
				_data = std::make_shared<T>();
			}
			return *_data.get();
		}

		/**
		 * @brief Get a write reference to contained data. This will cause the data to be copied.
		 *			In contrast to readWrite, only metadata may be copied and buffers will be allocated.
		 *			Reading from the returned value may result in undefined data.
		 *			The returned value is valid as long as the ZeroCopy instance exists.
		 * @return Write-only reference to data.
		 */
		inline T &w() EXTENSION_SYSTEM_NOEXCEPT {
			if(isValid()) {
				if(!_data.unique())
					_data = Clone<T>::fast_clone(*_data.get());
			} else {
				_data = std::make_shared<T>();
			}
			return *_data.get();
		}

		/**
		 * @brief Get a read-write reference to contained data. This will cause the data to be copied.
		 *			In contrast to write, metadata and data will be copied.
		 *			Reading and writing to the returned value is allowed.
		 *			The returned value is valid as long as the ZeroCopy instance exists.
		 * @return Read-write reference to data.
		 */
		inline T &rw() EXTENSION_SYSTEM_NOEXCEPT {
			if(isValid()) {
				if(!_data.unique())
					_data = Clone<T>::clone(*_data.get());
			} else {
				_data = std::make_shared<T>();
			}
			return *_data.get();
		}

	private:
		std::shared_ptr<T> _data;
	};

	template<typename T>
	class ZeroCopy<T, typename std::enable_if<SimpleType<T>::value>::type>
		: public BaseZeroCopy {
	public:

		ZeroCopy() EXTENSION_SYSTEM_NOEXCEPT
			: _is_valid(false) {}
		ZeroCopy( T obj ) EXTENSION_SYSTEM_NOEXCEPT
			: _is_valid(true), _data(obj) {}
		ZeroCopy( const ZeroCopy<T>& other ) EXTENSION_SYSTEM_NOEXCEPT
			: _is_valid(other._is_valid), _data(other._data) {}

		ZeroCopy<T> &operator=(const ZeroCopy<T> &other) EXTENSION_SYSTEM_NOEXCEPT {
			_is_valid = other._is_valid;
			_data = other._data;
			return *this;
		}

		ZeroCopy<T> &operator=(std::nullptr_t) EXTENSION_SYSTEM_NOEXCEPT {
			_is_valid = false;
			return *this;
		}

		bool isValid() const EXTENSION_SYSTEM_NOEXCEPT override {
			return _is_valid;
		}

		bool operator==(std::nullptr_t) const EXTENSION_SYSTEM_NOEXCEPT {
			return !isValid();
		}

		bool operator!=(std::nullptr_t) const EXTENSION_SYSTEM_NOEXCEPT {
			return isValid();
		}

		inline const T &r() const {
			if(!isValid())
				throw std::runtime_error("container is invalid");
			return _data;
		}

		inline T &c() EXTENSION_SYSTEM_NOEXCEPT {
			_is_valid = true;
			return _data;
		}

		inline T &f() EXTENSION_SYSTEM_NOEXCEPT {
			_is_valid = true;
			return _data;
		}

		inline T &w() EXTENSION_SYSTEM_NOEXCEPT {
			_is_valid = true;
			return _data;
		}

		inline T &rw() EXTENSION_SYSTEM_NOEXCEPT {
			_is_valid = true;
			return _data;
		}

	private:
		bool _is_valid;
		T _data;
	};
}
