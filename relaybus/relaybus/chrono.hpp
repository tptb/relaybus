/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <chrono>

namespace relaybus {

	typedef std::chrono::high_resolution_clock	clock;

	// Default time_point that should be used for all timestamps
	typedef clock::duration		duration;
	typedef clock::time_point	time_point;
}
