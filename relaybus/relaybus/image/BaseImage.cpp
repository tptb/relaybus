/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include "BaseImage.hpp"
#include "MemoryImageAllocator.hpp"

#include <cassert>

#include <utility>

using namespace relaybus;
using namespace relaybus::image;

BaseImageAllocator BaseImageAllocator::_allocator;

BaseImageView::BaseImageView() : _allocator(nullptr) {
	_width = 0;
	_height = 0;
	_pixelformat = RPixelformat::RPixelformat_NONE;
	_time_stamp = time_point::min();
	std::fill(_stride.begin(), _stride.end(), 0);
	std::fill(_data.begin(), _data.end(), nullptr);
}

BaseImageView::BaseImageView(BaseImageAllocator* allocator) : _allocator(allocator) {
	_width = 0;
	_height = 0;
	_pixelformat = RPixelformat::RPixelformat_NONE;
	_time_stamp = time_point::min();
	std::fill(_stride.begin(), _stride.end(), 0);
	std::fill(_data.begin(), _data.end(), nullptr);
}

BaseImageView::BaseImageView( const BaseImageView& other ) : _allocator(other._allocator) {
	_width = other._width;
	_height = other._height;
	_pixelformat = other._pixelformat;
	_time_stamp = other._time_stamp;
	std::copy(other._stride.begin(), other._stride.end(), _stride.begin());
	std::copy(other._data.begin(), other._data.end(), _data.begin());
	std::copy(other._base_data.begin(), other._base_data.end(), _base_data.begin());
}

bool BaseImageView::operator== ( std::nullptr_t ) const {
	return _width == 0 || _height == 0 || _pixelformat == RPixelformat::RPixelformat_NONE;
}

BaseImageView& BaseImageView::operator= ( std::nullptr_t ) {
	*this = BaseImageView();
	return *this;
}

BaseImageView& BaseImageView::operator= ( const BaseImageView& other ) {
	// TODO we have to change the allocator
	if( this != &other ) {
		_width = other._width;
		_height = other._height;
		_pixelformat = other._pixelformat;
		_time_stamp = other._time_stamp;
		_allocator = other._allocator;
		std::copy(other._stride.begin(), other._stride.end(), _stride.begin());
		std::copy(other._data.begin(), other._data.end(), _data.begin());
		std::copy(other._base_data.begin(), other._base_data.end(), _base_data.begin());
	}
	return *this;
}

BaseImageView::~BaseImageView() {
}

BaseImageView BaseImageView::roi(const Rectangle_size_t &rect) const {
	return roi(rect.x().x(), rect.x().y(), rect.y().x(), rect.y().y());
}

BaseImageView BaseImageView::roi( image_size_type x, image_size_type y, image_size_type width, image_size_type height ) const {
	BaseImageView n(*this);
	n._width = width;
	n._height = height;

	RPixelformat::PixelFormatDescriptionBase& desc = RPixelformat::PixelFormatDescriptionBase::get(_pixelformat);

	n._data[0] += desc.memoryOffset(0,x,y, _stride);
	n._data[1] += desc.memoryOffset(1,x,y, _stride);
	n._data[2] += desc.memoryOffset(2,x,y, _stride);
	n._data[3] += desc.memoryOffset(3,x,y, _stride);
	return n;
}

BaseImage BaseImageView::cloneData() const
{
	return BaseImage(*this);
}


BaseImage::BaseImage( BaseImageAllocator* allocator )
	: BaseImageView(allocator)
{}

BaseImage::BaseImage( image_size_type width, image_size_type height, RPixelformat::Format pixelformat, image_size_type stride, uint8_t* data, BaseImageAllocator* allocator )
	: BaseImageView(allocator)
{
	_width = width;
	_height = height;
	_pixelformat = pixelformat;

	_stride[0] = stride;
	_data[0] = data;
}

BaseImage::BaseImage( image_size_type width, image_size_type height, RPixelformat::Format pixelformat, const image_stride_t &stride, const image_data_t &data, BaseImageAllocator* allocator )
	: BaseImageView(allocator)
{
	_width = width;
	_height = height;
	_pixelformat = pixelformat;

	std::copy(stride.begin(), stride.end(), _stride.begin());
	std::copy(data.begin(), data.end(), _data.begin());
}

BaseImage::BaseImage(image_size_type width, image_size_type height, RPixelformat::Format pixelformat, const image_stride_t &stride, BaseImageAllocator* allocator)
	: BaseImageView(allocator)
{
	if(_allocator == nullptr)
		_allocator = MemoryImageAllocator::get();

	_width = width;
	_height = height;
	_pixelformat = pixelformat;

	_allocator->allocate(*this, stride);
}

BaseImage::BaseImage( image_size_type width, image_size_type height, RPixelformat::Format pixelformat, BaseImageAllocator* allocator )
	: BaseImageView(allocator)
{
	if(_allocator == nullptr)
		_allocator = MemoryImageAllocator::get();

	_width = width;
	_height = height;
	_pixelformat = pixelformat;
	_allocator->allocate(*this);
}

BaseImage::BaseImage(const  BaseImage& other )
	: BaseImageView(other._allocator)
{
	if(_allocator == nullptr)
		_allocator = MemoryImageAllocator::get();

	_width = other._width;
	_height = other._height;
	_pixelformat = other._pixelformat;
	_time_stamp = other._time_stamp;

	_allocator->cloneDataAndStride(other, *this);
}

BaseImage::BaseImage(const BaseImageView& other )
	: BaseImageView(nullptr)
{
	if(_allocator == nullptr)
		_allocator = MemoryImageAllocator::get();

	_width = other.width();
	_height = other.height();
	_pixelformat = other.format();
	_time_stamp = other.timestamp();

	_allocator->cloneDataAndStride(other, *this);
}

BaseImage& BaseImage::operator=( BaseImage&& other )
{
	_allocator = std::move(other._allocator);

	_width = std::move(other._width);
	_height = std::move(other._height);

	_stride = std::move(other._stride);
	_data = std::move(other._data);
	_base_data = std::move(other._base_data);

	_pixelformat = std::move(other._pixelformat);

	_time_stamp = std::move(other._time_stamp);
	return *this;
}
