/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>

#include "BaseImageData.hpp"

#include <relaybus/TypeSystem.hpp>
#include <relaybus/Clone.hpp>
#include <cstddef>
#include <relaybus/Vector.hpp>


namespace relaybus { namespace image {
	class BaseImage;

	class BaseImageAllocator;

	class RELAYBUS_API BaseImageView : public BaseImageData
	{
	public:
		/**
		* the allocater is set to BaseImageAllocator::get()
		*/
		BaseImageView();

		BaseImageView(const BaseImageView& other);

		bool operator== ( std::nullptr_t ) const;

		BaseImageView& operator= ( std::nullptr_t );
		BaseImageView& operator= ( const BaseImageView& other );

		virtual ~BaseImageView();
		inline image_size_type width() const { return _width; }
		inline image_size_type width(image_stride_t::size_type plane) const {
			const RPixelformat::PixelFormatDescriptionBase& desc = RPixelformat::PixelFormatDescriptionBase::get(_pixelformat);
			return desc.planeWidth(plane, _width);
		}

		inline image_size_type height() const { return _height; }
		inline image_size_type height(image_stride_t::size_type plane) const {
			const RPixelformat::PixelFormatDescriptionBase& desc = RPixelformat::PixelFormatDescriptionBase::get(_pixelformat);
			return desc.planeHeight(plane, _height);
		}

		inline image_size_type stride(image_stride_t::size_type plane=0) const { return _stride[plane]; }

		inline RPixelformat::Format format() const { return _pixelformat; }

		inline time_point timestamp() const { return _time_stamp; }

		inline uint8_t* data(image_stride_t::size_type plane=0) { return _data[plane]; }

		inline const uint8_t* data(image_stride_t::size_type plane=0) const { return _data[plane]; }

		inline image_size_type sizeInByte(image_stride_t::size_type plane) const {
			const RPixelformat::PixelFormatDescriptionBase& desc = RPixelformat::PixelFormatDescriptionBase::get(_pixelformat);
			return _stride[plane]*desc.planeHeight(plane, _height);
		}

		BaseImageView roi(const Rectangle_size_t &rect) const;
		BaseImageView roi(image_size_type x, image_size_type y, image_size_type width, image_size_type height) const;

		inline BaseImageAllocator* allocator() const {
			return _allocator;
		}

		BaseImage cloneData() const;

	protected:
		BaseImageView(BaseImageAllocator* allocator);
		BaseImageAllocator* _allocator;
		friend class BaseImageAllocator;
	};

	class RELAYBUS_API BaseImageAllocator
	{
	public:
		virtual ~BaseImageAllocator() {}

		image_data_t &data( BaseImageView& base ) { return base._data; }
		const image_data_t &data( const BaseImageView& base ) const { return base._data; }

		image_data_shared_ptr_t &baseData( BaseImageView& base ) { return base._base_data; }
		const image_data_shared_ptr_t &baseData( const BaseImageView& base ) const { return base._base_data; }

		image_stride_t &stride( BaseImageView& base ) { return base._stride; }
		const image_stride_t &stride( const BaseImageView& base ) const { return base._stride; }

		virtual void allocate( BaseImage& ) {}
		virtual void allocate( BaseImage&, const image_stride_t & ) {}

		virtual void cloneDataAndStride( const BaseImageView&, BaseImage& ) {}

		static inline BaseImageAllocator* get() {return &_allocator;}

	private:
		static BaseImageAllocator _allocator;
	};

	class RELAYBUS_API BaseImage : public BaseImageView
	{
	public:
		BaseImage(const BaseImage& other );
		BaseImage(const BaseImageView& other );

		BaseImage( BaseImageAllocator* allocator = nullptr);

		/**
		 * @brief BaseImage
		 * @param width
		 * @param height
		 * @param pixelformat
		 * @param stride
		 * @param data
		 * @param allocator can be a nullptr
		 */
		BaseImage(image_size_type width, image_size_type height, RPixelformat::Format pixelformat, image_size_type stride, uint8_t* data, BaseImageAllocator* allocator);

		/**
		 * @brief BaseImage
		 * @param width
		 * @param height
		 * @param pixelformat
		 * @param stride
		 * @param data
		 * @param allocator ca be a nullptr
		 */
		BaseImage(image_size_type width, image_size_type height, RPixelformat::Format pixelformat, const image_stride_t &stride, const image_data_t &data, BaseImageAllocator* allocator);

		BaseImage(image_size_type width, image_size_type height, RPixelformat::Format pixelformat, const image_stride_t &stride, BaseImageAllocator* allocator);

		BaseImage(image_size_type width, image_size_type height, RPixelformat::Format pixelformat, BaseImageAllocator* allocator);

		BaseImage& operator = ( BaseImage&& other );
	protected:
		BaseImage& operator = ( const BaseImage& )
		{
			// TODO: assign operator

			//if( this != &other )
			//{
			//  _width = other._width;
			//  _height = other._height;
			//  _pixelformat = other._pixelformat;

			//}
			return *this;
		}

	};
}

	template<>
	class Clone<image::BaseImageView> {
	public:
		typedef image::BaseImageView InputType;
		typedef image::BaseImage ResultType;
		typedef std::shared_ptr<ResultType> ResultTypePtr;

		static ResultTypePtr clone(const InputType &org) {
			return std::make_shared<ResultType>(org);
		}

		static ResultTypePtr fast_clone(const InputType &org) {
			return std::make_shared<ResultType>(org.width(), org.height(), org.format(), org.allocator());
		}
	};

	template<>
	class Clone<image::BaseImage> {
	public:
		typedef image::BaseImage InputType;
		typedef image::BaseImage ResultType;
		typedef std::shared_ptr<ResultType> ResultTypePtr;

		static ResultTypePtr clone(const InputType &org) {
			return std::make_shared<ResultType>(org);
		}

		static ResultTypePtr fast_clone(const InputType &org) {
			return std::make_shared<ResultType>(org.width(), org.height(), org.format(), org.allocator());
		}
	};
}

RELAYBUS_DECLARE_TYPE(relaybus::image::BaseImage, 'b','i','m','g')
RELAYBUS_DECLARE_TYPE(relaybus::image::RPixelformat::Format, 'r','p','f','f')
RELAYBUS_DECLARE_TYPE(relaybus::image::BaseImageView, 'r','b','i','w')
