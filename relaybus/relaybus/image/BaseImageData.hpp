/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include "PixelFormat.hpp"
#include <relaybus/chrono.hpp>
#include <memory>

namespace relaybus { namespace image {

	//using image_data_shared_ptr_t = std::array<std::shared_ptr<uint8_t*>, image_dimensions>;
	typedef std::array<std::shared_ptr<uint8_t>, image_dimensions> image_data_shared_ptr_t;

	struct BaseImageDataStruct {
		/**
		 * @brief Width of the image in pixels
		 */
		image_size_type _width;

		/**
		 * @brief Height of the image in pixels
		 */
		image_size_type _height;

		/**
		 * @brief Stride in bytes seperate for all planes
		 */
		image_stride_t _stride;

		/**
		 * @brief Data pointer
		 */
		image_data_t _data;

		/**
		 * @brief shared_ptr for the _data
		 *	The addresses for freeing the memory maybe different from the _data pointers
		 *	e.g. in case of an image roi
		 */
		image_data_shared_ptr_t _base_data;

		/**
		 * @brief Pixelformat of the image.
		 *	This specifies how man planes are actually used.
		 */
		RPixelformat::Format _pixelformat;

		time_point	_time_stamp;
	};

	class BaseImageData : protected BaseImageDataStruct {
	public:
		const BaseImageDataStruct* getBaseImageDataStruct() const {
			return static_cast<const BaseImageDataStruct*>(this);
		}
	};
}}
