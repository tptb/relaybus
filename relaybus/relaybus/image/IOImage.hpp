/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <relaybus/image/BaseImage.hpp>
#include <fstream>

namespace relaybus { namespace image {

	class IOImage {
	public:
		/**
		* @brief saveAsPPMImage
		*	Helper function for debugging
		*/
		inline static void saveAsPPMImage(const std::string &filename, const relaybus::BaseImageView &img) {
			if(img.format() != RPixelformat::RPixelformat_RGB24)
				throw std::runtime_error("only rgb24 is currently supported");

			std::ofstream out;
			out.open(filename.c_str(), std::ios_base::binary | std::ios_base::out);

			out << "P6\n";
			out << img.width() << " " << img.height() << "\n";
			out << 255 << "\n" ;

			for(image_size_type y=0;y<img.height();++y)
				out.write(reinterpret_cast<const char*>(img.data()+y*img.stride()), static_cast<std::streamsize>(img.stride()));

			out.close();
		}
	};

}}
