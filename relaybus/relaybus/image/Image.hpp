/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <cstdint>
#include <cstring>
#include <typeinfo>

#include "PixelFormat.hpp"
#include "BaseImage.hpp"
#include "MemoryImageAllocator.hpp"


namespace relaybus { namespace image {
  // forward declaration of image for use in image view
  template < RPixelformat::Format T >
  class Image;

  /**
   * ImageView
   * An ImageView acts like an image and performs a flat-copy when copied
   * \brief Provides a view to an image
   */
  template < RPixelformat::Format T >
  class ImageView : public BaseImageView
  {
  public:
	ImageView( BaseImageView& baseImageView ) : BaseImageView(baseImageView)
	{
		if( T != baseImageView.format() )
			throw std::bad_typeid();
	}

	/**
	  * Constructor
	  * \brief Create a view to an image
	  * \param image Image the view will be working on
	  */
	ImageView(Image<T>* image, BaseImageAllocator* allocator);

	/**
	  * Get a pixel from image.
	  * Format of the pixel is defined by pixel format of image.
	  * \param x X-Position
	  * \param y Y-Position
	  */
	inline typename RPixelformat::PixelFormatDescription<T>::Pixel pixel(int x, int y) {
		// TODO: only return pixels, if the allocator allows access.
		return _pixelFormatDescription.pixel(x, y, _stride, _data);
	}

	/**
	  * \brief Get with of an image plane
	  * \param plane_number Number of the plane to retrieve width (in pixels) from
	  */
	inline int planeWidth(int plane_number) {
		switch( plane_number ) {
			case 0: return _pixelFormatDescription.planeWidth<0>(_width);
			case 1: return _pixelFormatDescription.planeWidth<1>(_width);
			case 2: return _pixelFormatDescription.planeWidth<2>(_width);
			case 3: return _pixelFormatDescription.planeWidth<3>(_width);
		}
	  return 0;
	}

	/**
	  * \brief Get height of an image plane
	  * \param plane_number Number of the plane to retrieve height (in pixels) from
	  */
	inline int planeHeight(int plane_number) {
		switch( plane_number )
		{
			case 0: return _pixelFormatDescription.planeHeight<0>(_height);
			case 1: return _pixelFormatDescription.planeHeight<1>(_height);
			case 2: return _pixelFormatDescription.planeHeight<2>(_height);
			case 3: return _pixelFormatDescription.planeHeight<3>(_height);
		}
		return 0;
	}

	/**
	  * \brief Get with of an image plane
	  * \tparam plane_number Number of the plane to retrieve width (in pixels) from
	  */
	template <int plane_number>
	inline int planeWidth() {
		return _pixelFormatDescription.planeWidth<plane_number>(_width);
	}

	/**
	  * \brief Get height of an image plane
	  * \tparam plane_number Number of the plane to retrieve height (in pixels) from
	  */
	template <int plane_number>
	int planeHeight() {
		return _pixelFormatDescription.planeHeight()<plane_number>(_height);
	}

  protected:
	ImageView() : BaseImageView() {}
	//ImageView(int width, int height, BaseImageAllocator* allocator) :
	//  BaseImageView(width, height, T, allocator )
	//{ }
  private:
	RPixelformat::PixelFormatDescription<T> _pixelFormatDescription;
  };

  /**
   * Image
   * An Image stores image data and performs a deep-copy when copied.
   * \brief Provides a storage and access capabilities to image data.
   * \tparam T Pixel-format of the image.
   */
  template < RPixelformat::Format T >
  class Image : public BaseImage
  {
  public:

	/**
	  * Constructor for Image with specified dimensions
	  * \param[in] width With of image
	  * \param[in] height Height of image
	  * \param[in] allocator An allocator, used for memory allocation. To provide images with GPU etc. storage, use a specific allocator.
	  */
	Image(image_size_type width = 0, image_size_type height = 0, BaseImageAllocator* allocator = MemoryImageAllocator::get() )
		: BaseImage(width, height, T, allocator )
	{}

	// TODO: check operator!
	Image<T>& operator = ( const Image<T>& other ) {
		if( this != &other )
		{
			_width = other._width;
			_height = other._height;
			_time_stamp = other._time_stamp;
			_allocator->cloneDataAndStride(other, *this);
		}
	}

	inline operator ImageView<T>() {
		return ImageView<T>();
	}
	/**
	  * Get a pixel from image.
	  * Format of the pixel is defined by pixel format of image.
	  * \param x X-Position
	  * \param y Y-Position
	  */
	inline typename RPixelformat::PixelFormatDescription<T>::Pixel pixel(image_size_type x, image_size_type y) {
		// TODO: only return pixels, if the allocator allows access.
		return _pixelFormatDescription.pixel(x, y, _stride, _data);
	}

	/**
	  * \brief Get with of an image plane
	  * \param plane_number Number of the plane to retrieve width (in pixels) from
	  */
	inline image_size_type planeWidth(int plane_number)
	{
		switch( plane_number ) {
			case 0: return _pixelFormatDescription.planeWidth<0>(_width);
			case 1: return _pixelFormatDescription.planeWidth<1>(_width);
			case 2: return _pixelFormatDescription.planeWidth<2>(_width);
			case 3: return _pixelFormatDescription.planeWidth<3>(_width);
		}
		return 0;
	}

	/**
	  * \brief Get height of an image plane
	  * \param plane_number Number of the plane to retrieve height (in pixels) from
	  */
	inline image_size_type planeHeight(int plane_number) {
		switch( plane_number ) {
			case 0: return _pixelFormatDescription.planeHeight<0>(_height);
			case 1: return _pixelFormatDescription.planeHeight<1>(_height);
			case 2: return _pixelFormatDescription.planeHeight<2>(_height);
			case 3: return _pixelFormatDescription.planeHeight<3>(_height);
		}
		return 0;
	}

	/**
	  * \brief Get with of an image plane
	  * \tparam plane_number Number of the plane to retrieve width (in pixels) from
	  */
	template <int plane_number>
	inline image_size_type planeWidth() {
		return _pixelFormatDescription.planeWidth<plane_number>(_width);
	}

	/**
	  * \brief Get height of an image plane
	  * \tparam plane_number Number of the plane to retrieve height (in pixels) from
	  */
	template <int plane_number>
	image_size_type planeHeight() {
		return _pixelFormatDescription.planeHeight()<plane_number>(_height);
	}
  private:
	RPixelformat::PixelFormatDescription<T> _pixelFormatDescription;

  };

	template< RPixelformat::Format T >
	ImageView<T>::ImageView(Image<T>* image, BaseImageAllocator* allocator) : BaseImageView(*image, allocator)
	{}

	//template<PixelFormat::Format T>
	//ImageView<T> imageview_cast(BaseImageView* other)
	//{
	//  if( other->format() != T )
	//  {
	//    throw bad_cast();
	//  }
	//}
}}

