/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include "MemoryImageAllocator.hpp"
#include <cassert>
#include <cstring>

using namespace relaybus;
using namespace relaybus::image;

MemoryImageAllocator MemoryImageAllocator::_allocator;


void MemoryImageAllocator::allocate( BaseImage& base )
{
	image_stride_t stride;
	RPixelformat::PixelFormatDescriptionBase& desc = RPixelformat::PixelFormatDescriptionBase::get(base.format());
	for(image_stride_t::size_type i=0;i<stride.size();++i) {
		stride[i] = desc.bytesPerPlaneLine(i, base.width());
	}
	allocate(base, stride);
}

void MemoryImageAllocator::allocate( BaseImage& base, const image_stride_t &t_stride )
{
	// TODO: check stride
	// TODO: intelligent memory reallocation
	image_data_t &_data = data(base);
	image_data_shared_ptr_t &_base_data = baseData(base);
	image_stride_t &_stride = stride(base);

	std::copy(t_stride.begin(), t_stride.end(), _stride.begin());

	RPixelformat::PixelFormatDescriptionBase& desc = RPixelformat::PixelFormatDescriptionBase::get(base.format());

	for(image_stride_t::size_type i=0;i<_stride.size();++i) {
		image_size_type size = _stride[i]*desc.planeHeight(i, base.height());
		if(size != 0) {
			_data[i] = new uint8_t[size];
			_base_data[i].reset(_data[i], [](uint8_t *p){delete[] p;});
		}
	}
}

void MemoryImageAllocator::cloneDataAndStride( const BaseImageView& base, BaseImage& out )
{
	assert(base.format() == out.format());
	allocate( out, base.getBaseImageDataStruct()->_stride );
	image_data_t &_data_dst = data(out);
	const image_data_t &_data_src = data(base);
	image_stride_t _stride = stride(out);
	RPixelformat::PixelFormatDescriptionBase& desc = RPixelformat::PixelFormatDescriptionBase::get(base.format());

	for(image_data_t::size_type i=0;i<_data_src.size();++i) {
		if(_data_src[i] != nullptr)
			memcpy(_data_dst[i], _data_src[i], _stride[i]*desc.planeHeight(i, base.height()));
	}
}
