/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>

#include "BaseImage.hpp"

namespace relaybus { namespace image {

class RELAYBUS_API MemoryImageAllocator : public BaseImageAllocator
{
public:
	virtual void allocate( BaseImage& base ) override;
	virtual void allocate( BaseImage& base, const image_stride_t &stride ) override;

	virtual void cloneDataAndStride( const BaseImageView& base, BaseImage& out ) override;

	static inline MemoryImageAllocator* get() {return &_allocator;}

private:
	static MemoryImageAllocator _allocator;
};

}}
