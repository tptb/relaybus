/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <cstdint>
#include <array>

namespace relaybus { namespace image {

	typedef std::size_t image_size_type;

	enum {
		image_dimensions = 4
	};
	//using image_stride_t = std::array<image_size_type, image_dimensions>;
	typedef std::array<image_size_type, image_dimensions> image_stride_t;

	//using image_data_t = std::array<uint8_t*, image_dimensions>;
	typedef std::array<uint8_t*, image_dimensions> image_data_t;

	// The name PixelFormat is sadly blocked by ffmpeg :(
	namespace RPixelformat {
		enum Format {
			RPixelformat_NONE= -1,

			RPixelformat_FIRST_GRAY = 10000,
			RPixelformat_MONOWHITE  ///<        Y        ,            1bpp, 0 is white, 1 is black, in each byte pixels are ordered from the msb to the lsb
			= RPixelformat_FIRST_GRAY,
			RPixelformat_MONOBLACK, ///<        Y        ,            1bpp, 0 is black, 1 is white, in each byte pixels are ordered from the msb to the lsb
			RPixelformat_GRAY8,     ///<        Y        ,            8bpp
			RPixelformat_GRAY16BE,  ///<        Y        ,            16bpp, big-endian
			RPixelformat_GRAY16LE,  ///<        Y        ,            16bpp, little-endian
			RPixelformat_GRAY8A,    ///< 8bit gray, 8bit alpha        16bpp
			RPixelformat_GRAYSINGLE,///                               32bpp, single precision floating point number
			RPixelformat_GRAYDOUBLE,///                               64bpp, double precision floating point number
			RPixelformat_LAST_GRAY = RPixelformat_GRAYDOUBLE,

			RPixelformat_FIRST_PAL = 20000,
			RPixelformat_PAL8      ///< 8 bit with RPixelformat_RGB32 palette
			= RPixelformat_FIRST_PAL,
			RPixelformat_LAST_PAL = RPixelformat_PAL8,

			RPixelformat_FIRST_RGB = 30000,
			RPixelformat_RGB4       ///< packed RGB 1:2:1 bitstream,   4bpp, (msb)1R 2G 1B(lsb), a byte contains two pixels, the first pixel in the byte is the one composed by the 4 msb bits
			= RPixelformat_FIRST_RGB,
			RPixelformat_RGB4_BYTE, ///< packed RGB 1:2:1,             8bpp, (msb)1R 2G 1B(lsb)
			RPixelformat_BGR4,      ///< packed RGB 1:2:1 bitstream,   4bpp, (msb)1B 2G 1R(lsb), a byte contains two pixels, the first pixel in the byte is the one composed by the 4 msb bits
			RPixelformat_BGR4_BYTE, ///< packed RGB 1:2:1,             8bpp, (msb)1B 2G 1R(lsb)
			RPixelformat_RGB8,      ///< packed RGB 3:3:2,             8bpp, (msb)2R 3G 3B(lsb)
			RPixelformat_BGR8,      ///< packed RGB 3:3:2,             8bpp, (msb)2B 3G 3R(lsb)
			RPixelformat_RGB444LE,  ///< packed RGB 4:4:4,            16bpp, (msb)4A 4R 4G 4B(lsb), little-endian, most significant bits to 0
			RPixelformat_RGB444BE,  ///< packed RGB 4:4:4,            16bpp, (msb)4A 4R 4G 4B(lsb), big-endian, most significant bits to 0
			RPixelformat_BGR444LE,  ///< packed BGR 4:4:4             16bpp, (msb)4A 4B 4G 4R(lsb), little-endian, most significant bits to 1
			RPixelformat_BGR444BE,  ///< packed BGR 4:4:4,            16bpp, (msb)4A 4B 4G 4R(lsb), big-endian, most significant bits to 1
			RPixelformat_RGB565BE,  ///< packed RGB 5:6:5,            16bpp, (msb)   5R 6G 5B(lsb), big-endian
			RPixelformat_RGB565LE,  ///< packed RGB 5:6:5,            16bpp, (msb)   5R 6G 5B(lsb), little-endian
			RPixelformat_RGB555BE,  ///< packed RGB 5:5:5,            16bpp, (msb)1A 5R 5G 5B(lsb), big-endian, most significant bit to 0
			RPixelformat_RGB555LE,  ///< packed RGB 5:5:5,            16bpp, (msb)1A 5R 5G 5B(lsb), little-endian, most significant bit to 0
			RPixelformat_BGR565BE,  ///< packed BGR 5:6:5,            16bpp, (msb)   5B 6G 5R(lsb), big-endian
			RPixelformat_BGR565LE,  ///< packed BGR 5:6:5,            16bpp, (msb)   5B 6G 5R(lsb), little-endian
			RPixelformat_BGR555BE,  ///< packed BGR 5:5:5,            16bpp, (msb)1A 5B 5G 5R(lsb), big-endian, most significant bit to 1
			RPixelformat_BGR555LE,  ///< packed BGR 5:5:5,            16bpp, (msb)1A 5B 5G 5R(lsb), little-endian, most significant bit to 1
			RPixelformat_RGB24,     ///< packed RGB 8:8:8,            24bpp, RGBRGB...
			RPixelformat_BGR24,     ///< packed RGB 8:8:8,            24bpp, BGRBGR...
			RPixelformat_ARGB,      ///< packed ARGB 8:8:8:8,         32bpp, ARGBARGB...
			RPixelformat_RGBA,      ///< packed RGBA 8:8:8:8,         32bpp, RGBARGBA...
			RPixelformat_ABGR,      ///< packed ABGR 8:8:8:8,         32bpp, ABGRABGR...
			RPixelformat_BGRA,      ///< packed BGRA 8:8:8:8,         32bpp, BGRABGRA...
			RPixelformat_0RGB,      ///< packed RGB 8:8:8,            32bpp, 0RGB0RGB...
			RPixelformat_RGB0,      ///< packed RGB 8:8:8,            32bpp, RGB0RGB0...
			RPixelformat_0BGR,      ///< packed BGR 8:8:8,            32bpp, 0BGR0BGR...
			RPixelformat_BGR0,      ///< packed BGR 8:8:8,            32bpp, BGR0BGR0...
			RPixelformat_RGB48BE,   ///< packed RGB 16:16:16,         48bpp, 16R, 16G, 16B, the 2-byte value for each R/G/B component is stored as big-endian
			RPixelformat_RGB48LE,   ///< packed RGB 16:16:16,         48bpp, 16R, 16G, 16B, the 2-byte value for each R/G/B component is stored as little-endian
			RPixelformat_BGR48BE,   ///< packed RGB 16:16:16,         48bpp, 16B, 16G, 16R, the 2-byte value for each R/G/B component is stored as big-endian
			RPixelformat_BGR48LE,   ///< packed RGB 16:16:16,         48bpp, 16B, 16G, 16R, the 2-byte value for each R/G/B component is stored as little-endian
			RPixelformat_RGBA64BE,  ///< packed RGBA 16:16:16:16,     64bpp, 16R, 16G, 16B, 16A, the 2-byte value for each R/G/B/A component is stored as big-endian
			RPixelformat_RGBA64LE,  ///< packed RGBA 16:16:16:16,     64bpp, 16R, 16G, 16B, 16A, the 2-byte value for each R/G/B/A component is stored as little-endian
			RPixelformat_BGRA64BE,  ///< packed RGBA 16:16:16:16,     64bpp, 16B, 16G, 16R, 16A, the 2-byte value for each R/G/B/A component is stored as big-endian
			RPixelformat_BGRA64LE,  ///< packed RGBA 16:16:16:16,     64bpp, 16B, 16G, 16R, 16A, the 2-byte value for each R/G/B/A component is stored as little-endian
			RPixelformat_RGBSINGLE, ///< packed RGB 32:32:32,         96bpp, rgb stored in single precisition floating point numbers
			RPixelformat_BGRSINGLE, ///< packed BGR 32:32:32,         96bpp, bgr stored in single precisition floating point numbers
			RPixelformat_ARGBSINGLE,///< packed RGB 32:32:32:32,     128bpp, argb stored in single precisition floating point numbers
			RPixelformat_ABGRSINGLE,///< packed BGR 32:32:32:32,     128bpp, abgr stored in single precisition floating point numbers
			RPixelformat_RGBASINGLE,///< packed RGB 32:32:32:32,     128bpp, rgba stored in single precisition floating point numbers
			RPixelformat_BGRASINGLE,///< packed BGR 32:32:32:32,     128bpp, bgra stored in single precisition floating point numbers
			RPixelformat_RGBDOUBLE, ///< packed RGB 64:64:64,        192bpp, rgb stored in double precisition floating point numbers
			RPixelformat_BGRDOUBLE, ///< packed BGR 64:64:64,        192bpp, bgr stored in double precisition floating point numbers
			RPixelformat_ARGBDOUBLE,///< packed RGB 64:64:64:64,     256bpp, argb stored in double precisition floating point numbers
			RPixelformat_ABGRDOUBLE,///< packed BGR 64:64:64:64,     256bpp, abgr stored in double precisition floating point numbers
			RPixelformat_RGBADOUBLE,///< packed RGB 64:64:64:64,     256bpp, rgba stored in double precisition floating point numbers
			RPixelformat_BGRADOUBLE,///< packed BGR 64:64:64:64,     256bpp, bgra stored in double precisition floating point numbers
			RPixelformat_GBRP,      ///< planar GBR 8:8:8,            24bpp
			RPixelformat_GBRAP,     ///< planar GBRA 8:8:8:8,         32bpp
			RPixelformat_GBR64P,    ///< planar GBR 16:16:16,         24bpp
			RPixelformat_GBRA64P,   ///< planar GBRA 16:16:16:16,     32bpp
			RPixelformat_GBRSINGLEP, ///< planar GBR 32:32:32,        96bpp, stored in single precisition floating point numbers
			RPixelformat_GBRASINGLEP,///< planar GBRA 32:32:32:32,   128bpp, stored in single precisition floating point numbers
			RPixelformat_GBRDOUBLEP, ///< planar GBR 32:32:32,       192bpp, stored in double precisition floating point numbers
			RPixelformat_GBRADOUBLEP,///< planar GBRA 32:32:32:32,   256bpp, stored in double precisition floating point numbers
			RPixelformat_LAST_RGB = RPixelformat_GBRADOUBLEP,

			RPixelformat_FIRST_YUV = 40000,
			RPixelformat_UYYVYY411  ///< packed YUV 4:1:1,            12bpp, Cb Y0 Y1 Cr Y2 Y3
			= RPixelformat_FIRST_YUV,
			RPixelformat_UYVY422,   ///< packed YUV 4:2:2,            16bpp, Cb Y0 Cr Y1
			RPixelformat_YUYV422,   ///< packed YUV 4:2:2,            16bpp, Y0 Cb Y1 Cr
			RPixelformat_YUV410P,   ///< planar YUV 4:1:0,             9bpp, (1 Cr & Cb sample per 4x4 Y samples)
			RPixelformat_YUV420P,   ///< planar YUV 4:2:0,            12bpp, (1 Cr & Cb sample per 2x2 Y samples)
			RPixelformat_NV12,      ///< planar YUV 4:2:0,            12bpp, 1 plane for Y and 1 plane for the UV components, which are interleaved (first byte U and the following byte V)
			RPixelformat_NV21,      ///< planar YUV 4:2:0,            12bpp, 1 plane for Y and 1 plane for the UV components, which are interleaved (first byte V and the following byte U)
			RPixelformat_YUV411P,   ///< planar YUV 4:1:1,            12bpp, (1 Cr & Cb sample per 4x1 Y samples)
			RPixelformat_YUVJ420P,  ///< planar YUV 4:2:0,            12bpp, full scale (JPEG), deprecated in favor of RPixelformat_YUV420P and setting color_range
			RPixelformat_YUV422P,   ///< planar YUV 4:2:2,            16bpp, (1 Cr & Cb sample per 2x1 Y samples)
			RPixelformat_YUVJ422P,  ///< planar YUV 4:2:2,            16bpp, full scale (JPEG), deprecated in favor of RPixelformat_YUV422P and setting color_range
			RPixelformat_YUVA420P,  ///< planar YUV 4:2:0,            20bpp, (1 Cr & Cb sample per 2x2 Y & A samples)
			RPixelformat_YUV444P,   ///< planar YUV 4:4:4,            24bpp, (1 Cr & Cb sample per 1x1 Y samples)
			RPixelformat_YUV440P,   ///< planar YUV 4:4:0,            24bpp, (1 Cr & Cb sample per 1x2 Y samples)
			RPixelformat_YUVJ444P,  ///< planar YUV 4:4:4,            24bpp, full scale (JPEG), deprecated in favor of RPixelformat_YUV444P and setting color_range
			RPixelformat_YUV420P16LE,  ///< planar YUV 4:2:0,         24bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian
			RPixelformat_YUV420P16BE,  ///< planar YUV 4:2:0,         24bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian
			RPixelformat_YUVA422P,  ///< planar YUV 4:2:2,            24bpp, (1 Cr & Cb sample per 2x1 Y & A samples)
			RPixelformat_YUVA444P,  ///< planar YUV 4:4:4,            32bpp, (1 Cr & Cb sample per 1x1 Y & A samples)
			RPixelformat_YUV422P16LE,  ///< planar YUV 4:2:2,         32bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
			RPixelformat_YUV422P16BE,  ///< planar YUV 4:2:2,         32bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian
			RPixelformat_YUV444P16LE,  ///< planar YUV 4:4:4,         48bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian
			RPixelformat_YUV444P16BE,  ///< planar YUV 4:4:4,         48bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian
			RPixelformat_LAST_YUV = RPixelformat_YUV444P16BE,

			RPixelformat_FIRST_HSV = 50000,
			RPixelformat_HSV24       ///< packed hsv                  24bpp, h(0..180) s(0..255) v(0..255)
			= RPixelformat_FIRST_HSV,
			RPixelformat_HSV24FULL,  ///< packed hsv                  24bpp, h(0..255) s(0..255) v(0..255)
			RPixelformat_HSVSINGLE,  ///< packed hsv                  96bpp, h(0..360) s(0..1) v(0..1) stored in single precisition floating point numbers
			RPixelformat_HSVDOUBLE,  ///< packed hsv                 192bpp, h(0..360) s(0..1) v(0..1) stored in double precisition floating point numbers
			RPixelformat_HSV24P,     ///< planar hsv                  24bpp, h(0..180) s(0..255) v(0..255)
			RPixelformat_HSV24FULLP, ///< planar hsv                  24bpp, h(0..255) s(0..255) v(0..255)
			RPixelformat_HSVSINGLEP, ///< planar hsv                  96bpp, h(0..360) s(0..1) v(0..1) stored in single precisition floating point numbers
			RPixelformat_HSVDOUBLEP, ///< planar hsv                 192bpp, h(0..360) s(0..1) v(0..1) stored in double precisition floating point numbers
			RPixelformat_LAST_HSV = RPixelformat_HSVDOUBLEP,

			RPixelformat_FIRST_DEPTH = 60000,
			RPixelformat_DEPTH_16ULE,///< 16bit depth value            16bpp
			RPixelformat_LAST_DEPTH = RPixelformat_DEPTH_16ULE,

			RPixelformat_FIRST_USER = 1000000
		};
	}
}}

#include "PixelFormatDescription.hpp"
