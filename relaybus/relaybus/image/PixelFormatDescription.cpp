/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include "PixelFormat.hpp"
#include <relaybus/macros.hpp>
#include <stdexcept>

#ifdef RELAYBUS_DESCRIBE_PIXELFORMAT
#undef RELAYBUS_DESCRIBE_PIXELFORMAT
#endif

#define RELAYBUS_DESCRIBE_PIXELFORMAT( format_, pixelType_, components_, pw0_, pw1_, pw2_, pw3_, ph0_, ph1_, ph2_, ph3_, bppl0_, bppl1_, bppl2_, bppl3_, epp0_, epp1_, epp2_, epp3_, offFunc_, pixFunc_) \
																					  \
  static PixelFormatDescription<format_> PixelFormatDescription_##format_;                   \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::planeWidth<0>(image_size_type width) const                       \
  { return pw0_; EXTENSION_SYSTEM_UNUSED(width) }                                             \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::planeWidth<1>(image_size_type width) const                       \
  { return pw1_; EXTENSION_SYSTEM_UNUSED(width) }                                             \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::planeWidth<2>(image_size_type width) const                       \
  { return pw2_; EXTENSION_SYSTEM_UNUSED(width) }                                             \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::planeWidth<3>(image_size_type width) const                       \
  { return pw3_; EXTENSION_SYSTEM_UNUSED(width) }                                             \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::planeHeight<0>(image_size_type height) const                     \
  { return ph0_; EXTENSION_SYSTEM_UNUSED(height) }                                            \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::planeHeight<1>(image_size_type height) const                     \
  { return ph1_; EXTENSION_SYSTEM_UNUSED(height) }                                            \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::planeHeight<2>(image_size_type height) const                     \
  { return ph2_; EXTENSION_SYSTEM_UNUSED(height) }                                            \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::planeHeight<3>(image_size_type height) const                     \
  { return ph3_; EXTENSION_SYSTEM_UNUSED(height) }                                            \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::bytesPerPlaneLine<0>(image_size_type width) const                \
  { return bppl0_; EXTENSION_SYSTEM_UNUSED(width) }                                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::bytesPerPlaneLine<1>(image_size_type width) const                \
  { return bppl1_; EXTENSION_SYSTEM_UNUSED(width) }                                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::bytesPerPlaneLine<2>(image_size_type width) const                \
  { return bppl2_; EXTENSION_SYSTEM_UNUSED(width) }                                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::bytesPerPlaneLine<3>(image_size_type width) const                \
  { return bppl3_; EXTENSION_SYSTEM_UNUSED(width) }                                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::elementsPerPixel<0>() const                \
  { return epp0_; }                                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::elementsPerPixel<1>() const                \
  { return epp1_; }                                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::elementsPerPixel<2>() const                \
  { return epp2_; }                                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::elementsPerPixel<3>() const                \
  { return epp3_; }                                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::memoryOffset<0>(image_size_type x, image_size_type y, const image_stride_t &stride) const  \
  { EXTENSION_SYSTEM_UNUSED(x); EXTENSION_SYSTEM_UNUSED(y); EXTENSION_SYSTEM_UNUSED(stride);                  \
	const image_stride_t::size_type plane = 0; EXTENSION_SYSTEM_UNUSED(plane); offFunc_ }                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::memoryOffset<1>(image_size_type x, image_size_type y, const image_stride_t &stride) const  \
  { EXTENSION_SYSTEM_UNUSED(x); EXTENSION_SYSTEM_UNUSED(y); EXTENSION_SYSTEM_UNUSED(stride);                  \
	const image_stride_t::size_type plane = 1; EXTENSION_SYSTEM_UNUSED(plane); offFunc_ }                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::memoryOffset<2>(image_size_type x, image_size_type y, const image_stride_t &stride) const  \
  { EXTENSION_SYSTEM_UNUSED(x); EXTENSION_SYSTEM_UNUSED(y); EXTENSION_SYSTEM_UNUSED(stride);                  \
	const image_stride_t::size_type plane = 2; EXTENSION_SYSTEM_UNUSED(plane); offFunc_ }                           \
  template<> template <>                                                              \
  image_size_type PixelFormatDescription<format_>::memoryOffset<3>(image_size_type x, image_size_type y, const image_stride_t &stride) const  \
  { EXTENSION_SYSTEM_UNUSED(x); EXTENSION_SYSTEM_UNUSED(y); EXTENSION_SYSTEM_UNUSED(stride);                  \
	const image_stride_t::size_type plane = 3; EXTENSION_SYSTEM_UNUSED(plane); offFunc_ }                           \
  template<> PixelFormatDescription<format_>::Pixel PixelFormatDescription<format_>::pixel( image_size_type x, image_size_type y, const image_stride_t &stride, const image_data_t &data ) \
  { EXTENSION_SYSTEM_UNUSED(x); EXTENSION_SYSTEM_UNUSED(y); EXTENSION_SYSTEM_UNUSED(stride); EXTENSION_SYSTEM_UNUSED(data); \
	pixFunc_ }

using namespace relaybus;
using namespace relaybus::image;
using namespace relaybus::image::RPixelformat;

#include "PixelFormatDescription.pixeldef"

#define RELAYBUS_ACCESS_PIXELFORMAT(format_) case format_: return PixelFormatDescription_##format_

RPixelformat::PixelFormatDescriptionBase& RPixelformat::PixelFormatDescriptionBase::get( RPixelformat::Format format )
{
  switch( format )
  {
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_NONE);
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_GRAY8);
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_RGB24);
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_ARGB);
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_ABGR);
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_RGBA);
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_BGRA);
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_YUV420P);
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_YUVJ420P);
	RELAYBUS_ACCESS_PIXELFORMAT(RPixelformat_DEPTH_16ULE);
	default:
		throw std::runtime_error("PixelFormat::PixelFormatDescriptionBase::get: unhandled PixelFormat");
  }
}

