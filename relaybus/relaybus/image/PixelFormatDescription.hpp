/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>

#include <relaybus/TypeSystem.hpp>

#include <relaybus/image/BaseImageData.hpp>

// pw = planeWidth
// ph = planeHeight
// bppl = bytesPerPlaneLine
// epp = elementsPerPixel
#define RELAYBUS_DESCRIBE_PIXELFORMAT( format_, pixelType_, components_, pw0_, pw1_, pw2_, pw3_, ph0_, ph1_, ph2_, ph3_, bppl0_, bppl1_, bppl2_, bppl3_, epp0_, epp1_, epp2_, epp3_, offFunc_, pixFunc_) \
  namespace {                                                                         \
	template<>                                                                        \
	struct PixelFormatType<format_> {                                                 \
		enum { \
			pixel_size_in_byte = sizeof(pixelType_), \
			number_of_components = components_ \
		}; \
		typedef pixelType_	PixelType; \
		typedef std::array<pixelType_, components_> Pixel;                              \
	};                                                                                \
  }                                                                                   \
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::planeWidth<0>(image_size_type width) const;   \
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::planeWidth<1>(image_size_type width) const;   \
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::planeWidth<2>(image_size_type width) const;   \
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::planeWidth<3>(image_size_type width) const;   \
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::planeHeight<0>(image_size_type height) const; \
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::planeHeight<1>(image_size_type height) const; \
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::planeHeight<2>(image_size_type height) const; \
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::planeHeight<3>(image_size_type height) const; \
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::bytesPerPlaneLine<0>(image_size_type width) const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::bytesPerPlaneLine<1>(image_size_type width) const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::bytesPerPlaneLine<2>(image_size_type width) const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::bytesPerPlaneLine<3>(image_size_type width) const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::elementsPerPixel<0>() const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::elementsPerPixel<1>() const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::elementsPerPixel<2>() const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::elementsPerPixel<3>() const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::memoryOffset<0>(image_size_type x, image_size_type y, const image_stride_t &stride) const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::memoryOffset<1>(image_size_type x, image_size_type y, const image_stride_t &stride) const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::memoryOffset<2>(image_size_type x, image_size_type y, const image_stride_t &stride) const;\
  template<> template <>                                                              \
  image_size_type RELAYBUS_API PixelFormatDescription<format_>::memoryOffset<3>(image_size_type x, image_size_type y, const image_stride_t &stride) const;\
  template<> PixelFormatDescription<format_>::Pixel RELAYBUS_API PixelFormatDescription<format_>::pixel( image_size_type x, image_size_type y, const image_stride_t &stride, const image_data_t &data );

namespace relaybus { namespace image { namespace RPixelformat {
	namespace {
		template<RPixelformat::Format pixelFormat>
		struct PixelFormatType {};
	}

	class RELAYBUS_API PixelFormatDescriptionBase {
	public:
		virtual ~PixelFormatDescriptionBase() {}

		virtual image_size_type numberOfComponents() const = 0;

		virtual type_id pixelTypeID() const = 0;
		virtual const std::type_info &pixelType() const = 0;
		virtual std::string pixelTypeName() const = 0;
		virtual image_size_type sizeOfThePixelType() const = 0;

		virtual image_size_type bytesPerPlaneLine(image_stride_t::size_type plane, image_size_type width) const = 0;
		virtual image_size_type elementsPerPixel(image_stride_t::size_type plane) const = 0;
		virtual image_size_type memoryOffset(image_stride_t::size_type plane, image_size_type x, image_size_type y, const image_stride_t &stride) const = 0;
		virtual image_size_type planeWidth(image_stride_t::size_type plane, image_size_type width) const = 0;
		virtual image_size_type planeHeight(image_stride_t::size_type plane, image_size_type height) const = 0;

		static PixelFormatDescriptionBase &get(RPixelformat::Format format);
	};

	template <RPixelformat::Format pixelFormat>
	class PixelFormatDescription : public PixelFormatDescriptionBase {
	public:
		typedef typename PixelFormatType<pixelFormat>::Pixel Pixel;

		template<int planeNumber>
		inline image_size_type planeWidth(image_size_type imageWidth) const;

		template<int planeNumber>
		inline image_size_type planeHeight(image_size_type imageWidth) const;

		template<int planeNumber>
		inline image_size_type bytesPerPlaneLine(image_size_type width) const;

		template<int planeNumber>
		inline image_size_type elementsPerPixel() const;

		template<int planeNumber>
		inline image_size_type memoryOffset(image_size_type x, image_size_type y, const image_stride_t &stride) const;


		image_size_type numberOfComponents() const override {
			return PixelFormatType<pixelFormat>::number_of_components;
		}

		type_id pixelTypeID() const override {
			return TypeSystem<typename PixelFormatType<pixelFormat>::PixelType>::id;
		}

		const std::type_info &pixelType() const override {
			return TypeSystem<typename PixelFormatType<pixelFormat>::PixelType>::get();
		}

		std::string pixelTypeName() const override {
			return extension_system::InterfaceName<typename PixelFormatType<pixelFormat>::PixelType>::getString();
		}

		image_size_type sizeOfThePixelType() const override {
			return PixelFormatType<pixelFormat>::pixel_size_in_byte;
		}

		image_size_type planeWidth(image_stride_t::size_type plane, image_size_type width) const override {
			switch(plane) {
				case 0: return planeWidth<0>(width);
				case 1: return planeWidth<1>(width);
				case 2: return planeWidth<2>(width);
				case 3: return planeWidth<3>(width);
			}
			return 0;
		}

		image_size_type planeHeight(image_stride_t::size_type plane, image_size_type height) const override {
			switch(plane) {
				case 0: return planeHeight<0>(height);
				case 1: return planeHeight<1>(height);
				case 2: return planeHeight<2>(height);
				case 3: return planeHeight<3>(height);
			}
			return 0;
		}

		image_size_type bytesPerPlaneLine(image_stride_t::size_type plane, image_size_type imageWidth) const override {
			switch(plane) {
				case 0: return bytesPerPlaneLine<0>(imageWidth);
				case 1: return bytesPerPlaneLine<1>(imageWidth);
				case 2: return bytesPerPlaneLine<2>(imageWidth);
				case 3: return bytesPerPlaneLine<3>(imageWidth);
			}
			return 0;
		}

		image_size_type elementsPerPixel(image_stride_t::size_type plane) const override {
			switch(plane) {
				case 0: return elementsPerPixel<0>();
				case 1: return elementsPerPixel<1>();
				case 2: return elementsPerPixel<2>();
				case 3: return elementsPerPixel<3>();
			}
			return 0;
		}

		image_size_type memoryOffset(image_stride_t::size_type plane, image_size_type x, image_size_type y, const image_stride_t &stride) const override {
			switch(plane) {
				case 0: return memoryOffset<0>(x,y, stride);
				case 1: return memoryOffset<1>(x,y, stride);
				case 2: return memoryOffset<2>(x,y, stride);
				case 3: return memoryOffset<3>(x,y, stride);
			}
			return 0;
		}

		Pixel pixel( image_size_type x, image_size_type y, const image_stride_t &stride, const image_data_t &data );
	};

	#include "PixelFormatDescription.pixeldef"

} } }
