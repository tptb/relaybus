/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include <extension_system/macros.hpp>

#ifndef RELAYBUS_LIB
#  ifdef RELAYBUS_EXPORTS
#    define RELAYBUS_API EXTENSION_SYSTEM_EXPORT
#  else
#    define RELAYBUS_API EXTENSION_SYSTEM_IMPORT
#  endif
#else
#  define RELAYBUS_API
#endif
