/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <string>
#include <vector>
#include <cstdint>

#include "SharedArray.hpp"

#include "TypeSystem.hpp"

#include "template.hpp"

namespace relaybus {
	namespace CodecType {
		enum CodecType {
			Codec_Unknown = 0,
			Codec_Video = 10000,
			Codec_Video_MPEG1,
			Codec_Video_MPEG2,
			Codec_Video_H261,
			Codec_Video_H263,
			Codec_Video_RV10,
			Codec_Video_RV20,
			Codec_Video_MJPEG,
			Codec_Video_MJPEGB,
			Codec_Video_LJPEG,
			Codec_Video_SP5X,
			Codec_Video_JPEGLS,
			Codec_Video_MPEG4,
			Codec_Video_RAW,
			Codec_Video_MSMPEG4V1,
			Codec_Video_MSMPEG4V2,
			Codec_Video_MSMPEG4V3,
			Codec_Video_WMV1,
			Codec_Video_WMV2,
			Codec_Video_H263P,
			Codec_Video_H263I,
			Codec_Video_FLV1,
			Codec_Video_SVQ1,
			Codec_Video_SVQ3,
			Codec_Video_DV,
			Codec_Video_HUFFYUV,
			Codec_Video_CYUV,
			Codec_Video_H264,
			Codec_Video_INDEO3,
			Codec_Video_VP3,
			Codec_Video_THEORA,
			Codec_Video_VP8,
			Codec_Video_Last,
			Codec_Audio = 20000,
			Codec_Audio_MP3,
			Codec_Audio_Last,
			Codec_Count
		};
	}

	namespace StreamType {
		enum StreamType {
			Stream_Unknown = 0,
			Stream_Video,
			Stream_Audio,
			Stream_Data,
			Stream_Subtitle,
			Stream_NB,
			Stream_Attachment,
			Stream_Count
		};
	}

	namespace StreamPacketFlags {
		enum StreamPacketFlags {
			Flag_None = 0,
			Flag_KeyFrame	= 1 << 0,
			Flag_Corrupted	= 1 << 1,
			Flag_Count		= 1 << 2,
			Flag_Unknown	= 1 << 3 // used to indicate that there are no information about the StreamPacket
		};
	}

	struct StreamPacket {
		uint64_t timeStamp; // time stamp in micro seconds
		int streamNumber; // number of stream the packet belongs to
		StreamPacketFlags::StreamPacketFlags flags;
		SharedArray<uint8_t> data;
	};
	//RELAYBUS_SIMPLE_TYPE(StreamPacket)

	struct StreamInformation {
		StreamType::StreamType streamType;
		CodecType::CodecType codecType;
		int streamNumber;
		double framesPerSecond;
		SharedArray<uint8_t> extraData;
	};
	//RELAYBUS_SIMPLE_TYPE(StreamInformation)

	typedef std::vector<StreamInformation> StreamInformationVector;
}

RELAYBUS_DECLARE_TYPE(relaybus::CodecType::CodecType, ' ','r','c','t')
RELAYBUS_DECLARE_TYPE(relaybus::StreamType::StreamType, ' ','r','s','t')
RELAYBUS_DECLARE_TYPE(relaybus::StreamPacketFlags::StreamPacketFlags, 'r','s','p','f')
RELAYBUS_DECLARE_TYPE(relaybus::StreamPacket, ' ','r','s','p')
RELAYBUS_DECLARE_TYPE(relaybus::StreamInformation, ' ','r','s','i')
RELAYBUS_DECLARE_TYPE(relaybus::StreamInformationVector, 'r','s','i','v')
