/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <type_traits>

namespace std {
	template<typename _CharT, typename _Traits, typename _Alloc>
	class basic_string;
}

#define RELAYBUS_SIMPLE_TYPE(T) \
	namespace relaybus { \
		template<> \
		struct SimpleType<T> : public std::true_type {}; \
	}

namespace relaybus {

	template<typename T, typename Enable = void>
	struct SimpleType : public std::false_type {};

	template<typename T>
	struct SimpleType<T, typename std::enable_if<std::is_fundamental<T>::value>::type>
		: public std::true_type {};

	template<typename T>
	struct SimpleType<T, typename std::enable_if<std::is_enum<T>::value>::type>
		: public std::true_type {};

	template<typename T1, typename T2, typename T3>
	struct SimpleType<std::basic_string<T1, T2, T3> >
		: public std::true_type {};
}
