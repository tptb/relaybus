#include "thread.hpp"

#ifdef RELAYBUS_COMPILER_MSVC
	#include <Windows.h>
#else
	#include <pthread.h>
	#include <iostream>
#endif

namespace relaybus {

#ifdef RELAYBUS_COMPILER_MSVC
	void setThreadNameWindows(DWORD dwThreadID, LPCSTR szThreadName) {
#ifdef _DEBUG

#pragma pack(push,8)
		struct THREADNAME_INFO {
			DWORD dwType; // Must be 0x1000.
			LPCSTR szName; // Pointer to name (in user addr space).
			DWORD dwThreadID; // Thread ID (-1=caller thread).
			DWORD dwFlags; // Reserved for future use, must be zero.
		};
#pragma pack(pop)

		THREADNAME_INFO info;
		info.dwType = 0x1000;
		info.szName = szThreadName;
		info.dwThreadID = dwThreadID;
		info.dwFlags = 0;

		__try {
			RaiseException( 0x406D1388, 0, sizeof(info)/sizeof(ULONG_PTR), (ULONG_PTR*)&info );
		} __except(EXCEPTION_EXECUTE_HANDLER) {}
#else
		EXTENSION_SYSTEM_UNUSED(dwThreadID);
		EXTENSION_SYSTEM_UNUSED(szThreadName);
#endif
	}
#else

#endif

void setThreadName(std::thread &t, const std::string &name) {
#if defined(RELAYBUS_COMPILER_MSVC)

	// TODO: check if required if boost is used
	/*
	// convert HEX-string to DWORD
	unsigned int dwThreadId;
	std::stringstream ss;
	ss << std::hex << t.get_id();
	ss >> dwThreadId;*/

	setThreadNameWindows((DWORD)t.native_handle(), name.c_str());

#elif !defined(RELAYBUS_COMPILER_MINGW)
	const int name_max_len = 16;
	std::string limited_name = name;
	if(limited_name.length() > 16)
		limited_name = limited_name.substr(limited_name.length()-name_max_len, name_max_len);
	pthread_setname_np(t.native_handle(), limited_name.c_str());
#endif
}

}
