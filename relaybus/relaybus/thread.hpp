/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/macros.hpp>

#include <thread>

namespace relaybus {

	/**
	 * sets the name of a given thread, the function should be
	 */
	RELAYBUS_API void setThreadName(std::thread &t, const std::string &name);
}
