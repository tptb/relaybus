
#include <gloox/client.h>
#include <gloox/messagehandler.h>

using namespace gloox;

class Bot : public MessageHandler
{
 public:
   Bot()
   {
      JID jid( "bot@server/resource" );
      j = new Client( jid, "pwd" );
      j->registerMessageHandler( this );
      j->connect();
   }
   virtual void handleMessage( const Message& stanza,
                               MessageSession* session = 0 )
   {
     Message msg( stanza.from(), "hello world" );
     j->send( msg );
   }

 private:
   Client* j;
};
int main( int argc, char* argv[] )
{
  Bot b;
}