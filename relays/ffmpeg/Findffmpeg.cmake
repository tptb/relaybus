find_package(PkgConfig)

if(PKG_CONFIG_FOUND)
  pkg_search_module(LIBAVCODEC REQUIRED QUIET libavcodec)
  pkg_search_module(LIBAVDEVICE REQUIRED QUIET libavdevice)
  pkg_search_module(LIBAVFILTER REQUIRED QUIET libavfilter)
  pkg_search_module(LIBAVFORMAT REQUIRED QUIET libavformat)
  pkg_search_module(LIBAVUTIL REQUIRED QUIET libavutil)
  pkg_search_module(LIBPOSTPROC REQUIRED QUIET libpostproc)
  pkg_search_module(LIBSWRESAMPLE REQUIRED QUIET libswresample)
  pkg_search_module(LIBSWSCALE REQUIRED QUIET libswscale)

  set(FFMPEG_INCLUDE_DIRS FFMPEG_LIBAVCODEC_INCLUDE_DIRS)
  set(FFMPEG_LIBRARY_DIRS FFMPEG_LIBAVCODEC_LIBRARY_DIRS)

else()

	if(NOT FFMPEG_ROOT)
		find_path(FFMPEG_ROOT include/libavcodec/avcodec.h )
	endif()

	set(FFMPEG_INCLUDE_DIRS "${FFMPEG_ROOT}/include")
	set(FFMPEG_LIBRARY_DIRS "${FFMPEG_ROOT}/lib")

	find_library(LIBAVCODEC_LIBRARIES avcodec HINTS "${FFMPEG_LIBRARY_DIRS}")
	find_library(LIBAVDEVICE_LIBRARIES avdevice HINTS "${FFMPEG_LIBRARY_DIRS}")
	find_library(LIBAVFILTER_LIBRARIES avfilter HINTS "${FFMPEG_LIBRARY_DIRS}")
	find_library(LIBAVFORMAT_LIBRARIES avformat HINTS "${FFMPEG_LIBRARY_DIRS}")
	find_library(LIBAVUTIL_LIBRARIES avutil HINTS "${FFMPEG_LIBRARY_DIRS}")
	find_library(LIBPOSTPROC_LIBRARIES postproc HINTS "${FFMPEG_LIBRARY_DIRS}")
	find_library(LIBSWRESAMPLE_LIBRARIES swresample HINTS "${FFMPEG_LIBRARY_DIRS}")
	find_library(LIBSWSCALE_LIBRARIES swscale HINTS "${FFMPEG_LIBRARY_DIRS}")

	mark_as_advanced(LIBAVCODEC_LIBRARIES LIBAVDEVICE_LIBRARIES LIBAVFILTER_LIBRARIES
						LIBAVFORMAT_LIBRARIES LIBAVUTIL_LIBRARIES LIBPOSTPROC_LIBRARIES
						LIBSWRESAMPLE_LIBRARIES LIBSWSCALE_LIBRARIES)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ffmpeg REQUIRED_VARS LIBAVCODEC_LIBRARIES)
