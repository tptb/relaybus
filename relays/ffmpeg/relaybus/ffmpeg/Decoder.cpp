/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include <stdexcept>
#include <relaybus/MakeString.hpp>
#include "FFmpeg.hpp"
#include <relaybus/Relay.hpp>

using namespace relaybus::image;

namespace relaybus { namespace ffmpeg {

static BaseImageAllocator nullAllocator;

class Decoder : public Relay {
public:
	Decoder() :
		_streamNumber(0),
		_codecContext(nullptr),
		_codec(nullptr),
		_frame(nullptr)
	{
		FFmpeg::avcodec_register_all();
	}

private:
	void releaseData() {
		FFmpeg::avcodec_close(_codecContext);
		av_free(_codecContext);
		av_frame_free(&_frame);
	}

	void init() override {
		_streamNumber = 0;
		publish("stream/information", _stream_information);
		publish("stream/packet", _stream_packet);
		publish("image",  _image);
	}

	void deinit() override {
		releaseData();
	}

	void process() override {
		waitForPendingUpdates();

		if(_stream_information.tryUpdate()) {

			if(!_stream_information.isValid()) {
				_image = nullptr;
				_image.submit();
				return;
			}

			_streamNumber = _stream_information.r().streamNumber;

			AVCodecID codecID = translateCodec(_stream_information.r().codecType);

			if(codecID == CODEC_ID_NONE)
				reportError(MakeString() << "unhandled codec type " << _stream_information.r().codecType);

			_codec = avcodec_find_decoder(codecID);

			_codecContext = avcodec_alloc_context3(_codec);

			if(_codec->capabilities & CODEC_CAP_TRUNCATED)
				_codecContext->flags |= CODEC_FLAG_TRUNCATED;

			//if( streamInformation.codecType == CodecType::Codec_Video_H264)
			_codecContext->flags2 |= CODEC_FLAG2_CHUNKS;

			_codecContext->extradata = _stream_information.rw().extraData.data();
			_codecContext->extradata_size = _stream_information.rw().extraData.size();

			int error = FFmpeg::avcodec_open2(_codecContext, _codec, nullptr);
			EXTENSION_SYSTEM_UNUSED(error);

			_frame = av_frame_alloc();
			_stream_information.submit();
		}

		if(_stream_packet.tryUpdate() && _codecContext) {
			if(!_stream_packet.isValid()) {
				_image = nullptr;
				_image.submit();
				return;
			}

			// only decode packets of the right stream
			if( _stream_packet.r().streamNumber != _streamNumber )
				return;

			// setup AVPackert for decoding
			AVPacket packet;
			memset(&packet, 0, sizeof(AVPacket));
			packet.data = _stream_packet.rw().data.data();
			packet.size = _stream_packet.rw().data.size();

			int gotFrame = 0;

			// when multiple frames are within one packet, decode all of them
			while (packet.size > 0 ) {
				int len = avcodec_decode_video2(_codecContext, _frame, &gotFrame, &packet);

				// stop decoding of packet on error
				if( len < 0 )
					break;

				// if a frame could be retrieved from stream
				if( gotFrame) {
					// build image struct
					_image.f() = BaseImage( _frame->width, _frame->height,
										translateFormat(_frame->format),
										convertStride(_frame->linesize),
										convertData(_frame->data),
										nullptr ).cloneData();
					_image.submit();
				}

				packet.data += len;
				packet.size -= len;
			}

			_stream_packet.submit();
		}
	}

	int _streamNumber;
	AVCodecContext* _codecContext;
	AVCodec* _codec;
	AVFrame* _frame;

	Input<StreamInformation, DataProfile>	_stream_information;
	Input<StreamPacket, DataProfile>		_stream_packet;
	Output<BaseImageView>					_image;
};

}}


RELAYBUS_RELAY(relaybus::ffmpeg::Decoder, 1, "Decoder using ffmpeg")
