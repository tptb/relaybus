/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#if 0
#include "relaybus_ffmpeg_shared.hpp"

#include <list>
#include <relaybus/Relay.hpp>

namespace relaybus { namespace ffmpeg {

struct EncoderSettings
{
	int bitrate;
	int gopSize;
	CodecType::CodecType codecType;
};

static BaseImageAllocator nullAllocator;

class Encoder : public Relay {
public:
	Encoder() :
		_codecContext(nullptr),
		_codec(nullptr),
		_frame(nullptr),
		_packetDataHandler(nullptr)

	{
		avcodec_register_all();
		AVCodec* c = nullptr;

		while( (c = av_codec_next(c)) )
		{
			relaybus::CodecType::CodecType codecType = ffmpeg::translateCodec(c->id);
			if( av_codec_is_encoder(c) && codecType > CodecType::Codec_Unknown && codecType < CodecType::Codec_Count )
				_supportedCodecs.push_back(codecType);
		}
	}

private:
	void onData( BaseImageView* image )
	{
		if(image == nullptr)
			return;
		if( _codecContext == nullptr )
		{
			_codecContext = avcodec_alloc_context3(_codec);
			_codecContext->bit_rate = _encoderSettings.bitrate;
			_codecContext->gop_size = _encoderSettings.gopSize;
			_codecContext->pix_fmt = translateFormat(image->format());
			_codecContext->width = image->width();
			_codecContext->height = image->height();
		}

		// setup AVPackert for decoding
		AVPacket packet;
		AVFrame frame;
		memset(&packet, 0, sizeof(AVPacket));

		int gotPacket;

		avcodec_encode_video2(_codecContext, &packet, &frame, &gotPacket);
	}

	void setStreamInformation( StreamInformation & /*streamInformation*/ )
	{
		//_streamNumber = streamInformation.streamNumber;

		//switch(streamInformation.codecType)
		//{
		//	// add more codecs here !
	 // case CodecType::Codec_Video_H264:
		//	_codec = avcodec_find_decoder(CODEC_ID_H264);
		//	break;

		//}

		//_codecContext = avcodec_alloc_context3(_codec);

		//if(_codec->capabilities & CODEC_CAP_TRUNCATED)
		//	_codecContext->flags|=CODEC_FLAG_TRUNCATED;

		//if( streamInformation.codecType == CodecType::Codec_Video_H264)
		//	_codecContext->flags2 |= CODEC_FLAG2_CHUNKS;


		//_codecContext->extradata = streamInformation.extraData.data();
		//_codecContext->extradata_size = streamInformation.extraData.size();

		//int error = avcodec_open2(_codecContext, _codec, NULL);

		//_frame = avcodec_alloc_frame();
	}

	void releaseData() {
		av_free(_codecContext);
		av_free(_frame);
	}

	void setOnEncodedPacketHandler( IDataHandler<StreamPacket*>* pHandler )
	{
		_packetDataHandler = pHandler;
	}

	void configureEncoder( const EncoderSettings& settings )
	{
		_codec = avcodec_find_decoder(ffmpeg::translateCodec(settings.codecType));

		if( _codecContext != nullptr )
		{
			av_free(_codecContext);
			_codecContext = nullptr;
		}

		_encoderSettings = settings;
	}

	void releaseData();

	AVCodecContext* _codecContext;
	EncoderSettings _encoderSettings;
	AVCodec* _codec;
	AVFrame* _frame;
	IDataHandler<StreamPacket*>* _packetDataHandler;
	std::list<CodecType::CodecType> _supportedCodecs;
};

}}

RELAYBUS_RELAY(relaybus::ffmpeg::Encoder, 1, "Encoder using ffmpeg")

#endif
