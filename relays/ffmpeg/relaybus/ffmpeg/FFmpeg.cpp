/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include "FFmpeg.hpp"

using namespace relaybus::ffmpeg;

static std::mutex	_mutex;

int FFmpeg::avcodec_open2(AVCodecContext *avctx, const AVCodec *codec, AVDictionary **options) {
	std::unique_lock<std::mutex> lock(_mutex);
	return ::avcodec_open2(avctx, codec, options);
}

int FFmpeg::avcodec_close(AVCodecContext *avctx) {
	std::unique_lock<std::mutex> lock(_mutex);
	if(avctx)
		return ::avcodec_close(avctx);
	return 0;
}

void FFmpeg::avcodec_register_all() {
	std::unique_lock<std::mutex> lock(_mutex);
	::avcodec_register_all();
}
