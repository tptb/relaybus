/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include "relaybus_ffmpeg_shared.hpp"

#include <mutex>

namespace relaybus { namespace ffmpeg {

class FFmpeg {
public:
	static int avcodec_open2(AVCodecContext *avctx, const AVCodec *codec, AVDictionary **options);

	static int avcodec_close(AVCodecContext *avctx);

	static void avcodec_register_all();
};

}}
