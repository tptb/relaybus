/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-pedantic"
extern "C" {
	#include <libswscale/swscale.h>
}
#pragma GCC diagnostic pop

#include "relaybus_ffmpeg_shared.hpp"
#include <relaybus/Relay.hpp>

using namespace relaybus::image;

namespace relaybus { namespace ffmpeg {

class Scaler : public Relay {
public:
	Scaler() : _scale_ctx(nullptr) {}
private:

	void freeContext() {
		sws_freeContext(_scale_ctx);
	}

	void init() override {
		_width.f() = 0;
		_height.f() = 0;
		_pixelformat.f() = RPixelformat::RPixelformat_NONE;

		publish("input",  _input);
		publish("output",   _output);
		publish("width",  _width);
		publish("height", _height);
		publish("pixelformat", _pixelformat);
	}

	void deinit() override {
		freeContext();
	}

	void process() override {
		waitForPendingUpdates();

		if(_width.tryUpdate())
			_width.submit();

		if(_height.tryUpdate())
			_height.submit();

		if(_pixelformat.tryUpdate())
			_pixelformat.submit();

		if(_input.tryUpdate() && _input != nullptr) {

			if(_pixelformat.r() == RPixelformat::RPixelformat_NONE &&
					_width.r() == 0 && _height.r() == 0) {
				_output.f() = _input.r();
			} else {
				int width = (_width.r() == 0) ? _input.r().width() : _width.r();
				int height = (_height.r() == 0) ? _input.r().height() : _height.r();
				auto pixelformat = (_pixelformat.r() == RPixelformat::RPixelformat_NONE) ? _input.r().format() : _pixelformat.r();

				_scale_ctx = sws_getCachedContext(_scale_ctx,
													_input.r().width(), _input.r().height(),
													translateFormat(_input.r().format()),
													width, height, translateFormat(pixelformat),
													SWS_BICUBIC, nullptr, nullptr, nullptr); //SWS_FAST_BILINEAR or SWS_POINT

				AVFrame* frame = av_frame_alloc();
				int size = avpicture_get_size(translateFormat(pixelformat), width, height);
				uint8_t* picture_buf = reinterpret_cast<uint8_t*>(av_malloc(size));
				avpicture_fill(reinterpret_cast<AVPicture*>(frame), picture_buf, translateFormat(pixelformat), width, height);

				int stride[AV_NUM_DATA_POINTERS];
				uint8_t *data[AV_NUM_DATA_POINTERS];
				convertStride(stride, _input.r().getBaseImageDataStruct()->_stride);
				convertData(data, _input.r().getBaseImageDataStruct()->_data);

				sws_scale(_scale_ctx, data, stride, 0, height, frame->data, frame->linesize);

				_output.f() = BaseImage(width, height, pixelformat,
										convertStride(frame->linesize), convertData(frame->data), nullptr).cloneData();

				av_free(frame);
				av_free(picture_buf);
			}

			_output.submit();
			_input.submit();
		}
	}

	SwsContext *_scale_ctx;

	Input<BaseImageView, DataProfile>				_input;
	Output<BaseImageView>							_output;
	Input<image_size_type, ParameterProfile>		_width;
	Input<image_size_type, ParameterProfile>		_height;
	Input<RPixelformat::Format, ParameterProfile>	_pixelformat;
};

}}

RELAYBUS_RELAY(relaybus::ffmpeg::Scaler, 1, "Scaler using ffmpeg")
