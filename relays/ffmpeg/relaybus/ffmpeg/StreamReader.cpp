/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-pedantic"
extern "C"{
	#include <libavformat/avformat.h>
}
#pragma GCC diagnostic pop

#include "relaybus_ffmpeg_shared.hpp"
#include <relaybus/Relay.hpp>
#include <cstring>

namespace relaybus { namespace ffmpeg {

class StreamReader : public Relay {
public:
	StreamReader() : _formatContext(nullptr), _buffer_size(1024*1024) {}
private:

	void init() override {
		avcodec_register_all();
		av_register_all();

		_loop.rw() = true;

		publish("loop", _loop);

		publish("uri", _uri);
		publish("input_packet", _input_stream_packet);

		publish("stream/information/0", _stream_information);
		publish("stream/information/vec", _stream_informations);
		publish("stream/packet", _stream_packet);
	}

	void close() {
		if(_avioContext) {
			av_free(_avioContext->buffer);
			_avioContext.reset();
		}
		if(_formatContext)
			avformat_close_input( &_formatContext );
	}

	void deinit() override {
		// todo avformat_network_deinit
		close();
	}

	static int readFunction(void* opaque, uint8_t* buf, int buf_size) {
		auto& me = *reinterpret_cast<StreamReader*>(opaque);
		me._input_stream_packet.update();
		if(me._input_stream_packet == nullptr)
			return 0;
		auto &streamPacket = me._input_stream_packet.r();
		int size = std::min(buf_size, static_cast<int>(streamPacket.data.size()));
		memcpy(buf, streamPacket.data.data(), size);
		return size;
	}

	void process() override {
		waitForWires();

		const std::string relaybus_use_input_packet = "relaybus_use_input_packet";

		if(_uri.tryUpdate() ||
				(_formatContext == nullptr && _input_stream_packet.hasPendingUpdates()
					&& _uri.r() == relaybus_use_input_packet)) {
			close();

			if(_uri.r() != relaybus_use_input_packet) {
				const std::string rtsp = "rtsp";
				if(std::equal(rtsp.begin(), rtsp.end(), _uri.r().begin()))
					avformat_network_init();
			}

			// TODO: Error handling
			int error = 0; EXTENSION_SYSTEM_UNUSED(error);
			_formatContext = avformat_alloc_context();
			// _formatContext = std::makes_shared<AVFormatContext>(avformat_alloc_context(), &avformat_free_context); ??

			if(_uri.r() != relaybus_use_input_packet) {
				error = avformat_open_input( &_formatContext, _uri.r().data(), nullptr, nullptr);
			} else {
				_buffer.reset(reinterpret_cast<unsigned char*>(av_malloc(_buffer_size)), &av_free);
				_avioContext.reset(avio_alloc_context(_buffer.get(), _buffer_size, 0, reinterpret_cast<void*>(this), &StreamReader::readFunction, nullptr, nullptr), &av_free);
				_formatContext->pb = _avioContext.get();
				error = avformat_open_input( &_formatContext, "stream/input_packet", nullptr, nullptr);
			}

			if(_formatContext == nullptr) {
				close();
				return;
			}

			_stream_informations.f().clear();
			_stream_informations.rw().resize(_formatContext->nb_streams);

			error = avformat_find_stream_info( _formatContext, nullptr);

			for (unsigned int i = 0; i < _formatContext->nb_streams; i++) {
				StreamInformation &cur =  _stream_informations.rw()[i];
				cur.codecType = translateCodec(_formatContext->streams[i]->codec->codec_id);
				cur.streamType = translateStreamType(_formatContext->streams[i]->codec->codec_type);
				cur.streamNumber = i;
				if(_formatContext->streams[i]->codec->extradata_size > 0) {
					cur.extraData = SharedArray<uint8_t>(_formatContext->streams[i]->codec->extradata_size);
					memcpy(cur.extraData.data(), _formatContext->streams[i]->codec->extradata, cur.extraData.size());
				} else {
					cur.extraData = SharedArray<uint8_t>();
				}
				cur.framesPerSecond = av_q2d(_formatContext->streams[i]->r_frame_rate);
			}

			_uri.submit();
			_input_stream_packet.submit();
			_stream_informations.submit();

			if(_stream_informations.r().size() > 0)
				_stream_information.w() = _stream_informations.r()[0];
			_stream_information.submit();
		}

		// TODO
		// av_dict_set(&_formatOptions, key.c_str(), value.c_str(), flags);

		if(_formatContext == nullptr) {
			waitForPendingUpdates();
			return;
		}

		AVPacket packet;
		int err = av_read_frame( _formatContext, &packet );

		if(0 ==  err) {

			AVRational base = _formatContext->streams[packet.stream_index]->time_base;

			_stream_packet.f().timeStamp = packet.pts * base.num * 1000 * 1000 / base.den;
			_stream_packet.rw().streamNumber = packet.stream_index;

			_stream_packet.rw().flags = StreamPacketFlags::Flag_None;
			if( (packet.flags & AV_PKT_FLAG_KEY) != 0)
				_stream_packet.rw().flags = static_cast<StreamPacketFlags::StreamPacketFlags>(
												_stream_packet.rw().flags | StreamPacketFlags::Flag_KeyFrame);
			if( (packet.flags & AV_PKT_FLAG_CORRUPT) != 0)
				_stream_packet.rw().flags = static_cast<StreamPacketFlags::StreamPacketFlags>(
											_stream_packet.rw().flags | StreamPacketFlags::Flag_Corrupted);

			if(packet.size > 0) {
				_stream_packet.rw().data = SharedArray<uint8_t>(packet.size);
				memcpy(_stream_packet.rw().data.data(), packet.data, packet.size);
			} else {
				_stream_packet.rw().data = SharedArray<uint8_t>();
			}

			_stream_packet.submit();

			av_free_packet(&packet);
		} else {
			// TODO: how can we distinguish between an error and an end of file
			_loop.tryUpdate();
			if(_loop.r())
				av_seek_frame(_formatContext, -1, 0, AVSEEK_FLAG_ANY);
		}
	}

	Input<bool, ParameterProfile>	_loop;
	Input<std::string, ParameterProfile>	_uri;
	Input<StreamPacket, DataProfile> _input_stream_packet;

	Output<StreamInformation> _stream_information;
	Output<StreamInformationVector> _stream_informations;
	Output<StreamPacket>	_stream_packet;
	AVFormatContext *_formatContext;

	//
	const std::size_t _buffer_size;
	std::shared_ptr<unsigned char> _buffer;
	std::shared_ptr<AVIOContext> _avioContext;
};

}}

RELAYBUS_RELAY(relaybus::ffmpeg::StreamReader, 1, "StreamReader using ffmpeg")
