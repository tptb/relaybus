/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#if 0

#include <string>
#include <stdint.h>

#include <relaybus/Relay.hpp>

#include <relaybus/streaming.hpp>

#include <relaybus/MakeString.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-pedantic"
extern "C"{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
}
#pragma GCC diagnostic pop

#include <stdio.h>
#include <stdlib.h>

#include "relaybus_ffmpeg_shared.hpp"

#include <relaybus/relaybus_macros.hpp>

struct AVPacket;
struct AVFormatContext;
struct AVDictionary;
struct AVCodecContext;
struct AVCodec;
struct AVStream;
struct AVOutputFormat;

namespace relaybus { namespace ffmpeg {


class StreamWriter : public IDataHandler<StreamPacket*> {
public:

	StreamWriter( );
	~StreamWriter();

	void open();
	void close();

	void setURI( const std::string &uri ) {
		_uri = uri;
	}

	void onData( StreamPacket *streamPacket ) override;

	/**
	 * @brief setFormatOptions
	 *	you have to set them before calling open
	 * @param key
	 * @param value
	 * @param flags
	 */
	virtual void setFormatOptions(const std::string &key, const std::string &value, int flags);

	void setStreamInformation(StreamInformation &streamInformation);

  private:
	AVFormatContext *_formatContext;
	AVDictionary *_formatOptions;
	bool _new_file;
	AVCodecContext* _codecContext;
	AVCodec* _codec;
	AVStream *_stream;
	AVOutputFormat *_outputFormat;
	int _streamNumber;
	StreamInformation _streamInformation;

	std::string _uri;
};

StreamWriter::StreamWriter()
	: _formatContext(nullptr), _formatOptions(nullptr), _new_file(true),
	  _codecContext(nullptr), _codec(nullptr),
	  _stream(nullptr), _outputFormat(nullptr), _streamNumber(0)
{
	avcodec_register_all();
	av_register_all();
}

void StreamWriter::setFormatOptions(const std::string &key, const std::string &value, int flags)
{
	av_dict_set(&_formatOptions, key.c_str(), value.c_str(), flags);
}

StreamWriter::~StreamWriter()
{
}

void StreamWriter::setStreamInformation( relaybus::StreamInformation &streamInformation ) {
	_streamInformation = streamInformation;
	_streamNumber = streamInformation.streamNumber;

	AVCodecID codecID = translateCodec(streamInformation.codecType);

	if(codecID == CODEC_ID_NONE)
		throw std::runtime_error(MakeString() << "StreamWriter::setStreamInformation: unhandled codec type " << streamInformation.codecType);

	_codec = avcodec_find_decoder(codecID);

	_codecContext = avcodec_alloc_context3(_codec);

	if(_codec->capabilities & CODEC_CAP_TRUNCATED)
		_codecContext->flags|=CODEC_FLAG_TRUNCATED;

	//_codecContext->flags |= CODEC_FLAG_GLOBAL_HEADER;

	//if( streamInformation.codecType == CodecType::Codec_Video_H264)
	_codecContext->flags2 |= CODEC_FLAG2_CHUNKS;

	_codecContext->extradata = streamInformation.extraData.data();
	_codecContext->extradata_size = static_cast<int>(streamInformation.extraData.size());

	//int error = avcodec_open2(_codecContext, _codec, nullptr);
	//RELAYBUS_UNUSED(error);
}

void StreamWriter::open()
{
	_new_file = true;
	//open output file
	_outputFormat = av_guess_format( nullptr, _uri.c_str(), nullptr );
	_formatContext = avformat_alloc_context();
	_formatContext->oformat = _outputFormat;
	avio_open2( &_formatContext->pb, _uri.c_str(), AVIO_FLAG_WRITE, nullptr, &_formatOptions );

	 // Create output stream
	_stream = avformat_new_stream( _formatContext, nullptr );
	_stream->codec = _codecContext;

	_stream->sample_aspect_ratio.num = 0;
	_stream->sample_aspect_ratio.den = 0;

	// Assume r_frame_rate is accurate
	_stream->r_frame_rate = av_d2q(_streamInformation.framesPerSecond, 100000);
	_stream->avg_frame_rate = _stream->r_frame_rate;
	_stream->time_base = av_inv_q( _stream->r_frame_rate );
	_stream->codec->time_base = _stream->time_base;

	_stream->codec->width = 1920;
	_stream->codec->height = 1080;

	avformat_write_header( _formatContext, nullptr );
}

void StreamWriter::close()
{
	av_write_trailer( _formatContext );
	avio_close( _formatContext->pb );
	avformat_free_context( _formatContext );
	_formatContext = nullptr;
}

void StreamWriter::onData( relaybus::StreamPacket *streamPacket ) {
	if(streamPacket->streamNumber != _streamNumber)
		return;
	// Make sure we start on a key frame
	if (_new_file && ! ( streamPacket->flags & StreamPacketFlags::Flag_KeyFrame ) )
		return;
	_new_file = false;

	AVPacket packet;
	memset(&packet, 0, sizeof(AVPacket));
	packet.data = streamPacket->data.data();
	packet.size = static_cast<int>(streamPacket->data.size());

	packet.stream_index = 0; //ost->id; // todo
	packet.dts = packet.pts = AV_NOPTS_VALUE; // todo

	av_write_frame( _formatContext, &packet );
}

}}

#endif
