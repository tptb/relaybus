/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

// Don't include this file in any hpp file

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-pedantic"
extern "C" {
#include <libavcodec/avcodec.h>
}
#pragma GCC diagnostic pop

#if LIBAVUTIL_VERSION_MAJOR > 51
	#undef PixelFormat
#else
	typedef PixelFormat AVPixelFormat;
#endif

#if LIBAVUTIL_VERSION_MAJOR < 55
	typedef AVCodecID CodecID;
#endif

#include <relaybus/streaming.hpp>
#include <relaybus/image/Image.hpp>

namespace relaybus { namespace ffmpeg {

#define RELAYBUS_BUILD_FORMAT_HELPER(func) \
	func(YUV420P); \
	func(YUYV422); \
	func(RGB24); \
	func(BGR24); \
	func(YUV422P); \
	func(YUV444P); \
	func(YUV410P); \
	func(YUV411P); \
	func(GRAY8); \
	func(MONOWHITE); \
	func(MONOBLACK); \
	func(PAL8); \
	func(YUVJ420P); \
	func(YUVJ422P); \
	func(YUVJ444P); \
	func(BGRA); \
	func(RGBA); \
	func(ARGB); \
	func(ABGR);

inline relaybus::image::RPixelformat::Format translateFormat( int eFormat ) {
	switch( eFormat ) {
#define RELAYBUS_BUILD_FORMAT(a) case PIX_FMT_##a : return relaybus::image::RPixelformat::RPixelformat_##a
	RELAYBUS_BUILD_FORMAT_HELPER(RELAYBUS_BUILD_FORMAT)
#undef RELAYBUS_BUILD_FORMAT
	default:
		return relaybus::image::RPixelformat::RPixelformat_NONE;
	}
}

inline ::AVPixelFormat translateFormat( relaybus::image::RPixelformat::Format eFormat ) {
	switch( eFormat ) {
#define RELAYBUS_REV_BUILD_FORMAT(a) case relaybus::image::RPixelformat::RPixelformat_##a : return PIX_FMT_##a
	RELAYBUS_BUILD_FORMAT_HELPER(RELAYBUS_REV_BUILD_FORMAT)
#undef RELAYBUS_REV_BUILD_FORMAT
	default:
		return PIX_FMT_NONE;
	}
}

#undef RELAYBUS_BUILD_FORMAT_HELPER


#define RELAYBUS_BUILD_CODEC_HELPER(func) \
	func(H261); \
	func(H263); \
	func(RV10); \
	func(RV20); \
	func(MJPEG); \
	func(MJPEGB); \
	func(LJPEG); \
	func(SP5X); \
	func(JPEGLS); \
	func(MPEG4); \
	func(MSMPEG4V1); \
	func(MSMPEG4V2); \
	func(MSMPEG4V3); \
	func(WMV1); \
	func(WMV2); \
	func(H263P); \
	func(H263I); \
	func(FLV1); \
	func(SVQ1); \
	func(SVQ3); \
	func(HUFFYUV); \
	func(CYUV); \
	func(H264); \
	func(INDEO3); \
	func(VP3); \
	func(THEORA); \
	func(VP8);

inline relaybus::CodecType::CodecType translateCodec( int eCodec ) {
	switch( eCodec ) {
	case CODEC_ID_MPEG1VIDEO:
		return CodecType::Codec_Video_MPEG1;
	case CODEC_ID_MPEG2VIDEO:
		return CodecType::Codec_Video_MPEG2;
	case CODEC_ID_RAWVIDEO:
		return CodecType::Codec_Video_RAW;
	case CODEC_ID_DVVIDEO:
		return CodecType::Codec_Video_DV;

#define RELAYBUS_BUILD_CODEC_V(a) case CODEC_ID_##a : return CodecType::Codec_Video_##a
	RELAYBUS_BUILD_CODEC_HELPER(RELAYBUS_BUILD_CODEC_V)
#undef RELAYBUS_BUILD_CODEC_V
	default:
		return relaybus::CodecType::Codec_Unknown;
	}
}

inline AVCodecID translateCodec( relaybus::CodecType::CodecType eCodec ) {
	switch( eCodec ) {
	case CodecType::Codec_Video_MPEG1:
		return CODEC_ID_MPEG1VIDEO;
	case CodecType::Codec_Video_MPEG2:
		return CODEC_ID_MPEG2VIDEO;
	case CodecType::Codec_Video_RAW:
		return CODEC_ID_RAWVIDEO;
	case CodecType::Codec_Video_DV:
		return CODEC_ID_DVVIDEO;

#define RELAYBUS_REV_BUILD_CODEC_V(a) case CodecType::Codec_Video_##a : return CODEC_ID_##a
		RELAYBUS_BUILD_CODEC_HELPER(RELAYBUS_REV_BUILD_CODEC_V)
#undef RELAYBUS_REV_BUILD_CODEC_V
	default:
		return CODEC_ID_NONE;
	}
}

#undef RELAYBUS_BUILD_CODEC_HELPER

inline StreamType::StreamType translateStreamType( AVMediaType type ) {
	switch(type) {
	case AVMEDIA_TYPE_VIDEO:
		return StreamType::Stream_Video;
	case AVMEDIA_TYPE_AUDIO:
		return StreamType::Stream_Audio;
	case AVMEDIA_TYPE_DATA:
		return StreamType::Stream_Data;
	case AVMEDIA_TYPE_SUBTITLE:
		return StreamType::Stream_Subtitle;
	case AVMEDIA_TYPE_ATTACHMENT:
		return StreamType::Stream_Attachment;
	case AVMEDIA_TYPE_NB:
		return StreamType::Stream_NB;
	case AVMEDIA_TYPE_UNKNOWN:
#ifndef RELAYBUS_COMPILER_CLANG
	default:
#endif
		return StreamType::Stream_Unknown;
	}
}

inline AVMediaType translateStreamType( StreamType::StreamType type ) {
	switch(type) {
	case StreamType::Stream_Video:
		return AVMEDIA_TYPE_VIDEO;
	case StreamType::Stream_Audio:
		return AVMEDIA_TYPE_AUDIO;
	case StreamType::Stream_Data:
		return AVMEDIA_TYPE_DATA;
	case StreamType::Stream_Subtitle:
		return AVMEDIA_TYPE_SUBTITLE;
	case StreamType::Stream_Attachment:
		return AVMEDIA_TYPE_ATTACHMENT;
	case StreamType::Stream_NB:
		return AVMEDIA_TYPE_NB;
	case StreamType::Stream_Unknown:
	case StreamType::Stream_Count:
#ifndef RELAYBUS_COMPILER_CLANG
	default:
#endif
		return AVMEDIA_TYPE_UNKNOWN;
	}
}


inline image::image_stride_t convertStride(int stride[AV_NUM_DATA_POINTERS]) {
	image::image_stride_t r;

	for(image::image_stride_t::size_type i=0; i < r.size(); ++i) {
		r[i] = static_cast<image::image_size_type>(stride[i]);
	}

	return r;

}

inline void convertStride(int dst[AV_NUM_DATA_POINTERS], const image::image_stride_t &src) {
	for(image::image_stride_t::size_type i=0; i < src.size(); ++i) {
		dst[i] = static_cast<int>(src[i]);
	}
}

inline image::image_data_t convertData(uint8_t *data[AV_NUM_DATA_POINTERS]) {
	image::image_data_t r;

	for(image::image_data_t::size_type i=0; i < r.size(); ++i) {
		r[i] = data[i];
	}

	return r;
}

inline void convertData(uint8_t *dst[AV_NUM_DATA_POINTERS], const image::image_data_t &src) {
	for(image::image_data_t::size_type i=0; i < src.size(); ++i) {
		dst[i] = src[i];
	}
}

}}
