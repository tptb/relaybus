/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include <relaybus/image/Image.hpp>
#include <relaybus/Relay.hpp>

namespace relaybus { namespace image {

class CorrelationCoefficient : public relaybus::Relay {
	void init() override {
		publish("input1", _input1);
		publish("input2", _input2);
		publish("value", _value);
	}

	void deinit() override {
	}

	void process() override {

		//if(!(*_inputImg1 == *_inputImg2) || _inputImg1->_type != UFACore::Image::DATATYPE_8U)
		//	UFACORE_THROW(UFACore::exceptions::Exception, "*_inputImg1 != *_inputImg2 || !DATATYPE_8U");
#if 0
		// data
		u8 *data1 = reinterpret_cast<u8*>(_inputImg1->_data);
		u8 *data2 = reinterpret_cast<u8*>(_inputImg2->_data);

		// step
		int data1step = _inputImg1->_step;
		int data2step = _inputImg2->_step;

		int width = _inputImg1->_width;
		int height = _inputImg1->_height;

		int widthSize = width*_inputImg1->_channels;

		double sum = 0, sum1 = 0, sum2 = 0;

		for(int x=0;x<widthSize;++x) {
			for(int y=0;y<height;++y) {
				double data1v = data1[y*data1step+x];
				double data2v = data2[y*data2step+x];
				sum  += data1v*data2v;
				sum1 += data1v*data1v;
				sum2 += data2v*data2v;
			}
		}

		_value = sum/(sqrt(sum1)*sqrt(sum2));
#endif
	}

	Input<BaseImageView, DataProfile>	_input1;
	Input<BaseImageView, DataProfile>	_input2;
	Output<BaseImageView>	_value;
};

}}

RELAYBUS_RELAY(relaybus::image::CorrelationCoefficient, 1, "Correlation coefficient")
