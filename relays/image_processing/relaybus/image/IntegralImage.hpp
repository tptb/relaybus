/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#pragma once

#include <relaybus/image/Image.hpp>
#include <vector>
#include <cstdint>
#include <cmath>

namespace relaybus { namespace image {

template<typename T>
struct Vector {
	T x;
	T y;
	Vector() : x(static_cast<T>(0.0)), y(static_cast<T>(0.0)) {}
	Vector(T x_, T y_) : x(x_), y(y_) {}
};

typedef std::vector<Vector<double> > rvec;

struct IntegralImage {

	IntegralImage() : width(0), height(0) {}

	void calculate(const BaseImageView &image) {
		// reallocate data, if the size  has changed
		if(image.width() != width || image.height() != height) {
			width = image.width()+1;
			height = image.height()+1;
			integral_image.resize(width*height);
			sq_integral_image.resize(width*height);

			memset(integral_image.data(), 0, width*height*sizeof(int));
			memset(sq_integral_image.data(), 0, width*height*sizeof(double));
		}

		const image_size_type step = image.stride();

		uint8_t const *src = image.data();
		int *sum = integral_image.data() + width + 1;
		double *sqsum = sq_integral_image.data() + width + 1;
		for(image_size_type y = 0; y < height-1; y++, src += step,
						sum += width, sqsum += width )
		{
			int s =  0;
			double sq = 0;
			for(image_size_type x = 0; x < width-1; x++ ) {
				int it = ((int)src[x*3] + src[x*3+1] + src[x*3+2])/3;
				s += it;
				sq += it*it;
				int t = sum[x - width] + s;
				double tq = sqsum[x - width] + sq;
				sum[x] = t;
				sqsum[x] = tq;
			}
		}
	}

	double getAvg(const Vector<image_size_type> &left_up, const Vector<image_size_type> &right_down) {
		double n = (right_down.x - left_up.x) * (right_down.y - left_up.y);
		const int lu = integral_image[left_up.y*width + left_up.x];
		const int ld = integral_image[right_down.y*width + left_up.x];
		const int ru = integral_image[left_up.y*width + right_down.x];
		const int rd = integral_image[right_down.y*width + right_down.x];
		return (lu+rd-ld-ru) / n;
	}

	double getStdDev(const Vector<image_size_type> &left_up, const Vector<image_size_type> &right_down) {
		double n = (right_down.x - left_up.x) * (right_down.y - left_up.y);
		const double avg = getAvg(left_up, right_down);

		const double lu = sq_integral_image[left_up.y*width + left_up.x];
		const double ld = sq_integral_image[right_down.y*width + left_up.x];
		const double ru = sq_integral_image[left_up.y*width + right_down.x];
		const double rd = sq_integral_image[right_down.y*width + right_down.x];

		return  sqrt((lu+rd-ld-ru)/n - avg*avg);
	}

	std::vector<int>	integral_image;
	std::vector<double>	sq_integral_image;
	image_size_type					width;
	image_size_type					height;
};

}}

RELAYBUS_DECLARE_TYPE(relaybus::image::IntegralImage, 'r','i','i','i')
