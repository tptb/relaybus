/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include <relaybus/image/Image.hpp>
#include <relaybus/Relay.hpp>

#include <relaybus/Vector.hpp>

namespace relaybus { namespace image {

class Roi : public Relay {
public:
	Roi() {}
protected:
	void init() override {
		publish("input", _input);
		publish("output", _output);
		publish("roi", _roi);
	}

	void deinit() override {
	}

	void process() override {
		_input.update();

		if(_roi.tryUpdate())
			_roi.submit();

		if(_input != nullptr) {
			if(_roi == nullptr)
				_output.f() = _input.r();
			else {
				// Check if the roi is valid for the current image
				auto w = _roi.r().x().x() + _roi.r().y().x();
				auto h = _roi.r().x().y() + _roi.r().y().y();
				if(_input.r().width() >= w && _input.r().height() >= h) {
					_output.f() = _input.r().roi(_roi.r());
				} else {
					// Ignore the roi
					_output.f() = _input.r();
				}
			}
		} else
			_output.f() = nullptr;

		_input.submit();
		_output.submit();
	}

	Input<BaseImageView, DataProfile>	_input;
	Output<BaseImageView>	_output;

	Input<Rectangle_size_t, ParameterProfile> _roi;
};

}}
RELAYBUS_RELAY(relaybus::image::Roi, 1, "simple relay that outputs just the roi from the input image")

