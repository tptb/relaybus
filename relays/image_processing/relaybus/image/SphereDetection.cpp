/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include <relaybus/image/Image.hpp>
#include <relaybus/Relay.hpp>

#include "IntegralImage.hpp"
#include <vector>
#include <stdexcept>

namespace relaybus { namespace image {

class SphereDetection : public Relay {
public:
	SphereDetection() {}
protected:
	void init() override {
		//_input.set("input");
		//publish(_input, 1);

		publish("output", _output);
		publish("box_size", _box_size);
		publish("stddev_threshold", _stddev_threshold);
		publish("stddev2_threshold", _stddev2_threshold);
	}

	void deinit() override {
	}

	void process() override {
#if 0
		rvec detect(const cv::Mat &image, const cv::Mat &mask)
			rvec result;

			if(image.size().width != mask.size().width ||
				image.size().height != mask.size().height)
				throw std::runtime_error("image size doesn't match mask size");

			integral_image.calculate(image);

			int width = image.size().width;
			int height = image.size().height;
			if(_output.size() != image.size())
				_output = cv::Mat::zeros(height, width, CV_8U);
			int step = _output.step;

			// apply background image
			uchar *dst = _output.data + step*boxsize;
			for(int y=boxsize;y<height-boxsize;++y, dst += step) {
				for(int x=boxsize;x<width-boxsize;++x) {
					double stddev = integral_image.getStdDev(Vector<int>(x-boxsize, y-boxsize), Vector<int>(x+boxsize, y+boxsize));
					int hb = boxsize *2;
					double stddev2 = integral_image.getStdDev(Vector<int>(x-hb, y-hb), Vector<int>(x+hb, y+hb));
					if(stddev > stddev_threshold && stddev2 < stddev2_threshold)
						dst[x] = 0;
					else
						dst[x] = 255;
				}
			}

			imshow("result", _output);

			return result;
#endif
	}

	//Input<IntegralImage>	_input;
	Output<BaseImageView>	_output;

	Input<int, ParameterProfile> _box_size; // 8
	Input<int, ParameterProfile> _stddev_threshold; // 23
	Input<int, ParameterProfile> _stddev2_threshold; // 23
};

}}
RELAYBUS_RELAY(relaybus::image::SphereDetection, 1, "sphere detection using integral images")

