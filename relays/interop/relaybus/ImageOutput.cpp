/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include "ImageOutput.hpp"

RELAYBUS_RELAY(relaybus::ImageOutput, 1, "ImageOutput ...")

using namespace relaybus;

void ImageOutput::init() {
	_image = nullptr;
	_buffer.setMaxSize(2); // TODO: allow changing using a Pin
	_buffer.setEnableDrop(false);
	publish("image", _image);
}

void ImageOutput::deinit() {
	_buffer.clear();
}

void ImageOutput::process() {
	_image.updateAndSubmit();
	_buffer.push(_image.current());
}


bool ImageOutput::hasNewImage() const {
	return _buffer.hasBufferedElements();
}

std::size_t ImageOutput::pendingUpdateCount() const {
	return _buffer.bufferedElementCount();
}

ZeroCopy<image::BaseImageView> ImageOutput::getNewImage() {
	return _buffer.pop().data();
}
