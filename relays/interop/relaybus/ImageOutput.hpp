/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#pragma once

#include <relaybus/image/BaseImage.hpp>
#include <relaybus/Relay.hpp>
#include <relaybus/Buffer.hpp>
#include "interop_global.hpp"

namespace relaybus {

class RELAYBUS_INTEROP_API ImageOutput : public Relay {
	struct PrivateProfile {
	EXTENSION_SYSTEM_CONSTEXPR inline static WireProfile value() {
		return WireProfile(WireType::Blocking, 4);
	}
};
public:
	bool hasNewImage() const;
	std::size_t pendingUpdateCount() const;
	ZeroCopy<image::BaseImageView> getNewImage();

	template<typename duration_type>
	Optional<ZeroCopy<image::BaseImageView>> getNewImage(duration_type const& wait_duration) {
		return _buffer.pop(wait_duration);
	}

private:
	void init() override;
	void deinit() override;
	void process() override;

	Input<image::BaseImageView, PrivateProfile>	_image;
	Buffer<image::BaseImageView>				_buffer;
};

}
