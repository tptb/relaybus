# Copyright Bernd Amend 2014
# Distributed under the Boost Software License, Version 1.0.
# (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)
cmake_minimum_required (VERSION 2.8)
project(Live555)

remove_definitions(-std=c++11)

if(${CMAKE_SYSTEM_NAME} STREQUAL "AIX")
	add_definitions(-DBSD=1 -DAIX=1)
	add_definitions(-DSOCKLEN_T=socklen_t -DTIME_BASE=int)
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
	add_definitions(-DBSD=1)
	add_definitions(-DSOCKLEN_T=socklen_t -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64)
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
	add_definitions(-Wall -Wno-int-to-pointer-cast -Wno-conversion-null -Wno-write-strings
					-Wno-parentheses -Wno-overloaded-virtual -Wno-pedantic -Wno-maybe-uninitialized
					-Wno-unused-but-set-variable)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
	add_definitions(-Wall -Wno-int-to-void-pointer-cast -Wno-int-to-pointer-cast
					-Wno-array-bounds -Wno-unused-private-field)
elseif(MSVC)
# TODO
endif()

if(NOT MSVC)
	if( CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64" )
		add_definitions(-fPIC)
	endif()
endif()

option(ALLOW_RTSP_SERVER_PORT_REUSE "" ON)
if(ALLOW_RTSP_SERVER_PORT_REUSE)
	add_definitions(-DALLOW_RTSP_SERVER_PORT_REUSE)
endif()

set(Live555_Library_List liveMedia groupsock UsageEnvironment BasicUsageEnvironment)

foreach(library ${Live555_Library_List})
	file(GLOB ${library}_files
		${CMAKE_CURRENT_SOURCE_DIR}/${library}/*.c*
		${CMAKE_CURRENT_SOURCE_DIR}/${library}/include/*.h*
	)

	add_library(${library} STATIC ${${library}_files})

#	install(TARGETS ${library}
#		ARCHIVE DESTINATION lib
#		LIBRARY DESTINATION lib
#		RUNTIME DESTINATION bin
#		)
endforeach()

set(PROJECT_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/liveMedia/include/
						 ${CMAKE_CURRENT_SOURCE_DIR}/UsageEnvironment/include/
						 ${CMAKE_CURRENT_SOURCE_DIR}/groupsock/include/
						 ${CMAKE_CURRENT_SOURCE_DIR}/BasicUsageEnvironment/include/
						 ${CMAKE_CURRENT_SOURCE_DIR}/mediaServer/
)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/liveMedia/include
					 ${CMAKE_CURRENT_SOURCE_DIR}/UsageEnvironment/include
					 ${CMAKE_CURRENT_SOURCE_DIR}/groupsock/include
					 ${CMAKE_CURRENT_SOURCE_DIR}/BasicUsageEnvironment/include)
