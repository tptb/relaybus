#if 0
/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <relaybus/streaming.hpp>

#include <liveMedia/liveMedia.hh>
#include <BasicUsageEnvironment/BasicUsageEnvironment.hh>

// provides the function parseSPropParameterSets
#include <liveMedia/H264VideoRTPSource.hh>

#include <liveMedia/MediaSink.hh>
#include <liveMedia/MediaSession.hh>
typedef Boolean LIVE_MEDIA_Boolean;
#undef Boolean

#include <cstdint>
#include <stdexcept>
#include <string>
#include <vector>
#include <relaybus/MakeString.hpp>

#include <relaybus/Relay.hpp>

class TaskScheduler;
class UsageEnvironment;
class RTSPClient;
class MediaSession;

namespace relaybus {

class LiveMediaRtspSourceInterface {
public:
	~LiveMediaRtspSourceInterface() {}
	virtual void onData() = 0;
	virtual bool process_internal() = 0;

	virtual void setDataReceived() = 0;

	Output<StreamPacket>					_stream_packet;
};

class LiveTaskScheduler : public BasicTaskScheduler {
public:
	LiveTaskScheduler(LiveMediaRtspSourceInterface &lminterface)
		: BasicTaskScheduler(1000), _lminterface(lminterface)
	{}

	void doEventLoop(char* watchVariable) {
		// Repeatedly loop, handling readable sockets and timed events
		while (_lminterface.process_internal()) {
			if (watchVariable != nullptr && *watchVariable != 0)
				break;
			SingleStep(0);
		}
	}
private:
	LiveMediaRtspSourceInterface &_lminterface;
};

class VideoSink: public MediaSink {
public:
	VideoSink(UsageEnvironment& env, MediaSubsession& subsession, LiveMediaRtspSourceInterface &lminterface, const std::string &streamId)
		: MediaSink(env), _subsession(subsession), _lminterface(lminterface), _streamId(streamId)
	{
	}
private:

	static void afterGettingFrame(void* clientData, unsigned frameSize, unsigned numTruncatedBytes,
								  struct timeval presentationTime, unsigned durationInMicroseconds) {
		VideoSink* sink = reinterpret_cast<VideoSink*>(clientData);
		sink->afterGettingFrame(frameSize, numTruncatedBytes, presentationTime, durationInMicroseconds);
	}

	void afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
							struct timeval presentationTime, unsigned durationInMicroseconds) {

		_lminterface.setDataReceived();

		EXTENSION_SYSTEM_UNUSED(numTruncatedBytes);
		EXTENSION_SYSTEM_UNUSED(durationInMicroseconds);
		if(frameSize<4)
			return;

		auto &streamPacket = _lminterface._stream_packet.rw();

		std::size_t start_pos = 0;
		frameSize += 3;

		uint8_t *pBuffer = streamPacket.data.data();

		// some streams like mp4 already have the 00 00 01 sequence
		if(pBuffer[3] == 0 && pBuffer[4] == 0 && pBuffer[5] == 1) {
			start_pos = 3;
			frameSize -= 3;
		}

		// fSubsession.getNormalPlayTime(presentationTime)
		streamPacket.timeStamp = static_cast<uint64_t>(presentationTime.tv_sec*1000000) +  presentationTime.tv_usec;
		streamPacket.streamNumber = 0; // todo
		streamPacket.flags = StreamPacketFlags::Flag_Unknown; // the decoder has to guess
		streamPacket.data.setDataRegion(start_pos, frameSize + 3);

		_lminterface._stream_packet.submit();

		// Then continue, to request the next frame of data:
		continuePlaying();
	}

	virtual LIVE_MEDIA_Boolean continuePlaying() {
		if (fSource == nullptr)  // sanity check (should not happen)
			return False;

		auto &streamPacket = _lminterface._stream_packet.c();
		streamPacket.data = SharedArray<uint8_t>(1024*1024 + 3);

		// dummy sink receive buffer size
		// TODO use only one int store
		streamPacket.data[0] = 0;
		streamPacket.data[1] = 0;
		streamPacket.data[2] = 1;

		// Request the next frame of data from our input source.  "afterGettingFrame()" will get called later, when it arrives:
		fSource->getNextFrame(streamPacket.data.data()+3, streamPacket.data.size()-3,
								&VideoSink::afterGettingFrame, this,
								&MediaSink::onSourceClosure, this);
		return True;
	}

	MediaSubsession& _subsession;
	LiveMediaRtspSourceInterface &_lminterface;
	const std::string _streamId;
};

class LiveMediaRtspSource : public Relay, public LiveMediaRtspSourceInterface {
public:
	LiveMediaRtspSource()
		: _taskScheduler(nullptr), _rtspClient(nullptr), _session(nullptr) {}

	void init() override {
		_retry_connect_after_ms.f() = 500;
		publish("uri",				_uri);
		publish("retry_connect_after_ms",		_retry_connect_after_ms);
		publish("stream/packet",	_stream_packet);
		publish("stream/information/0", _stream_information);
		publish("stream/information/vec", _stream_informations);
	}

	void deinit() override {
	}

	// currently blocking, this may change later
	void process() override {

		if(_uri.tryUpdate())
			_uri.submit();

		if(!_uri.isValid())
			return;

		bool stream_information_was_invalid = (_stream_information == nullptr) || (_stream_informations == nullptr);
		_stream_information = nullptr;
		_stream_informations = nullptr;
		_stream_packet = nullptr;

		setDataReceived();

		_taskScheduler = new LiveTaskScheduler(*this);
		_usageEnvironment = nullptr;
		_usageEnvironment = BasicUsageEnvironment::createNew(*_taskScheduler);

		// VERIFY DOWN

		_rtspClient = RTSPClient::createNew(*_usageEnvironment, 0);
		char* sdp = _rtspClient->describeURL(_uri.r().c_str());
		_session = MediaSession::createNew(*_usageEnvironment, sdp);

		setDataReceived();

		if(_session == nullptr) {
			if(!stream_information_was_invalid) {
				_stream_information.submit();
				_stream_informations.submit();
				_stream_packet.submit();
			}
			if(_retry_connect_after_ms.tryUpdate())
				_retry_connect_after_ms.submit();
			if(_retry_connect_after_ms.isValid())
				this_thread::sleep_for(chrono::milliseconds(_retry_connect_after_ms.r()));
			//reportInformation("Couldn't open stream");
		} else {
			{
				MediaSubsessionIterator itor(*_session);
				MediaSubsession* subsession = itor.next();
				while(subsession != nullptr) {
					if(subsession->initiate(0)) {
						_rtspClient->setupMediaSubsession(*subsession, false, false, false);
						const std::string codecName = subsession->codecName();
						if(codecName == "META") {
						} else {

							StreamInformation streamInfo;
							uint8_t *data = new uint8_t[4092];
							const char *parameterString = subsession->fmtp_spropparametersets();

							if(parameterString != nullptr) {
								unsigned numSPropRecords;
								SPropRecord* sPropRecords = parseSPropParameterSets(parameterString, numSPropRecords);
								unsigned cntr = 0;
								for (unsigned i = 0; i < numSPropRecords; ++i) {
									data[cntr+0] = 0;
									data[cntr+1] = 0;
									data[cntr+2] = 1;
									cntr += 3;

									memcpy( &data[cntr], sPropRecords[i].sPropBytes, sPropRecords[i].sPropLength );
									cntr += sPropRecords[i].sPropLength;
								}
								delete[] sPropRecords;

								streamInfo.extraData = 	SharedArray<uint8_t>(data, cntr, &SharedArray<uint8_t>::arrayDelete);
							}

							bool skip_stream = false;
							std::string mediumName = subsession->mediumName();
							if(mediumName == "video") {
								streamInfo.streamType = StreamType::Stream_Video;
							} else if (mediumName == "audio") {
								skip_stream = true; // skip audio stream
							} else {
								reportWarning(MakeString() << "unknown stream type " << mediumName);
								skip_stream = true;
							}

							if(!skip_stream) {
								if(codecName == "JPEG") {
									streamInfo.codecType = CodecType::Codec_Video_MJPEG;
								} else if(codecName == "H264") {
									streamInfo.codecType = CodecType::Codec_Video_H264;
								} else if(codecName == "MPEG4" || codecName == "MP4V-ES" || codecName == "MPEG4-GENERIC")	{
									streamInfo.codecType = CodecType::Codec_Video_MPEG4;
								} else if(codecName == "MPV") {
									streamInfo.codecType = CodecType::Codec_Video_MPEG2;
								} else if(codecName == "MPA") { // audio
									// currently ignored
								} else if(codecName == "AC3") { // audio
									// currently ignored
								} else {
									reportWarning(MakeString() << "unknown codec type " << codecName);
								}

								streamInfo.streamNumber = 0; // todo
								streamInfo.framesPerSecond = 30; // todo

								_stream_informations.rw().push_back(streamInfo);

								VideoSink *videoSink = new VideoSink(*_usageEnvironment, *subsession, *this);

								subsession->sink = videoSink;

								RTPSource* source = subsession->rtpSource();
								// timing critical !!!
								//source->setPacketReorderingThresholdTime(unsigned int(1000000));
								setDataReceived();
								videoSink->startPlaying(*source, nullptr, nullptr);
							}
						}
					}
					subsession = itor.next();
				}
			}

			setDataReceived();

			_stream_information.rw() = _stream_informations.r()[0];

			_stream_information.submit();
			_stream_informations.submit();

			_rtspClient->playMediaSession(*_session);
			setDataReceived();

			// VERIFY up

			openURL(*_usageEnvironment, _uri.r().c_str());
			setDataReceived();

			_usageEnvironment->taskScheduler().doEventLoop();

			// VERIFY down

			{
				MediaSubsessionIterator itor(*_session);
				auto subsession = itor.next();
				while(subsession != nullptr) {
					if (subsession->sink) {
						subsession->sink->stopPlaying();
						MediaSink::close(subsession->sink);
						// do we have to delete the subsession->sink instance?
					}
					subsession = itor.next();
				}
			}
			_rtspClient->teardownMediaSession(*_session);
			MediaSession::close(_session);
		}

		RTSPClient::close(_rtspClient);

		_usageEnvironment->reclaim();
		_usageEnvironment = nullptr;

		_taskScheduler.reset();
	}

	bool process_internal() override {
		if(_uri.hasPendingUpdates())
			return false;

		// check if the stream is still alive
		auto dur = clock::now() - _last_data_received;
		if(dur > chrono::milliseconds(2000))
			return false;

		return !isExitRequested();
	}

	void onData() override {
		_stream_packet.submit();
	}

	void setDataReceived() override {
		_last_data_received = clock::now();
	}

protected:
	std::unique_ptr<LiveTaskScheduler> _taskScheduler;
	UsageEnvironment *_usageEnvironment;

	RTSPClient *_rtspClient;
	MediaSession *_session;

	time_point				_last_data_received;

	Input<std::string, ParameterProfile>	_uri;
	Input<int, ParameterProfile>	_retry_connect_after_ms;
	Output<StreamInformation>				_stream_information;
	Output<std::vector<StreamInformation> >	_stream_informations;
};

}

RELAYBUS_RELAY(relaybus::LiveMediaRtspSource, 2, "live media rtsp source")

#if 0
// Forward function definitions:

// RTSP 'response handlers':
void continueAfterDESCRIBE(RTSPClient* rtspClient, int resultCode, char* resultString);
void continueAfterSETUP(RTSPClient* rtspClient, int resultCode, char* resultString);
void continueAfterPLAY(RTSPClient* rtspClient, int resultCode, char* resultString);

// Other event handler functions:
void subsessionAfterPlaying(void* clientData); // called when a stream's subsession (e.g., audio or video substream) ends
void subsessionByeHandler(void* clientData); // called when a RTCP "BYE" is received for a subsession
void streamTimerHandler(void* clientData);
  // called at the end of a stream's expected duration (if the stream has not already signaled its end using a RTCP "BYE")

// The main streaming routine (for each "rtsp://" URL):
void openURL(UsageEnvironment& env, char const* progName, char const* rtspURL);

// Used to iterate through each stream's 'subsessions', setting up each one:
void setupNextSubsession(RTSPClient* rtspClient);

// Used to shut down and close a stream (including its "RTSPClient" object):
void shutdownStream(RTSPClient* rtspClient, int exitCode = 1);

// Define a class to hold per-stream state that we maintain throughout each stream's lifetime:

class StreamClientState {
public:
	StreamClientState()
	: iter(NULL), session(NULL), subsession(NULL), streamTimerTask(NULL), duration(0.0) {}

	virtual StreamClientState() {
		delete iter;
		if (session != NULL) {
			// We also need to delete "session", and unschedule "streamTimerTask" (if set)
			UsageEnvironment& env = session->envir(); // alias

			env.taskScheduler().unscheduleDelayedTask(streamTimerTask);
			Medium::close(session);
		}
	}

public:
	MediaSubsessionIterator* iter;
	MediaSession* session;
	MediaSubsession* subsession;
	TaskToken streamTimerTask;
	double duration;
};

// If you're streaming just a single stream (i.e., just from a single URL, once), then you can define and use just a single
// "StreamClientState" structure, as a global variable in your application.  However, because - in this demo application - we're
// showing how to play multiple streams, concurrently, we can't do that.  Instead, we have to have a separate "StreamClientState"
// structure for each "RTSPClient".  To do this, we subclass "RTSPClient", and add a "StreamClientState" field to the subclass:

class ourRTSPClient: public RTSPClient {
public:
  static ourRTSPClient* createNew(UsageEnvironment& env, char const* rtspURL,
				  int verbosityLevel = 0,
				  char const* applicationName = NULL,
				  portNumBits tunnelOverHTTPPortNum = 0) {
		return new ourRTSPClient(env, rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum);
	}

protected:
	ourRTSPClient(UsageEnvironment& env, char const* rtspURL,
				 int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum)
	: RTSPClient(env,rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum, -1) {
	}

public:
	StreamClientState scs;
};

void openURL(UsageEnvironment& env, char const* progName, char const* rtspURL) {
	// Begin by creating a "RTSPClient" object.  Note that there is a separate "RTSPClient" object for each stream that we wish
	// to receive (even if more than stream uses the same "rtsp://" URL).
	RTSPClient* rtspClient = ourRTSPClient::createNew(env, rtspURL, 0, progName);
	if (rtspClient == nullptr) {
		reportError(MakeString() << "Failed to create a RTSP client for URL \"" << rtspURL << "\": " << env.getResultMsg());
		return;
	}

	// Next, send a RTSP "DESCRIBE" command, to get a SDP description for the stream.
	// Note that this command - like all RTSP commands - is sent asynchronously; we do not block, waiting for a response.
	// Instead, the following function call returns immediately, and we handle the RTSP response later, from within the event loop:
	rtspClient->sendDescribeCommand(continueAfterDESCRIBE);
}


// Implementation of the RTSP 'response handlers':

void continueAfterDESCRIBE(RTSPClient* rtspClient, int resultCode, char* resultString) {
	do {
		UsageEnvironment& env = rtspClient->envir(); // alias
		StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

		if (resultCode != 0) {
			env << *rtspClient << "Failed to get a SDP description: " << resultString << "\n";
			delete[] resultString;
			break;
		}

		char* const sdpDescription = resultString;
		env << *rtspClient << "Got a SDP description:\n" << sdpDescription << "\n";

		// Create a media session object from this SDP description:
		scs.session = MediaSession::createNew(env, sdpDescription);
		delete[] sdpDescription; // because we don't need it anymore
		if (scs.session == NULL) {
			env << *rtspClient << "Failed to create a MediaSession object from the SDP description: " << env.getResultMsg() << "\n";
			break;
		} else if (!scs.session->hasSubsessions()) {
			env << *rtspClient << "This session has no media subsessions (i.e., no \"m=\" lines)\n";
			break;
		}

		// Then, create and set up our data source objects for the session.  We do this by iterating over the session's 'subsessions',
		// calling "MediaSubsession::initiate()", and then sending a RTSP "SETUP" command, on each one.
		// (Each 'subsession' will have its own data source.)
		scs.iter = new MediaSubsessionIterator(*scs.session);
		setupNextSubsession(rtspClient);
		return;
	} while (0);

	// An unrecoverable error occurred with this stream.
	shutdownStream(rtspClient);
}

void setupNextSubsession(RTSPClient* rtspClient) {
	UsageEnvironment& env = rtspClient->envir(); // alias
	StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

	scs.subsession = scs.iter->next();
	if (scs.subsession != NULL) {
		if (!scs.subsession->initiate()) {
			env << *rtspClient << "Failed to initiate the \"" << *scs.subsession << "\" subsession: " << env.getResultMsg() << "\n";
			setupNextSubsession(rtspClient); // give up on this subsession; go to the next one
		} else {
			env << *rtspClient << "Initiated the \"" << *scs.subsession
			<< "\" subsession (client ports " << scs.subsession->clientPortNum() << "-" << scs.subsession->clientPortNum()+1 << ")\n";

			// Continue setting up this subsession, by sending a RTSP "SETUP" command:
			rtspClient->sendSetupCommand(*scs.subsession, continueAfterSETUP, False, False);
		}
		return;
	}

	// We've finished setting up all of the subsessions.  Now, send a RTSP "PLAY" command to start the streaming:
	if (scs.session->absStartTime() != NULL) {
		// Special case: The stream is indexed by 'absolute' time, so send an appropriate "PLAY" command:
		rtspClient->sendPlayCommand(*scs.session, continueAfterPLAY, scs.session->absStartTime(), scs.session->absEndTime());
	} else {
		scs.duration = scs.session->playEndTime() - scs.session->playStartTime();
		rtspClient->sendPlayCommand(*scs.session, continueAfterPLAY);
	}
}

void continueAfterSETUP(RTSPClient* rtspClient, int resultCode, char* resultString) {
	do {
		UsageEnvironment& env = rtspClient->envir(); // alias
		StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

		if (resultCode != 0) {
			env << *rtspClient << "Failed to set up the \"" << *scs.subsession << "\" subsession: " << resultString << "\n";
			break;
		}

		env << *rtspClient << "Set up the \"" << *scs.subsession
		<< "\" subsession (client ports " << scs.subsession->clientPortNum() << "-" << scs.subsession->clientPortNum()+1 << ")\n";

		// Having successfully setup the subsession, create a data sink for it, and call "startPlaying()" on it.
		// (This will prepare the data sink to receive data; the actual flow of data from the client won't start happening until later,
		// after we've sent a RTSP "PLAY" command.)

		scs.subsession->sink = VideoSink::createNew(env, *scs.subsession, rtspClient->url());
		// perhaps use your own custom "MediaSink" subclass instead
		if (scs.subsession->sink == NULL) {
			env << *rtspClient << "Failed to create a data sink for the \"" << *scs.subsession
			<< "\" subsession: " << env.getResultMsg() << "\n";
			break;
		}

		env << *rtspClient << "Created a data sink for the \"" << *scs.subsession << "\" subsession\n";
		scs.subsession->miscPtr = rtspClient; // a hack to let subsession handle functions get the "RTSPClient" from the subsession
		scs.subsession->sink->startPlaying(*(scs.subsession->readSource()),
						subsessionAfterPlaying, scs.subsession);
		// Also set a handler to be called if a RTCP "BYE" arrives for this subsession:
		if (scs.subsession->rtcpInstance() != NULL) {
			scs.subsession->rtcpInstance()->setByeHandler(subsessionByeHandler, scs.subsession);
		}
	} while (0);
	delete[] resultString;

	// Set up the next subsession, if any:
	setupNextSubsession(rtspClient);
}

void continueAfterPLAY(RTSPClient* rtspClient, int resultCode, char* resultString) {
	Boolean success = False;

	do {
		UsageEnvironment& env = rtspClient->envir(); // alias
		StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

		if (resultCode != 0) {
			env << *rtspClient << "Failed to start playing session: " << resultString << "\n";
			break;
		}

		// Set a timer to be handled at the end of the stream's expected duration (if the stream does not already signal its end
		// using a RTCP "BYE").  This is optional.  If, instead, you want to keep the stream active - e.g., so you can later
		// 'seek' back within it and do another RTSP "PLAY" - then you can omit this code.
		// (Alternatively, if you don't want to receive the entire stream, you could set this timer for some shorter value.)
		if (scs.duration > 0) {
			unsigned const delaySlop = 2; // number of seconds extra to delay, after the stream's expected duration.  (This is optional.)
			scs.duration += delaySlop;
			unsigned uSecsToDelay = (unsigned)(scs.duration*1000000);
			scs.streamTimerTask = env.taskScheduler().scheduleDelayedTask(uSecsToDelay, (TaskFunc*)streamTimerHandler, rtspClient);
		}

		env << *rtspClient << "Started playing session";
		if (scs.duration > 0) {
			env << " (for up to " << scs.duration << " seconds)";
		}
		env << "...\n";

		success = True;
	} while (0);
	delete[] resultString;

	if (!success) {
		// An unrecoverable error occurred with this stream.
		shutdownStream(rtspClient);
	}
}


// Implementation of the other event handlers:

void subsessionAfterPlaying(void* clientData) {
	MediaSubsession* subsession = (MediaSubsession*)clientData;
	RTSPClient* rtspClient = (RTSPClient*)(subsession->miscPtr);

	// Begin by closing this subsession's stream:
	Medium::close(subsession->sink);
	subsession->sink = NULL;

	// Next, check whether *all* subsessions' streams have now been closed:
	MediaSession& session = subsession->parentSession();
	MediaSubsessionIterator iter(session);
	while ((subsession = iter.next()) != NULL) {
		if (subsession->sink != NULL) return; // this subsession is still active
	}

	// All subsessions' streams have now been closed, so shutdown the client:
	shutdownStream(rtspClient);
}

void subsessionByeHandler(void* clientData) {
	MediaSubsession* subsession = (MediaSubsession*)clientData;
	RTSPClient* rtspClient = (RTSPClient*)subsession->miscPtr;
	UsageEnvironment& env = rtspClient->envir(); // alias

	env << *rtspClient << "Received RTCP \"BYE\" on \"" << *subsession << "\" subsession\n";

	// Now act as if the subsession had closed:
	subsessionAfterPlaying(subsession);
}

void streamTimerHandler(void* clientData) {
	ourRTSPClient* rtspClient = (ourRTSPClient*)clientData;
	StreamClientState& scs = rtspClient->scs; // alias

	scs.streamTimerTask = NULL;

	// Shut down the stream:
	shutdownStream(rtspClient);
}

void shutdownStream(RTSPClient* rtspClient, int exitCode) {
	UsageEnvironment& env = rtspClient->envir(); // alias
	StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

	// First, check whether any subsessions have still to be closed:
	if (scs.session != NULL) {
		Boolean someSubsessionsWereActive = False;
		MediaSubsessionIterator iter(*scs.session);
		MediaSubsession* subsession;

		while ((subsession = iter.next()) != NULL) {
			if (subsession->sink != NULL) {
				Medium::close(subsession->sink);
				subsession->sink = NULL;

				if (subsession->rtcpInstance() != NULL) {
					subsession->rtcpInstance()->setByeHandler(NULL, NULL); // in case the server sends a RTCP "BYE" while handling "TEARDOWN"
				}

				someSubsessionsWereActive = True;
			}
		}

		if (someSubsessionsWereActive) {
			// Send a RTSP "TEARDOWN" command, to tell the server to shutdown the stream.
			// Don't bother handling the response to the "TEARDOWN".
			rtspClient->sendTeardownCommand(*scs.session, NULL);
		}
	}

	env << *rtspClient << "Closing the stream.\n";
	Medium::close(rtspClient);
	// Note that this will also cause this stream's "StreamClientState" structure to get reclaimed.

	// The final stream has ended, so exit the application now.
	// (Of course, if you're embedding this code into your own application, you might want to comment this out,
	// and replace it with "eventLoopWatchVariable = 1;", so that we leave the LIVE555 event loop, and continue running "main()".)
	// TODO: kill the stream? I have no idea
}

#endif
#endif
