/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <cmath>

#include <relaybus/image/Image.hpp>
#include <relaybus/Relay.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-pedantic"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videostab/videostab.hpp>
#include <opencv2/videostab/optical_flow.hpp>
#pragma GCC diagnostic pop

using namespace relaybus::image;

namespace relaybus { namespace opencv {

struct VideoSource : public cv::videostab::IFrameSource {
public:
	VideoSource() { reset(); }
	virtual void reset() {}
	virtual cv::Mat nextFrame() { return _frame; }

	void setFrame(const cv::Mat &frame) {
		_frame = frame.clone();
	}
private:
	cv::Mat _frame;
};

struct WindowMotionEstimator : public cv::videostab::IGlobalMotionEstimator
{
	WindowMotionEstimator() {
		_motion_estimator.reset(new cv::videostab::PyrLkRobustMotionEstimator());
		_motion_estimator->setMotionModel(cv::videostab::TRANSLATION);
	}

	cv::Mat estimate(const cv::Mat &frame0, const cv::Mat &frame1) {
		const int width = 200;
		const int height = 200;
		int x = (frame0.size().width-width)/2;
		int y = (frame0.size().height-height)/2;
		cv::Mat estimation = _motion_estimator->estimate(cv::Mat(frame0, cv::Rect(x, y, width, height)), cv::Mat(frame1, cv::Rect(x, y, width, height)));
		return estimation;
	}
	std::shared_ptr<cv::videostab::PyrLkRobustMotionEstimator> _motion_estimator;
};

class ImageStabilization : public Relay {

	void init() override {

		_enable.f() = false;
		_radius.f() = 10;

		publish("input", _input);
		publish("enable", _enable);
		publish("output", _output);
		publish("radius", _radius);

		using namespace cv::videostab;

		_stabilizer.reset(new OnePassStabilizer());

		//_motion_filter = std::make_shared<GaussianMotionFilter>();
		if (_motion_filter.get()) {
			_stabilizer->setMotionFilter(_motion_filter.get());
		}

		_frameSource.reset(new VideoSource());
		_stabilizer->setFrameSource(_frameSource.get());
		_stabilizer->setBorderMode(cv::BORDER_REPLICATE);

		_motion_estimator.reset(new WindowMotionEstimator());
		_stabilizer->setMotionEstimator(_motion_estimator.get());
		_stabilizer->setRadius(10);

		//_deblurer.reset(new WeightingDeblurer());
		if(_deblurer.get()) {
			_deblurer->setSensitivity(0.5);
			_stabilizer->setDeblurer(_deblurer.get());
		}
	}

	void deinit() override {}

	void process() override {
		waitForPendingUpdates();

		_enable.tryUpdateAndSubmit();

		if(_radius.tryUpdate()) {
			if(_radius.r() <= 0) {
				//_radius.rejectLastUpdate(); // TODO
			} else {
				_stabilizer->setRadius(_radius.r());
				_radius.submit();
			}
		}

		if(_input.tryUpdate()) {

			if(_input.isValid()) {
				if(_enable.isValid() && _enable.r()) {
					try {
						switch(_input.r().format()) {
							case RPixelformat::RPixelformat_RGB24:
							_frameSource->setFrame(cv::Mat(static_cast<int>(_input.r().height()),
													static_cast<int>(_input.r().width()),
													CV_8UC3,  // TODO: convert the value
													_input.rw().data(), _input.r().stride()));
							_stabilizedFrame = _stabilizer->nextFrame();

							_output.w() = BaseImage(_stabilizedFrame.cols,
															  _stabilizedFrame.rows,
															  _input.r().format(),
															  _stabilizedFrame.step1(),
															  _stabilizedFrame.data,
															  nullptr
											).cloneData();
							break;
							case RPixelformat::RPixelformat_GRAY8:
							_frameSource->setFrame(cv::Mat(static_cast<int>(_input.r().height()),
													static_cast<int>(_input.r().width()),
													CV_8UC1,  // TODO: convert the value
													_input.rw().data(), _input.r().stride()));
							_stabilizedFrame = _stabilizer->nextFrame();

							_output.w() = BaseImage(_stabilizedFrame.cols,
															  _stabilizedFrame.rows,
															  RPixelformat::RPixelformat_GRAY8,
															  _stabilizedFrame.step1(),
															  _stabilizedFrame.data,
															  nullptr
											).cloneData();
							break;
							case RPixelformat::RPixelformat_DEPTH_16ULE:
							break;
							case RPixelformat::RPixelformat_YUVJ420P:
							case RPixelformat::RPixelformat_YUV420P:
							{
								// HACK
								_frameSource->setFrame(cv::Mat(static_cast<int>(_input.r().height()),
														static_cast<int>(_input.r().width()),
														CV_8UC1,  // TODO: convert the value
														_input.rw().data(), _input.r().stride()));
								_stabilizedFrame = _stabilizer->nextFrame();

								//using image_stride_t = std::array<image_size_type, image_dimensions>;
								image_stride_t stride;
								image_data_t data;

								stride[0] = _stabilizedFrame.step1();
								data[0] = _stabilizedFrame.data;
								for(int i=1;i<3;++i) {
									stride[i] = _input.r().stride(i);
									data[i] = const_cast<uint8_t*>(_input.r().data(i));
								}


								BaseImage outimg(_stabilizedFrame.cols,
														  _stabilizedFrame.rows,
														  RPixelformat::RPixelformat_YUVJ420P,
														  stride,
														  data,
														  nullptr
										);

								_output.w() = outimg.cloneData();
								break;
							}
						default:
							_output.w() = _input.r();
						}
					} catch(std::exception &) {
						_output.w() = _input.r();
					}
				} else {
					_output.w() = _input.r();
				}

				_input.submit();
				_output.submit();
			}
		}
	}

	Input<BaseImageView, DataProfile>	_input;
	Output<BaseImageView>	_output;
	Input<int, ParameterProfile>				_radius;
	Input<bool, ParameterProfile>				_enable;

	std::unique_ptr<cv::videostab::OnePassStabilizer> _stabilizer;
	std::unique_ptr<cv::videostab::GaussianMotionFilter> _motion_filter;
	std::unique_ptr<cv::videostab::WeightingDeblurer> _deblurer;
	std::unique_ptr<cv::videostab::IGlobalMotionEstimator> _motion_estimator;
	std::unique_ptr<VideoSource>	_frameSource;
	cv::Mat _stabilizedFrame;
};

}}
RELAYBUS_RELAY(relaybus::opencv::ImageStabilization, 1, "OpenCV image stabilization")
