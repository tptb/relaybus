/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include <cmath>

#include <relaybus/image/Image.hpp>
#include <relaybus/Relay.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace relaybus { namespace opencv {

class PerspectiveWarp : public Relay {
public:
	PerspectiveWarp() {}

private:
	void init() override {
		publish("input", _input);
		publish("output", _output);
	}

	void deinit() override {
	}

	void process() override {
		// cv::getPerspectiveTransform
		// cv::warpPerspective
	}

	Input<image::BaseImageView, DataProfile>	_input;
	Output<image::BaseImageView>	_output;
};

}}
RELAYBUS_RELAY(relaybus::opencv::PerspectiveWarp, 1, "OpenCV perspective warp component")
