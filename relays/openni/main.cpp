/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#ifdef __linux__
	#define linux true
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreorder"
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma clang diagnostic ignored "-Wextra-semi"
#pragma clang diagnostic ignored "-Wcast-align"
#pragma clang diagnostic ignored "-Wunreachable-code"
#pragma clang diagnostic ignored "-Wcovered-switch-default"
#pragma clang diagnostic ignored "-Wundef"
#pragma clang diagnostic ignored "-Wnewline-eof"
#include <XnOS.h>
#include <XnCppWrapper.h>
#pragma GCC diagnostic pop

#include <cmath>
#include <stdexcept>
#include <sstream>

#include <relaybus/BaseImage.hpp>
#include <relaybus/Relay.hpp>
#include <relaybus/MakeString.hpp>

using namespace xn;

namespace relaybus {

struct OpenNI : public Relay {
	OpenNI() {}
private:
	void init() override {
		publish("rgb", _rgb_image);
		publish("depth", _depth_image);
		publish("mirror_images", _mirror_images);

		EnumerationErrors errors;
		XnStatus rc = _context.Init();
		if (rc == XN_STATUS_NO_NODE_PRESENT) {
			XnChar strError[1024];
			errors.ToString(strError, 1024);
			reportError(strError);
		} else if (rc != XN_STATUS_OK) {
			reportError(MakeString() << "Open failed: "<<xnGetStatusString(rc));
		}

		rc = _depth.Create(_context);
		if (rc != XN_STATUS_OK)
			reportError("No depth node exists!");

		rc = _image.Create(_context);
		if (rc != XN_STATUS_OK)
			reportError("No image node exists!");

		XnMapOutputMode mapMode;
		mapMode.nXRes = XN_VGA_X_RES;
		mapMode.nYRes = XN_VGA_Y_RES;
		mapMode.nFPS = 30;
		rc = _depth.SetMapOutputMode(mapMode);
		rc = _image.SetMapOutputMode(mapMode);

		_context.StartGeneratingAll();

		_depth.GetMetaData(_depthMD);
		_image.GetMetaData(_imageMD);

		// RGB is the only image format supported.
		if (_imageMD.PixelFormat() != XN_PIXEL_FORMAT_RGB24)
			reportError("The device image format must be RGB24");

		_depth.GetAlternativeViewPointCap().SetViewPoint(_image);
		//_depth.GetAlternativeViewPointCap().ResetViewPoint();

		//_context.SetGlobalMirror(!_context.GetGlobalMirror());
	}

	void deinit() override {
		//_context.Shutdown(); // shut be called
	}

	void process() override {
		waitForWires();

		// Read a new frame
		XnStatus rc = _context.WaitAnyUpdateAll();
		if (rc != XN_STATUS_OK)
			reportError(MakeString() << "Read failed: " << xnGetStatusString(rc));

		if(_rgb_image.hasOutgoingWires()) {
			_image.GetMetaData(_imageMD);
			_rgb_image.f() = BaseImage(_imageMD.XRes(), _imageMD.YRes(),
									   relaybus::RPixelformat::RPixelformat_RGB24,
									   3*_imageMD.XRes(),
									   const_cast<uint8_t*>(_imageMD.Data()),
									   nullptr).cloneData();
			_rgb_image.submit();
		}

		if(_depth_image.hasOutgoingWires()) {
			_depth.GetMetaData(_depthMD);
			_depth_image.f() = BaseImage(_depthMD.XRes(), _depthMD.YRes(),
										 relaybus::RPixelformat::RPixelformat_DEPTH_16ULE,
										 2*_depthMD.XRes(),
										 reinterpret_cast<uint8_t*>(const_cast<uint16_t*>(_depthMD.Data())),
										 nullptr).cloneData();
			_depth_image.submit();
		}
	}

	Output<BaseImageView>			_rgb_image;
	Output<BaseImageView>			_depth_image;
	Input<bool, ParameterProfile>	_mirror_images;

	Context _context;

	DepthGenerator	_depth;
	ImageGenerator	_image;
	ImageMetaData	_imageMD;
	DepthMetaData	_depthMD;
};

}
RELAYBUS_RELAY(relaybus::OpenNI, 1, "OpenNI component")
