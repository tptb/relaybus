/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @date      2013-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <relaybus/Relay.hpp>
#include <relaybus/MakeString.hpp>

#include <relaybus/streaming.hpp>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

namespace relaybus {

class UdpMulticastH264Receiver : public Relay {
public:
	UdpMulticastH264Receiver() : _socket(_io_service) {}
private:
	void init() override {

		_listen_address = boost::asio::ip::address::from_string("0.0.0.0");
		_pin_listen_address.f() = _listen_address.to_string();
		_pin_listen_address.submit();

		publish("listen_address", _pin_listen_address);
		publish("multicast_address", _pin_multicast_address);
		publish("multicast_port", _pin_multicast_port);
		publish("stream/packet",	_stream_packet);
	}

	void deinit() override {}

	void process() override {
		//boost::asio::io_service::work work(_io_service);
		waitForWires();
		handleUpdates();
		_io_service.run();
	}

	void handleUpdates() {
		bool reconnect = false;

		if(_pin_listen_address.tryUpdate()) {
			// TODO check for errors
			_listen_address = boost::asio::ip::address::from_string(_pin_listen_address.r());
			reconnect = true;
			_pin_listen_address.f() = _listen_address.to_string();
			_pin_listen_address.submit();
		}

		if(_pin_multicast_address.tryUpdate()) {
			// TODO check for errors
			_multicast_address = boost::asio::ip::address::from_string(_pin_multicast_address.r());
			reconnect = true;
			_pin_multicast_address.f() = _multicast_address.to_string();
			_pin_multicast_address.submit();
		}

		if(_pin_multicast_port.tryUpdate()) {
			_multicast_port = _pin_multicast_port.r();
			reconnect = true;
			_pin_multicast_port.submit();
		}

		if(reconnect) {
			boost::system::error_code ec;
			// Create the socket so that multiple may be bound to the same address.
			boost::asio::ip::udp::endpoint listen_endpoint(
				_multicast_address, _multicast_port);
			_socket.open(listen_endpoint.protocol(), ec);
			_socket.set_option(boost::asio::ip::udp::socket::reuse_address(true), ec);
			_socket.bind(listen_endpoint, ec);

			// Join the multicast group.
			_socket.set_option(
				boost::asio::ip::multicast::join_group(_multicast_address), ec);

			auto &streamPacket = _stream_packet.c();
			streamPacket.data = SharedArray<uint8_t>(1024*1024);

			_socket.async_receive_from(
				boost::asio::buffer(streamPacket.data.data(), streamPacket.data.size()),
				_sender_endpoint,
				boost::bind(&UdpMulticastH264Receiver::handleReceiveFrom, this,
					boost::asio::placeholders::error,
					boost::asio::placeholders::bytes_transferred));
		}
	}

	void handleReceiveFrom(const boost::system::error_code& error, size_t bytes_recvd)
	{
		if (!error) {
			{
				auto &streamPacket = _stream_packet.rw();
				streamPacket.timeStamp = 0;
				streamPacket.streamNumber = 0; // todo
				streamPacket.flags = StreamPacketFlags::Flag_Unknown; // the decoder has to guess
				streamPacket.data.setDataRegion(0, bytes_recvd);
			}

			_stream_packet.submit();

			if(!isExitRequested()){
				auto &streamPacket = _stream_packet.c();
				streamPacket.data = SharedArray<uint8_t>(1024*1024);

				_socket.async_receive_from(
					boost::asio::buffer(streamPacket.data.data(), streamPacket.data.size()),
					_sender_endpoint,
					boost::bind(&UdpMulticastH264Receiver::handleReceiveFrom, this,
						boost::asio::placeholders::error,
						boost::asio::placeholders::bytes_transferred));
			}
		}

		handleUpdates();
	}

	boost::asio::io_service					_io_service;

	Input<std::string, ParameterProfile>	_pin_listen_address;
	Input<std::string, ParameterProfile>	_pin_multicast_address;
	Input<int, ParameterProfile>			_pin_multicast_port;
	Output<StreamPacket>					_stream_packet;

	uint16_t								_multicast_port;
	boost::asio::ip::address				_listen_address;
	boost::asio::ip::address				_multicast_address;

	boost::asio::ip::udp::socket			_socket;
	boost::asio::ip::udp::endpoint			_sender_endpoint;

	//std::array<char, 65535>					_data;
};

}





RELAYBUS_RELAY(relaybus::UdpMulticastH264Receiver, 1, "udp multicast source that can receive streams send with e.g. /opt/vc/bin/raspivid | mnc -p 12345 233.1.15.175")
