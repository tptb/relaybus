/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @date      2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <relaybus/Relay.hpp>
#include <relaybus/MakeString.hpp>

#include <relaybus/streaming.hpp>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

namespace relaybus {

class UdpMulticastH264Sender : public Relay {
public:
	UdpMulticastH264Sender() : _socket(_io_service) {}
private:
	void init() override {

		_send_address = boost::asio::ip::address::from_string("0.0.0.0");
		_pin_send_address.f() = _send_address.to_string();
		_pin_send_address.submit();

		publish("send_address", _pin_send_address);
		publish("multicast_address", _pin_multicast_address);
		publish("multicast_port", _pin_multicast_port);
		publish("stream/packet",	_stream_packet);
	}

	void deinit() override {}

	void process() override {
		waitForWires();
		handleUpdates();
		_io_service.run();
	}

	void handleUpdates() {
		bool reconnect = false;

		if(_pin_send_address.tryUpdate()) {
			// TODO check for errors
			_send_address = boost::asio::ip::address::from_string(_pin_send_address.r());
			reconnect = true;
			_pin_send_address.f() = _send_address.to_string();
			_pin_send_address.submit();
		}

		if(_pin_multicast_address.tryUpdate()) {
			// TODO check for errors
			_multicast_address = boost::asio::ip::address::from_string(_pin_multicast_address.r());
			reconnect = true;
			_pin_multicast_address.f() = _multicast_address.to_string();
			_pin_multicast_address.submit();
		}

		if(_pin_multicast_port.tryUpdate()) {
			_multicast_port = _pin_multicast_port.r();
			reconnect = true;
			_pin_multicast_port.submit();
		}

		if(reconnect) {
			// Create the socket so that multiple may be bound to the same address.
			boost::asio::ip::udp::endpoint send_endpoint(
				_send_address, _multicast_port);
			_socket.open(send_endpoint.protocol());
			_socket.set_option(boost::asio::ip::udp::socket::reuse_address(true));
			_socket.bind(send_endpoint);

			// Join the multicast group.
			_socket.set_option(
				boost::asio::ip::multicast::join_group(_multicast_address));

			auto &streamPacket = _stream_packet.c();
			streamPacket.data = SharedArray<uint8_t>(1024*1024);

			_socket.async_receive_from(
				boost::asio::buffer(streamPacket.data.data(), streamPacket.data.size()),
				_sender_endpoint,
				boost::bind(&UdpMulticastH264Sender::handleReceiveFrom, this,
					boost::asio::placeholders::error,
					boost::asio::placeholders::bytes_transferred));
		}
	}

	void handleReceiveFrom(const boost::system::error_code& error, size_t bytes_recvd)
	{
		if (!error) {
			{
				auto &streamPacket = _stream_packet.rw();
				streamPacket.timeStamp = 0;
				streamPacket.streamNumber = 0; // todo
				streamPacket.flags = StreamPacketFlags::Flag_Unknown; // the decoder has to guess
				streamPacket.data.setDataRegion(0, bytes_recvd);
			}

			_stream_packet.submit();

			if(!isExitRequested()){
				auto &streamPacket = _stream_packet.c();
				streamPacket.data = SharedArray<uint8_t>(1024*1024);

				_socket.async_receive_from(
					boost::asio::buffer(streamPacket.data.data(), streamPacket.data.size()),
					_sender_endpoint,
					boost::bind(&UdpMulticastH264Sender::handleReceiveFrom, this,
						boost::asio::placeholders::error,
						boost::asio::placeholders::bytes_transferred));
			}
		}

		handleUpdates();
	}

	boost::asio::io_service					_io_service;

	Input<std::string, ParameterProfile>	_pin_send_address;
	Input<std::string, ParameterProfile>	_pin_multicast_address;
	Input<int, ParameterProfile>			_pin_multicast_port;
	Output<StreamPacket>					_stream_packet;

	uint16_t								_multicast_port;
	boost::asio::ip::address				_send_address;
	boost::asio::ip::address				_multicast_address;

	boost::asio::ip::udp::socket			_socket;
	boost::asio::ip::udp::endpoint			_sender_endpoint;

	//std::array<char, 65535>					_data;
};

}

RELAYBUS_RELAY(relaybus::UdpMulticastH264Sender, 1, "port: 12345 address: 233.1.15.175")
