/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <relaybus/image/BaseImage.hpp>
#include <relaybus/Relay.hpp>
#include <relaybus/MakeString.hpp>

namespace relaybus {

struct Sink : public Relay {
	void init() override {
		_pin_debug.w() = false;
		_pin_bool.w() = true;
		_pin_int32.w() = 1;
		_pin_double.w() = 1.0;
		_pin_image.w() = image::BaseImage(100, 100, image::RPixelformat::RPixelformat_GRAY8, nullptr);
		_pin_image.w().data(0)[0] = 10;

		publish("debug",  _pin_debug);
		publish("bool",   _pin_bool);
		publish("int32",  _pin_int32);
		publish("double", _pin_double);
		publish("string", _pin_string);
		publish("image",  _pin_image);
		publish("string_2", _pin_string_2);
	}

	void deinit() override {}

	void debugOutput(const std::string &str) {
		if(_pin_debug.isValid() && _pin_debug.r())
			reportInformation(str);
	}

	void process() override {
		waitForWires();

		if(_pin_debug.tryUpdate()) {
			_pin_debug.submit();
		}

		if(_pin_bool.tryUpdate()) {
			if(_pin_bool.isValid())
				debugOutput(MakeString() << "Received bool="<<_pin_bool.r());
			_pin_bool.submit();
		}

		if(_pin_int32.tryUpdate()) {
			if(_pin_int32.isValid())
				debugOutput(MakeString() << "Received int32="<<_pin_int32.r());
			_pin_int32.submit();
		}

		if(_pin_double.tryUpdate()) {
			if(_pin_double.isValid())
				debugOutput(MakeString() << "Received double="<<_pin_double.r());
			_pin_double.submit();
		}

		if(_pin_string.tryUpdate()) {
			if(_pin_string.isValid())
				debugOutput(MakeString() << "Received string="<<_pin_string.r());
			_pin_string.submit();
		}

		if(_pin_image.tryUpdate()) {
			if(_pin_image.isValid()) {
				debugOutput(MakeString() << "Received image: width="<<_pin_image.r().width()<<" height="<<_pin_image.r().height()<<" value="<<static_cast<int>(_pin_image.r().data(0)[0]));
				_pin_image.submit();
			}
		}

		if(_pin_string_2.tryUpdate()) {
			if(_pin_string_2.isValid())
				debugOutput(MakeString() << "Received string_2="<<_pin_string_2.r());
			_pin_string_2.submit();
		}
	}

	Input<bool, ParameterProfile>		_pin_debug;
	Input<bool, DataProfile>			_pin_bool;
	Input<int32_t, DataProfile>			_pin_int32;
	Input<double, DataProfile>			_pin_double;
	Input<std::string, DataProfile>		_pin_string;
	Input<image::BaseImageView, DataProfile>	_pin_image;
	Input<std::string, DataProfile>		_pin_string_2;
};

}

RELAYBUS_RELAY(relaybus::Sink, 1, "Test sink that has different output pins")
