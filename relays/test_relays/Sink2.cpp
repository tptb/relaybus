/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <relaybus/Relay.hpp>
#include <relaybus/MakeString.hpp>
#include <relaybus/chrono.hpp>

namespace relaybus {

struct Sink2 : public Relay {
	void init() override {
		_pin_int32.w() = 1;
		_count = 0;
		publish("int32",  _pin_int32);
		_start = clock::now();
	}

	void deinit() override {}


	void process() override {
		_pin_int32.updateAndSubmit();
		if(_pin_int32.isValid()) {
			_count++;
			if(_count == 1000000) {
				double dur = 1000000.0 / (double)std::chrono::duration_cast<std::chrono::microseconds>(clock::now() - _start).count();

				reportInformation(MakeString() << _count * dur);
				_start = clock::now();
				_count = 0;
			}
		}
	}

	Input<int32_t, DataProfile>			_pin_int32;
	uint64_t _count;
	time_point _start;
};

}

RELAYBUS_RELAY(relaybus::Sink2, 1, "Test sink that has different output pins")
