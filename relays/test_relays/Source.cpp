/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <relaybus/image/BaseImage.hpp>
#include <relaybus/Relay.hpp>
#include <relaybus/MakeString.hpp>

namespace relaybus {

struct Source : public Relay {
	void init() override {
		_pin_debug.w() = false;
		_pin_bool.w() = false;
		_pin_int32.w() = 0;
		_pin_double.w() = 0.0;
		_pin_image.w() = image::BaseImage(200, 200, image::RPixelformat::RPixelformat_GRAY8, nullptr);
		_pin_image.w().data(0)[0] = 0;

		publish("debug",   _pin_debug);
		publish("bool",   _pin_bool);
		publish("int32",  _pin_int32);
		publish("double", _pin_double);
		publish("string", _pin_string);
		publish("image",  _pin_image);
	}

	void deinit() override {}

	void debugOutput(const std::string &str) {
		if(_pin_debug.r())
			reportInformation(str);
	}

	void process() override {
		waitForWires();

		if(_pin_debug.tryUpdate())
			_pin_debug.submit();

		if(_pin_bool.hasOutgoingWires()) {
			_pin_bool.w() = ! _pin_bool.r();
			debugOutput(MakeString() << "Send bool="<<_pin_bool.r());
			_pin_bool.submit();
		}

		if(_pin_int32.hasOutgoingWires()) {
			_pin_int32.w() = _pin_int32.r() + 1;
			debugOutput(MakeString() << "Send int32="<<_pin_int32.r());
			_pin_int32.submit();
		}

		if(_pin_double.hasOutgoingWires()) {
			_pin_double.rw() += 0.01;
			debugOutput(MakeString() << "Send double="<<_pin_double.r());
			_pin_double.submit();
		}

		if(_pin_string.hasOutgoingWires()) {
			_pin_string.w() = MakeString() << "bool="<<_pin_bool.r()<<" int32="<<_pin_int32.r()<<" double="<<_pin_double.r();
			debugOutput(MakeString() << "Send string="<<_pin_string.r());
			_pin_string.submit();
		}

		if(_pin_image.hasOutgoingWires()) {
			_pin_image.rw().data(0)[0]++;
			debugOutput(MakeString() << "Send image: width="<<_pin_image.r().width()<<" height="<<_pin_image.r().height()<<" value="<<static_cast<int>(_pin_image.r().data(0)[0]));
			_pin_image.submit();
		}
	}

	Input<bool, ParameterProfile>	_pin_debug;
	Output<bool>					_pin_bool;
	Output<int32_t>					_pin_int32;
	Output<double>					_pin_double;
	Output<std::string>				_pin_string;
	Output<image::BaseImageView>			_pin_image;
};

}

RELAYBUS_RELAY(relaybus::Source, 1, "Test source that has different output pins")
