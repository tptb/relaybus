/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */

#include <relaybus/Relay.hpp>
#include <relaybus/MakeString.hpp>

namespace relaybus {

struct Source2 : public Relay {
	void init() override {
		_pin_int32.w() = 0;
		publish("int32",  _pin_int32);
	}

	void deinit() override {}

	void process() override {
		_pin_int32.w() = _pin_int32.r() + 1;
		_pin_int32.submit();
	}

	Output<int32_t>					_pin_int32;
};

}

RELAYBUS_RELAY(relaybus::Source2, 1, "Test source that has different output pins")
