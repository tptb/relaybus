/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include <string>
#include <vector>

#include <relaybus/image/BaseImage.hpp>
#include <relaybus/streaming.hpp>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <stdexcept>

#include <fcntl.h>	// low-level i/o
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>

#include <relaybus/MakeString.hpp>

#include <relaybus/Relay.hpp>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

namespace relaybus { namespace image {

static void noopDataDeallocator(uint8_t *) {}

/**
 * @brief The V4L2 class
 *  Based on the sourcecode from https://github.com/csete/bonecam/tree/master/capture
 */
class V4L2 : public Relay {
public:
	V4L2() : fd(-1) {}
protected:

	struct Buffer {
		void   *start;
		size_t length;
	};

	void init() override {
		publish("uri",				_uri);
		publish("stream/packet",	_stream_packet);
		publish("stream/information/0", _stream_information);
		publish("stream/information/vec", _stream_informations);
	}

	void deinit() override {
		close();
	}

	void process() override {
		waitForWires();

		if(_uri.tryUpdate()) {
			open();
			_uri.submit();
		}

		if(fd != -1) {

			fd_set fds;
			timeval tv;

			FD_ZERO(&fds);
			FD_SET(fd, &fds);

			// Timeout
			tv.tv_sec = 2;
			tv.tv_usec = 0;

			int r = select(fd + 1, &fds, nullptr, nullptr, &tv);

			if (-1 == r) {
				if (EINTR == errno)
					return;
				errno_exit("select");
			}

			if (0 == r)
				reportError("select timeout");

			//read_frame
			v4l2_buffer buf;

			CLEAR(buf);

			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory = V4L2_MEMORY_MMAP;

			if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
				switch (errno) {
				case EAGAIN: // no data yet
					return;

				case EIO:
					// Could ignore EIO, see spec.
					// fall through

				default:
					errno_exit("VIDIOC_DQBUF");
				}
			}

			_stream_packet.w().streamNumber = 0;

			_stream_packet.rw().timeStamp = buf.sequence; // todo presentationTime
			_stream_packet.rw().flags = StreamPacketFlags::Flag_Unknown;
			_stream_packet.rw().data = SharedArray<uint8_t>(
											reinterpret_cast<uint8_t*>(buffers[buf.index].start),
												buf.bytesused, &noopDataDeallocator);

			_stream_packet.submit();

			if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
				errno_exit("VIDIOC_QBUF");
		}
	}

	void open() {
		close();
		open_device();
		init_device(1920, 1080, 30);
		start_capturing();

		// write stream information
		StreamInformation stream_info;
		stream_info.streamNumber = 0;
		stream_info.streamType = StreamType::Stream_Video;
		stream_info.codecType = CodecType::Codec_Video_H264;
		stream_info.framesPerSecond = 30;
		//stream_info.extraData
		_stream_informations.rw().clear();
		_stream_informations.rw().push_back(stream_info);
		_stream_information.rw() = stream_info;
		_stream_information.submit();
		_stream_informations.submit();
	}

	void close() {
		if(fd == -1)
			return;
		stop_capturing();
		uninit_device();
		close_device();
	}

	void errno_exit(const std::string &msg) {
		reportError(MakeString()<<_uri.r()<<" errno="<<errno<<" errnostr="<<strerror(errno)<<" msg="<<msg);
	}

	int xioctl(int fh, unsigned long int request, void *arg) {
		int r;

		do {
			r = ioctl(fh, request, arg);
		} while (-1 == r && EINTR == errno);

		return r;
	}

	void stop_capturing() {
		enum v4l2_buf_type type;

		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type))
			errno_exit("VIDIOC_STREAMOFF");
	}

	void start_capturing() {
		enum v4l2_buf_type type;

		for (unsigned int i = 0; i < buffers.size(); ++i) {
			v4l2_buffer buf;

			CLEAR(buf);
			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory = V4L2_MEMORY_MMAP;
			buf.index = i;

			if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
				errno_exit("VIDIOC_QBUF");
		}
		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
			errno_exit("VIDIOC_STREAMON");
	}

	void uninit_device() {
		for (auto b : buffers)
			if (-1 == munmap(b.start, b.length))
				errno_exit("munmap");

		buffers.clear();
	}

	void init_mmap() {
		v4l2_requestbuffers req;

		CLEAR(req);

		req.count = 4;
		req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		req.memory = V4L2_MEMORY_MMAP;

		if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
			if (EINVAL == errno)
				reportError("does not support memory mapping.");
			else
				errno_exit("VIDIOC_REQBUFS");
		}

		if (req.count < 2)
			reportError("Insufficient buffer memory");

		buffers.resize(req.count);

		for (unsigned int i = 0; i < buffers.size(); ++i) {
			v4l2_buffer buf;

			CLEAR(buf);

			buf.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory	= V4L2_MEMORY_MMAP;
			buf.index	= i;

			if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf))
				errno_exit("VIDIOC_QUERYBUF");

			buffers[i].length = buf.length;
			buffers[i].start = mmap(nullptr, // start anywhere
									buf.length,
									PROT_READ | PROT_WRITE, // required
									MAP_SHARED, // recommended
									fd, buf.m.offset);

			if (MAP_FAILED == buffers[i].start)
				errno_exit("mmap");
		}
	}

	void init_device(image_size_type width, image_size_type height, double fps) {
		v4l2_capability cap;
		v4l2_cropcap cropcap;
		v4l2_crop crop;
		v4l2_format fmt;

		if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap)) {
			if (EINVAL == errno)
				reportError("is no V4L2 device");
			else
				errno_exit("VIDIOC_QUERYCAP");
		}

		if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
			reportError("is no video capture device");

		if (!(cap.capabilities & V4L2_CAP_STREAMING))
			reportError("does not support streaming i/o");

		// Select video input, video standard and tune here.

		CLEAR(cropcap);

		cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

		if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap)) {
			crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			crop.c = cropcap.defrect; // reset to default

			if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop)) {
				switch (errno) {
				case EINVAL:
					// Cropping not supported
					break;
				default:
					// Errors ignored
					break;
				}
			}
		} else {
			// Errors ignored
		}

		CLEAR(fmt);

		fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

		// set resolution
		fmt.fmt.pix.width       = static_cast<uint32_t>(width);
		fmt.fmt.pix.height      = static_cast<uint32_t>(height);
		fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_H264; //replace
		fmt.fmt.pix.field       = V4L2_FIELD_ANY;

		if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt))
			errno_exit("VIDIOC_S_FMT");

		// Note VIDIOC_S_FMT may change width and height.

		// Preserve original settings as set by v4l2-ctl for example
		//if (-1 == xioctl(fd, VIDIOC_G_FMT, &fmt))
		//	errno_exit("VIDIOC_G_FMT");

		// Buggy driver paranoia
		unsigned int min = fmt.fmt.pix.width * 2;
		if (fmt.fmt.pix.bytesperline < min)
			fmt.fmt.pix.bytesperline = min;
		min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
		if (fmt.fmt.pix.sizeimage < min)
			fmt.fmt.pix.sizeimage = min;

		{ // set framerate
			v4l2_streamparm parm;
			CLEAR(parm);
			parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			parm.parm.capture.timeperframe.numerator = 1000;
			parm.parm.capture.timeperframe.denominator =
					static_cast<uint32_t>(fps * parm.parm.capture.timeperframe.numerator);

			if (xioctl(fd, VIDIOC_S_PARM, &parm) == 0) {
				v4l2_fract *tf = &parm.parm.capture.timeperframe;

				if (!tf->denominator || !tf->numerator) {
					//std::cout<<"Invalid frame rate\n";
				} else {
					double nfps = (1.0 * tf->denominator / tf->numerator);
					reportInformation(relaybus::MakeString() << "Frame rate set to "<<nfps<<" fps");
				}
			}
		}

		init_mmap();
	}

	void close_device() {
		if (-1 == ::close(fd))
			errno_exit("close");
		fd = -1;
	}

	void open_device() {
		struct stat st;

		if (-1 == stat(_uri.r().c_str(), &st))
			errno_exit("Cannot identify");

		if (!S_ISCHR(st.st_mode))
			reportError("is no device");

		fd = ::open(_uri.r().c_str(), O_RDWR | O_NONBLOCK, 0);

		if (-1 == fd)
			errno_exit("Cannot open");
	}

	int fd;
	std::vector<Buffer> buffers;

	Input<std::string, ParameterProfile>	_uri;
	Output<StreamPacket>						_stream_packet;
	Output<StreamInformation>					_stream_information;
	Output<std::vector<StreamInformation> >		_stream_informations;
};

} }

RELAYBUS_RELAY(relaybus::image::V4L2, 1, "v4l2 relay")
