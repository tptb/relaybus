/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2012-2014
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include <relaybus/BaseImage.hpp>
#include <relaybus/Image.hpp>
#include <relaybus/MemoryImageAllocator.hpp>
#include <relaybus/relaybus_macros.hpp>
#include <relaybus/ZeroCopy.hpp>
#include <algorithm>
#include <iostream>

using namespace relaybus;

void require(bool, const std::string &);
void require(bool condition, const std::string &message) {
	if(!condition)
		std::cerr<<"require failed: "<<message<<std::endl;
}

int main(int argc, char *argv[])
{
	RELAYBUS_UNUSED(argc);
	RELAYBUS_UNUSED(argv);
	BaseImage b(MemoryImageAllocator::get());

	b = BaseImage(10, 10, RPixelformat::RPixelformat_GRAY8, MemoryImageAllocator::get());

	BaseImageView view;
	view = BaseImage(100, 100, RPixelformat::RPixelformat_RGB24, MemoryImageAllocator::get());

	view.data(0)[0] = 200;

	{
		BaseImageView view2 = BaseImage(100, 100, RPixelformat::RPixelformat_RGB24, MemoryImageAllocator::get());
		view2.data(0)[0]=100;
		view = view2;
	}
	require(view.data(0)[0]==100, "test");

	BaseImageView view3 = view.roi(5, 10, 20, 30);

	view = Image<RPixelformat::RPixelformat_RGB24>(100, 100);

	relaybus::ZeroCopy<BaseImage> image1 = std::make_shared<BaseImage>(100, 200, RPixelformat::RPixelformat_GRAY8, nullptr);
	image1.write().data(0)[0] = 142;
	relaybus::ZeroCopy<BaseImage> image2(image1);

	require(image1.read().data(0)[0] == image2.read().data(0)[0], "todo1");

	image1.write().data(0)[0] = 242;
	require(image1.read().data(0)[0] == 242, "todo2");
	require(image2.read().data(0)[0] == 142, "todo2");

	return 0;
}
