/**
 * @file
 * @author    Bernd Amend <berndamend+relaybus@googlemail.com>
 * @author    Michael Adam <relaybus+Michael.Adam@gmail.com>
 * @date      2013
 * @copyright GNU Lesser General Public version 2.1 or later
 *            see http://www.gnu.org/licenses/lgpl-2.1.txt
 */
#include <relaybus/relaybus_macros.hpp>
#include <relaybus/ZeroCopy.hpp>
#include <relaybus/TypeSystem.hpp>
#include <relaybus/chrono.hpp>
#include <relaybus/BaseImage.hpp>
#include <algorithm>
#include <iostream>

using namespace relaybus;

void require(bool, const std::string &);
void require(bool condition, const std::string &message) {
	if(!condition)
		std::cerr<<"require failed: "<<message<<std::endl;
}

const int loops = 10000000;

int compareType(const std::type_info &type) {
	if(typeid(bool) == type) {
		return 0;
	} else if(typeid(int8_t) == type) {
		return 1;
	} else if(typeid(int16_t) == type) {
		return 2;
	} else if(typeid(int32_t) == type) {
		return 3;
	} else if(typeid(int64_t) == type) {
		return 4;
	} else if(typeid(uint8_t) == type) {
		return 5;
	} else if(typeid(uint16_t) == type) {
		return 6;
	} else if(typeid(uint32_t) == type) {
		return 7;
	} else if(typeid(uint64_t) == type) {
		return 8;
	} else if(typeid(float) == type) {
		return 9;
	} else if(typeid(double) == type) {
		return 10;
	} else if(typeid(BaseImage) == type) {
		return 11;
	} else if(typeid(BaseImageView) == type) {
		return 12;
	} else if(typeid(std::string) == type) {
		return 13;
	} else if(typeid(std::wstring) == type) {
		return 14;
	}
	return 0xffff;
}

int compareTypeTS(const std::string &type) {
	if(TypeSystem<bool>::getString() == type) {
		return 0;
	} else if(TypeSystem<int8_t>::getString() == type) {
		return 1;
	} else if(TypeSystem<int16_t>::getString() == type) {
		return 2;
	} else if(TypeSystem<int32_t>::getString() == type) {
		return 3;
	} else if(TypeSystem<int64_t>::getString() == type) {
		return 4;
	} else if(TypeSystem<uint8_t>::getString() == type) {
		return 5;
	} else if(TypeSystem<uint16_t>::getString() == type) {
		return 6;
	} else if(TypeSystem<uint32_t>::getString() == type) {
		return 7;
	} else if(TypeSystem<uint64_t>::getString() == type) {
		return 8;
	} else if(TypeSystem<float>::getString() == type) {
		return 9;
	} else if(TypeSystem<double>::getString() == type) {
		return 10;
	} else if(TypeSystem<BaseImage>::getString() == type) {
		return 11;
	} else if(TypeSystem<BaseImageView>::getString() == type) {
		return 12;
	} else if(TypeSystem<std::string>::getString() == type) {
		return 13;
	} else if(TypeSystem<std::wstring>::getString() == type) {
		return 14;
	}
	return 0xffff;
}

int compareTypeTS(type_id type) {
	if(TypeSystem<bool>::id == type) {
		return 0;
	} else if(TypeSystem<int8_t>::id == type) {
		return 1;
	} else if(TypeSystem<int16_t>::id == type) {
		return 2;
	} else if(TypeSystem<int32_t>::id == type) {
		return 3;
	} else if(TypeSystem<int64_t>::id == type) {
		return 4;
	} else if(TypeSystem<uint8_t>::id == type) {
		return 5;
	} else if(TypeSystem<uint16_t>::id == type) {
		return 6;
	} else if(TypeSystem<uint32_t>::id == type) {
		return 7;
	} else if(TypeSystem<uint64_t>::id == type) {
		return 8;
	} else if(TypeSystem<float>::id == type) {
		return 9;
	} else if(TypeSystem<double>::id == type) {
		return 10;
	} else if(TypeSystem<BaseImage>::id == type) {
		return 11;
	} else if(TypeSystem<BaseImageView>::id == type) {
		return 12;
	} else if(TypeSystem<std::string>::id == type) {
		return 13;
	} else if(TypeSystem<std::wstring>::id == type) {
		return 14;
	}
	return 0xffff;
}

int compareTypeTSS(type_id type) {
	switch(type) {
		case TypeSystem<bool>::id: return 0;
		case TypeSystem<int8_t>::id: return 1;
		case TypeSystem<int16_t>::id: return 2;
		case TypeSystem<int32_t>::id: return 3;
		case TypeSystem<int64_t>::id: return 4;
		case TypeSystem<uint8_t>::id: return 5;
		case TypeSystem<uint16_t>::id: return 6;
		case TypeSystem<uint32_t>::id: return 7;
		case TypeSystem<uint64_t>::id: return 8;
		case TypeSystem<float>::id: return 9;
		case TypeSystem<double>::id: return 10;
		case TypeSystem<BaseImage>::id: return 11;
		case TypeSystem<BaseImageView>::id: return 12;
		case TypeSystem<std::string>::id: return 13;
		case TypeSystem<std::wstring>::id: return 14;
	}
	return 0xffff;
}

int compareTypeTS(const std::type_info &type) {
	if(TypeSystem<bool>::get() == type) {
		return 0;
	} else if(TypeSystem<int8_t>::get() == type) {
		return 1;
	} else if(TypeSystem<int16_t>::get() == type) {
		return 2;
	} else if(TypeSystem<int32_t>::get() == type) {
		return 3;
	} else if(TypeSystem<int64_t>::get() == type) {
		return 4;
	} else if(TypeSystem<uint8_t>::get() == type) {
		return 5;
	} else if(TypeSystem<uint16_t>::get() == type) {
		return 6;
	} else if(TypeSystem<uint32_t>::get() == type) {
		return 7;
	} else if(TypeSystem<uint64_t>::get() == type) {
		return 8;
	} else if(TypeSystem<float>::get() == type) {
		return 9;
	} else if(TypeSystem<double>::get() == type) {
		return 10;
	} else if(TypeSystem<BaseImage>::get() == type) {
		return 11;
	} else if(TypeSystem<BaseImageView>::get() == type) {
		return 12;
	} else if(TypeSystem<std::string>::get() == type) {
		return 13;
	} else if(TypeSystem<std::wstring>::get() == type) {
		return 14;
	}
	return 0xffff;
}

int compareInts(int type) {
	if(0x0fff == type) {
		return 0;
	} else if(0x1fff == type) {
		return 1;
	} else if(0x2fff == type) {
		return 2;
	} else if(0x3fff == type) {
		return 3;
	} else if(0x4fff == type) {
		return 4;
	} else if(0x5fff == type) {
		return 5;
	} else if(0x6fff == type) {
		return 6;
	} else if(0x7fff == type) {
		return 7;
	} else if(0x8fff == type) {
		return 8;
	} else if(0x9fff == type) {
		return 9;
	} else if(0xafff == type) {
		return 10;
	} else if(0xbfff == type) {
		return 11;
	} else if(0xcfff == type) {
		return 12;
	} else if(0xdfff == type) {
		return 13;
	} else if(0xefff == type) {
		return 14;
	}
	return 0xffff;
}

int compareIntsS( int type) {
	switch(type) {
		case 0x0fff: return 0;
		case 0x1fff: return 1;
		case 0x2fff: return 2;
		case 0x3fff: return 3;
		case 0x4fff: return 4;
		case 0x5fff: return 5;
		case 0x6fff: return 6;
		case 0x7fff: return 7;
		case 0x8fff: return 8;
		case 0x9fff: return 9;
		case 0xafff: return 10;
		case 0xbfff: return 11;
		case 0xcfff: return 12;
		case 0xdfff: return 13;
		case 0xefff: return 14;
	}
	return 0xffff;
}

int main()
{
	{
		std::cout<<"compare ints if ";
		std::cout.flush();
		auto start = clock::now();

		volatile uint64_t result = 0;
		for(int i=0;i<loops;++i) {
			result += compareInts(0x0fff);
			result += compareInts(0x1fff);
			result += compareInts(0x2fff);
			result += compareInts(0x3fff);
			result += compareInts(0x4fff);
			result += compareInts(0x5fff);
			result += compareInts(0x6fff);
			result += compareInts(0x7fff);
			result += compareInts(0x8fff);
			result += compareInts(0x9fff);
			result += compareInts(0xafff);
			result += compareInts(0xbfff);
			result += compareInts(0xcfff);
			result += compareInts(0xdfff);
			result += compareInts(0xefff);
		}
		auto end = clock::now();
		std::cout<<chrono::duration_cast<chrono::milliseconds>(end-start).count()<<" ms"<<std::endl;
		std::cout<<"trash="<<result<<std::endl;
	}

	{
		std::cout<<"compare ints switch ";
		std::cout.flush();
		auto start = clock::now();

		volatile uint64_t result = 0;
		for(int i=0;i<loops;++i) {
			result += compareIntsS(0x0fff);
			result += compareIntsS(0x1fff);
			result += compareIntsS(0x2fff);
			result += compareIntsS(0x3fff);
			result += compareIntsS(0x4fff);
			result += compareIntsS(0x5fff);
			result += compareIntsS(0x6fff);
			result += compareIntsS(0x7fff);
			result += compareIntsS(0x8fff);
			result += compareIntsS(0x9fff);
			result += compareIntsS(0xafff);
			result += compareIntsS(0xbfff);
			result += compareIntsS(0xcfff);
			result += compareIntsS(0xdfff);
			result += compareIntsS(0xefff);
		}
		auto end = clock::now();
		std::cout<<chrono::duration_cast<chrono::milliseconds>(end-start).count()<<" ms"<<std::endl;
		std::cout<<"trash="<<result<<std::endl;
	}

	{
		std::cout<<"compare typeid ";
		std::cout.flush();
		auto start = clock::now();

		volatile uint64_t result = 0;
		for(int i=0;i<loops;++i) {
			result += compareType(typeid(bool));
			result += compareType(typeid(int8_t));
			result += compareType(typeid(int16_t));
			result += compareType(typeid(int32_t));
			result += compareType(typeid(int64_t));
			result += compareType(typeid(uint8_t));
			result += compareType(typeid(uint16_t));
			result += compareType(typeid(uint32_t));
			result += compareType(typeid(uint64_t));
			result += compareType(typeid(float));
			result += compareType(typeid(double));
			result += compareType(typeid(BaseImage));
			result += compareType(typeid(BaseImageView));
			result += compareType(typeid(std::string));
			result += compareType(typeid(std::wstring));
		}
		auto end = clock::now();
		std::cout<<chrono::duration_cast<chrono::milliseconds>(end-start).count()<<" ms"<<std::endl;
		std::cout<<"trash="<<result<<std::endl;
	}

	{
		std::cout<<"compare TypeSystem id if ";
		std::cout.flush();
		auto start = clock::now();

		volatile uint64_t result = 0;
		for(int i=0;i<loops;++i) {
			result += compareTypeTS(TypeSystem<bool>::id);
			result += compareTypeTS(TypeSystem<int8_t>::id);
			result += compareTypeTS(TypeSystem<int16_t>::id);
			result += compareTypeTS(TypeSystem<int32_t>::id);
			result += compareTypeTS(TypeSystem<int64_t>::id);
			result += compareTypeTS(TypeSystem<uint8_t>::id);
			result += compareTypeTS(TypeSystem<uint16_t>::id);
			result += compareTypeTS(TypeSystem<uint32_t>::id);
			result += compareTypeTS(TypeSystem<uint64_t>::id);
			result += compareTypeTS(TypeSystem<float>::id);
			result += compareTypeTS(TypeSystem<double>::id);
			result += compareTypeTS(TypeSystem<BaseImage>::id);
			result += compareTypeTS(TypeSystem<BaseImageView>::id);
			result += compareTypeTS(TypeSystem<std::string>::id);
			result += compareTypeTS(TypeSystem<std::wstring>::id);
		}
		auto end = clock::now();
		std::cout<<chrono::duration_cast<chrono::milliseconds>(end-start).count()<<" ms"<<std::endl;
		std::cout<<"trash="<<result<<std::endl;
	}

	{
		std::cout<<"compare TypeSystem id switch ";
		std::cout.flush();
		auto start = clock::now();

		volatile uint64_t result = 0;
		for(int i=0;i<loops;++i) {
			result += compareTypeTSS(TypeSystem<bool>::id);
			result += compareTypeTSS(TypeSystem<int8_t>::id);
			result += compareTypeTSS(TypeSystem<int16_t>::id);
			result += compareTypeTSS(TypeSystem<int32_t>::id);
			result += compareTypeTSS(TypeSystem<int64_t>::id);
			result += compareTypeTSS(TypeSystem<uint8_t>::id);
			result += compareTypeTSS(TypeSystem<uint16_t>::id);
			result += compareTypeTSS(TypeSystem<uint32_t>::id);
			result += compareTypeTSS(TypeSystem<uint64_t>::id);
			result += compareTypeTSS(TypeSystem<float>::id);
			result += compareTypeTSS(TypeSystem<double>::id);
			result += compareTypeTSS(TypeSystem<BaseImage>::id);
			result += compareTypeTSS(TypeSystem<BaseImageView>::id);
			result += compareTypeTSS(TypeSystem<std::string>::id);
			result += compareTypeTSS(TypeSystem<std::wstring>::id);
		}
		auto end = clock::now();
		std::cout<<chrono::duration_cast<chrono::milliseconds>(end-start).count()<<" ms"<<std::endl;
		std::cout<<"trash="<<result<<std::endl;
	}

	{
		std::cout<<"compare TypeSystem typeid ";
		std::cout.flush();
		auto start = clock::now();

		volatile uint64_t result = 0;
		for(int i=0;i<loops;++i) {
			result += compareTypeTS(TypeSystem<bool>::get());
			result += compareTypeTS(TypeSystem<int8_t>::get());
			result += compareTypeTS(TypeSystem<int16_t>::get());
			result += compareTypeTS(TypeSystem<int32_t>::get());
			result += compareTypeTS(TypeSystem<int64_t>::get());
			result += compareTypeTS(TypeSystem<uint8_t>::get());
			result += compareTypeTS(TypeSystem<uint16_t>::get());
			result += compareTypeTS(TypeSystem<uint32_t>::get());
			result += compareTypeTS(TypeSystem<uint64_t>::get());
			result += compareTypeTS(TypeSystem<float>::get());
			result += compareTypeTS(TypeSystem<double>::get());
			result += compareTypeTS(TypeSystem<BaseImage>::get());
			result += compareTypeTS(TypeSystem<BaseImageView>::get());
			result += compareTypeTS(TypeSystem<std::string>::get());
			result += compareTypeTS(TypeSystem<std::wstring>::get());
		}
		auto end = clock::now();
		std::cout<<chrono::duration_cast<chrono::milliseconds>(end-start).count()<<" ms"<<std::endl;
		std::cout<<"trash="<<result<<std::endl;
	}

	{
		std::cout<<"compare TypeSystem strings ";
		std::cout.flush();
		auto start = clock::now();

		volatile uint64_t result = 0;
		for(int i=0;i<loops;++i) {
			result += compareTypeTS(TypeSystem<bool>::getString());
			result += compareTypeTS(TypeSystem<int8_t>::getString());
			result += compareTypeTS(TypeSystem<int16_t>::getString());
			result += compareTypeTS(TypeSystem<int32_t>::getString());
			result += compareTypeTS(TypeSystem<int64_t>::getString());
			result += compareTypeTS(TypeSystem<uint8_t>::getString());
			result += compareTypeTS(TypeSystem<uint16_t>::getString());
			result += compareTypeTS(TypeSystem<uint32_t>::getString());
			result += compareTypeTS(TypeSystem<uint64_t>::getString());
			result += compareTypeTS(TypeSystem<float>::getString());
			result += compareTypeTS(TypeSystem<double>::getString());
			result += compareTypeTS(TypeSystem<BaseImage>::getString());
			result += compareTypeTS(TypeSystem<BaseImageView>::getString());
			result += compareTypeTS(TypeSystem<std::string>::getString());
			result += compareTypeTS(TypeSystem<std::wstring>::getString());
		}
		auto end = clock::now();
		std::cout<<chrono::duration_cast<chrono::milliseconds>(end-start).count()<<" ms"<<std::endl;
		std::cout<<"trash="<<result<<std::endl;
	}

	return 0;
}
